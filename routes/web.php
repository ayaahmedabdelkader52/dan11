<?php

use App\Http\Controllers\Site\AuthController;
use App\Http\Controllers\Site\HomeController;
use App\Http\Controllers\Site\ProductController;
use App\Http\Controllers\Site\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/dashboard')->name('dashboard')->middleware('admin');

app()->singleton('lang', function () {
    return App\Http\Controllers\Site\LanguageController::lang();
});

Route::get('lang/{lang}', function ($lang) {
    if ($lang == 'ar') {
        session()->put('lang' , 'ar');
    } else {
        session()->put('lang' , 'en');
    }
    if (\Auth::check()) {
        \Auth::user()->lang = $lang;
        \Auth::user()->update();
    }
    return back();
});

Route::group(['middleware' => 'auth'] , function () {


    Route::post('/add-to-cart' , [ProductController::class , 'addCart'])->name('add-to-cart');
    Route::get('/cart', [ProductController::class, 'getCart'])->name('cart');
    Route::get('/bill', [ProductController::class, 'getCheckout'])->name('bill');
    Route::post('/checkout', [ProductController::class, 'Checkout'])->name('checkout');
    Route::get('/reduce/{product}', [ProductController::class, 'getRemoveItem'])->name('reduce');


    Route::get('/likes', [HomeController::class, 'likes'])->name('likes');
    Route::post('/like', [HomeController::class, 'like'])->name('like');
    Route::post('/rate', [HomeController::class, 'rate'])->name('rate');



    Route::get('/change', [AuthController::class, 'change'])->name('change');

    Route::get('/profile' , [AuthController::class , 'profile'])->name('profile');
    Route::post('/storeuser' , [AuthController::class , 'storeuser'])->name('storeuser');
    Route::post('/updatePassword' , [AuthController::class , 'updatePassword'])->name('updatePassword');

});

Route::get('/', [HomeController::class, 'index'])->name('index');

Route::get('/products/{category}', [ProductController::class, 'products'])->name('products');
Route::get('/product/{product}', [ProductController::class, 'product'])->name('product');
Route::post('/filter',[ProductController::class , 'getFilter'])->name('filter');
Route::get('/album/{album}', [ProductController::class, 'album'])->name('album');

Route::get('/questions', [HomeController::class, 'questions'])->name('questions');
Route::post('/storefaqs', [HomeController::class, 'store'])->name('storefaqs');
Route::get('/contact', [SettingController::class, 'contact'])->name('contact');
Route::get('/joinus', [SettingController::class, 'joinus'])->name('joinus');

Route::get('/about', [SettingController::class, 'about'])->name('about');


Route::get('/forget' , [AuthController::class , 'forget'])->name('forget');
Route::post('/forgetData' , [AuthController::class , 'forgetData'])->name('forgetData');
Route::get('/sendcode/{user}' , [AuthController::class , 'sendcode'])->name('sendcode');
Route::post('/send/{user}' , [AuthController::class , 'passwordupdate'])->name('send');

Route::get('/active/{id}' , [AuthController::class , 'active'])->name('confirmation');
Route::post('/activeAccount/{id}' , [AuthController::class , 'activeAccount'])->name('activeAccount');

Route::get('/rules', [HomeController::class, 'rules'])->name('rules');
Route::get('/privacy', [SettingController::class, 'privacy'])->name('privacy');

Route::group(['middleware' =>'guest'] , function (){
Route::get('register' , [AuthController::class , 'register'])->name('register');
Route::post('store' , [AuthController::class , 'store'])->name('storeregister');
Route::get('login', [AuthController::class, 'login'])->name('login');
Route::post('login', [AuthController::class, 'checkLogin'])->name('post.login');
});
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
