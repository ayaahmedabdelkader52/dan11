<?php

use App\Http\Controllers\Dashboard\{AlbumImageController,
    AlbumsController,
    AudioController,
    CharacterController,
    CityController,
    AuthController,
    ComplainController,
    ContactUsController,
    CountryController,
    DashboardController,
    FaqsController,
    DistrictController,
    MessageController,
    OfferController,
    OrderController,
    PermissionController,
    NotificationController,
    CategoryController,
    ProductController,
    QuestionsController,
    RuleController,
    SingerController,
    Slidecontroller,
    SubcategoriesController,
    RegionController,
    ReportController,
    Settings\SocialMediaController,
    CouponController,
    SubjectController,
    UserController,
    Settings\SettingController,
    Settings\PackageController};

use App\Models\OrderProduct;


Route::group(['as' => 'admin.', 'middleware' => ['admin', 'role.check', 'locale']], function () {

    /** -------------- Home Start --------------- */
    Route::get('/', [
        'uses' => 'App\Http\Controllers\Dashboard\DashboardController@dashboard',
        'as' => 'admin',
        'title' => 'الصفحه الرئيسيه',
        'child' => [
            'admin.admin'
        ]
    ]);
    /** -------------- Home End --------------- */

    /** -------------- Settings Start --------------- */
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\Settings\SettingController@index',
            'as' => 'index',
            'title' => __('dashboard.main.settings'),
            'child' => [
                'admin.settings.index',
                'admin.settings.update',
                'admin.settings.update_profile',
                'admin.settings.update_password',
                'admin.settings.store_social_media',
                'admin.settings.update_social_media',
                'admin.settings.delete_social_media',
                'admin.settings.update_smtp',
                'admin.settings.update_sms',
                'admin.settings.update_fcm',
            ]

        ]);
        Route::post('/update/setting', [SettingController::class, 'update'])->name('update');
        Route::post('/update/profile/{user}', [SettingController::class, 'updateProfile'])->name('update_profile');
        Route::post('/update/password', [SettingController::class, 'updatePassword'])->name('update_password');

        Route::post('/store/social', [SocialMediaController::class, 'store'])->name('store_social_media');
        Route::put('/update/social/{social}', [SocialMediaController::class, 'update'])->name('update_social_media');
        Route::delete('/{social}', [SocialMediaController::class, 'destroy'])->name('delete_social_media');

        Route::post('smtp/update', [PackageController::class, 'smtp'])->name('update_smtp');
        Route::put('packages/update', [PackageController::class, 'updateSms'])->name('update_sms');
        Route::post('fcm/update', [PackageController::class, 'updateFcm'])->name('update_fcm');
    });
    /** -------------- Settings End --------------- */

    /** -------------- Reports Start --------------- */
    Route::group(['prefix' => 'reports'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\ReportController@index',
            'as' => 'reports.index',
            'title' => 'التقارير',
            'child' => [
                'admin.reports.index',
                'admin.reports.destroy',
                'admin.reports.destroy_selected',
            ]
        ]);

        Route::delete('/{report}', [ReportController::class, 'destroy'])->name('reports.destroy');
        Route::post('/', [ReportController::class, 'destroySelected'])->name('reports.destroy_selected');

    });
    /** -------------- Reports End --------------- */

    /** -------------- inbox-page Start --------------- */
    Route::group(['prefix' => 'inbox-page', 'as' => 'inbox.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\ContactUsController@index',
            'as' => 'index',
            'title' => 'البريد الالكتروني',
            'child' => [
                'admin.inbox.index',
                'admin.inbox.show',
                'admin.inbox.destroy',
                'admin.inbox.destroy_selected',
                'admin.inbox.reply_sms_to_single_user',
                'admin.inbox.reply_email_to_single_user',
                'admin.inbox.reply_notification_to_single_user',
            ]
        ]);
        Route::get('show/{contact}', [ContactUsController::class, 'show'])->name('show');
        Route::delete('/{contact}', [ContactUsController::class, 'destroy'])->name('destroy');
        Route::post('/delete-selected', [ContactUsController::class, 'destroySelected'])->name('destroy_selected');


        /** -------------- Reply message to Single User: Start --------------- */
        Route::post('/reply-sms-to-single-inbox', [ContactUsController::class, 'replySmsToSingleUser'])->name('reply_sms_to_single_user');
        Route::post('/reply-email-to-single-inbox', [ContactUsController::class, 'replyEmailToSingleUser'])->name('reply_email_to_single_user');
        Route::post('/reply-notification-to-single-inbox', [ContactUsController::class, 'replyNotificationToSingleUser'])->name('reply_notification_to_single_user');
        /** -------------- Reply message to Single User: End --------------- */
    });
    /** -------------- inbox-page End --------------- */

    /** -------------- complains Start --------------- */
    Route::group(['prefix' => 'complains', 'as' => 'complains.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\ComplainController@index',
            'as' => 'index',
            'title' => 'الشكاوي والمقترحات',
            'child' => [
                'admin.complains.index',
                'admin.complains.show',
                'admin.complains.destroy',
                'admin.complains.destroy_selected',
                'admin.complains.change_status',
                'admin.complains.reply_sms_to_single_user',
                'admin.complains.reply_email_to_single_user',
                'admin.complains.reply_notification_to_single_user',
            ]
        ]);
        Route::get('-show/{complain}', [ComplainController::class, 'show'])->name('show');
        Route::delete('/{complain}', [ComplainController::class, 'destroy'])->name('destroy');
        Route::post('/delete-selected-complain', [ComplainController::class, 'destroySelected'])->name('destroy_selected');
        Route::post('/change-status-complain/{complain}', [ComplainController::class, 'changeStatus'])->name('change_status');

        /** -------------- Reply message to Single User: Start --------------- */
        Route::post('/reply-sms-to-single-complain', [ComplainController::class, 'replySmsToSingleUser'])->name('reply_sms_to_single_user');
        Route::post('/reply-email-to-single-complain', [ComplainController::class, 'replyEmailToSingleUser'])->name('reply_email_to_single_user');
        Route::post('/reply-notification-to-single-complain', [ComplainController::class, 'replyNotificationToSingleUser'])->name('reply_notification_to_single_user');
        /** -------------- Reply message to Single User: End --------------- */
    });
    /** -------------- complains End --------------- */



    /** -------------- User Start --------------- */
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\UserController@index',
            'as' => 'index',
            'title' => 'الاعضاء',
            'child' => [
                'admin.users.index',
                'admin.users.blocked',
                'admin.users.active',
                'admin.users.inactive',
                'admin.users.male',
                'admin.users.female',
                'admin.users.create',
                'admin.users.downloadUsers',
                'admin.users.store',
                'admin.users.show',
                'admin.users.edit',
                'admin.users.update',
                'admin.users.destroy',
                'admin.users.destroy_selected',
                'admin.users.block_user',
                'admin.users.un_block_user',
                'admin.notification.create',
                'admin.users.send_sms_to_all',
                'admin.users.send_email_to_all',
                'admin.users.send_notification_to_all',
                'admin.users.send_sms_to_single_user',
                'admin.users.send_email_to_single_user',
                'admin.users.send_notification_to_single_user',
            ]
        ]);
        Route::get('/blocked', [UserController::class, 'blocked'])->name('blocked');
        Route::get('/active', [UserController::class, 'activeUsers'])->name('active');
        Route::get('/inactive', [UserController::class, 'inActiveUsers'])->name('inactive');
        Route::get('/male', [UserController::class, 'maleUsers'])->name('male');
        Route::get('/female', [UserController::class, 'femaleUsers'])->name('female');
        Route::get('/download-users', [UserController::class, 'downloadAllUsers'])->name('downloadUsers');
        Route::get('/create', [UserController::class, 'create'])->name('create');
        Route::post('/', [UserController::class, 'store'])->name('store');
        Route::get('/{user}', [UserController::class, 'show'])->name('show');
        Route::get('/{user}/edit', [UserController::class, 'edit'])->name('edit');
        Route::put('/{user}', [UserController::class, 'update'])->name('update');
        Route::delete('/{user}', [UserController::class, 'destroy'])->name('destroy');
        Route::post('/selected', [UserController::class, 'destroySelected'])->name('destroy_selected');
        Route::post('/block-user/{user}', [UserController::class, 'block'])->name('block_user');
        Route::post('/un-block-user/{user}', [UserController::class, 'unBlock'])->name('un_block_user');

        /** -------------- Send message to All User: Start --------------- */
        Route::post('/send-sms-to-all', [MessageController::class, 'smsToAllUsers'])->name('send_sms_to_all');
        Route::post('/send-email-to-all', [MessageController::class, 'emailAllUsers'])->name('send_email_to_all');
        Route::post('/send-notification-to-all', [MessageController::class, 'notificationAllUsers'])->name('send_notification_to_all');
        /** -------------- Send message to All User: End --------------- */
        //
        /** -------------- Send message to Single User: Start --------------- */
        Route::post('/send-sms-to-single-user', [MessageController::class, 'smsToSingleUser'])->name('send_sms_to_single_user');
        Route::post('/send-email-to-single-user', [MessageController::class, 'emailToSingleUser'])->name('send_email_to_single_user');
        Route::post('/send-notification-to-single-user', [MessageController::class, 'notificationToSingleUser'])->name('send_notification_to_single_user');
        /** -------------- Send message to Single User: End --------------- */
    });
    /** -------------- User End --------------- */

    /** -------------- Countries: Start --------------- */
    Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\CountryController@index',
            'as' => 'index',
            'title' => 'الدول',
            'child' => [
                'admin.countries.index',
                'admin.countries.create',
                'admin.countries.downloadCountries',
                'admin.countries.store',
                'admin.countries.show',
                'admin.countries.edit',
                'admin.countries.update',
                'admin.countries.destroy',
                'admin.countries.destroy_selected',
                'admin.countries.regions',
                'admin.countries.regions_create',
            ]
        ]);
        Route::resource('/', CountryController::class)->except('index');
        Route::get('/download-countries', [CountryController::class, 'downloadAllCountries'])->name('downloadCountries');
        Route::post('/selected', [CountryController::class, 'destroySelected'])->name('destroy_selected');
        // *** Regions: Start *** //
        Route::get('/regions/{country}', [RegionController::class, 'index'])->name('regions');
        Route::get('{country}/regions/create', [RegionController::class, 'create'])->name('regions_create');
        // *** Regions: End *** //
    });
    /** -------------- Countries: End --------------- */

    /** -------------- Regions Start --------------- */
    Route::group(['prefix' => 'regions', 'as' => 'regions.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\RegionController@index',
            'as' => 'index',
            'title' => 'المناطق',
            'child' => [
                'admin.regions.index',
                'admin.regions.create',
                'admin.regions.downloadCities',
                'admin.regions.store',
                'admin.regions.show',
                'admin.regions.edit',
                'admin.regions.update',
                'admin.regions.destroy',
                'admin.regions.destroy_selected',
                'admin.regions.cities',
                'admin.regions.cities_create',
            ]
        ]);
        // Route::get('/create/{country}', [RegionController::class, 'create'])->name('create');
        Route::get('regions/download', [RegionController::class, 'downloadAllRegions'])->name('downloadExcel');
        Route::resource('/', RegionController::class)->except('index', 'create');
        Route::post('/selected', [RegionController::class, 'destroySelected'])->name('destroy_selected');
        // *** Cities: Start *** //
        Route::get('/cities/{region}', [CityController::class, 'index'])->name('cities');
        Route::get('{region}/cities/create', [CityController::class, 'create'])->name('cities_create');
        // *** Cities: End *** //
    });
    /** -------------- Regions: End --------------- */

    /** -------------- Cities: Start --------------- */
    Route::group(['prefix' => 'cities', 'as' => 'cities.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\CityController@index',
            'as' => 'index',
            'title' => 'المدن',
            'child' => [
                'admin.cities.index',
                'admin.cities.create',
                'admin.cities.downloadCities',
                'admin.cities.store',
                'admin.cities.show',
                'admin.cities.edit',
                'admin.cities.update',
                'admin.cities.destroy',
                'admin.cities.destroy_selected',
                'admin.cities.districts',
                'admin.cities.districts_create',

            ]
        ]);
        Route::resource('/', CityController::class)->except('index', 'create');
        Route::get('/download', [CityController::class, 'downloadAllCities'])->name('downloadExcel');
        Route::post('/selected', [CityController::class, 'destroySelected'])->name('destroy_selected');
        // *** Districts: Start *** //
        Route::get('/districts/{city}', [DistrictController::class, 'index'])->name('districts');
        Route::get('{city}/districts/create', [DistrictController::class, 'create'])->name('districts_create');
        // *** Districts: End *** //
    });
    /** -------------- Cities: End --------------- */

    /** -------------- Districts Start --------------- */
    Route::group(['prefix' => 'districts', 'as' => 'districts.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\DistrictController@index',
            'as' => 'index',
            'title' => 'الأحياء',
            'child' => [
                'admin.districts.index',
                'admin.districts.create',
                'admin.districts.downloadCities',
                'admin.districts.store',
                'admin.districts.show',
                'admin.districts.edit',
                'admin.districts.update',
                'admin.districts.destroy',
                'admin.districts.destroy_selected',

            ]
        ]);
        Route::resource('/', DistrictController::class)->except('index', 'create');
        Route::get('/download', [DistrictController::class, 'downloadAllCities'])->name('downloadExcel');
        Route::post('/selected', [DistrictController::class, 'destroySelected'])->name('destroy_selected');
    });
    /** -------------- Districts End --------------- */

    /** -------------- Permissions Start --------------- */
    Route::group(['prefix' => 'permissions', 'as' => 'permissions.'], function () {

        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\PermissionController@index',
            'as' => 'index',
            'title' => 'الصلاحيات',
            'child' => [
                'admin.permissions.index',
                'admin.permissions.create',
                'admin.permissions.store',
                'admin.permissions.show',
                'admin.permissions.edit',
                'admin.permissions.update',
                'admin.permissions.destroy',
                'admin.permissions.destroy_selected',
            ]
        ]);
        Route::get('/create', [PermissionController::class, 'create'])->name('create');
        Route::post('/', [PermissionController::class, 'store'])->name('store');
        Route::get('/{role}', [PermissionController::class, 'show'])->name('show');
        Route::get('/{role}/edit', [PermissionController::class, 'edit'])->name('edit');
        Route::put('/{role}', [PermissionController::class, 'update'])->name('update');
        Route::delete('/{role}', [PermissionController::class, 'destroy'])->name('destroy');
        Route::post('/selected', [PermissionController::class, 'destroySelected'])->name('destroy_selected');
    });
    /** -------------- Permissions End --------------- */

    /** -------------- Rules Start --------------- */
    Route::group(['prefix' => 'rules', 'as' => 'rules.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\RuleController@index',
            'as'=>'index',
            'title'=>'الاحكام والشروط',
            'child'=>[
                'admin.rules.index',
                'admin.rules.create',
                'admin.rules.store',
                'admin.rules.edit',
                'admin.rules.update',
                'admin.rules.destroy',
                'admin.rules.destroy_selected',
            ],
            ]);
        Route::get('/create' , [RuleController::class, 'create'])->name('create');
        Route::post('/store' , [RuleController::class, 'store'])->name('store');
        Route::get('/edit/{rule}' , [RuleController::class, 'edit'])->name('edit');
        Route::post('/update/{rule}' , [RuleController::class, 'update'])->name('update');
        Route::get('/destroy/{rule}' , [RuleController::class, 'destroy'])->name('destroy');
        Route::post('destroy' , [RuleController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Rules End --------------- */

    /** -------------- Questions Start --------------- */
    Route::group(['prefix' => 'questions', 'as' => 'questions.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\QuestionsController@index',
            'as'=>'index',
            'title'=>'الاسئله',
            'child'=>[
                'admin.questions.index',
                'admin.questions.create',
                'admin.questions.store',
                'admin.questions.edit',
                'admin.questions.update',
                'admin.questions.destroy',
                'admin.questions.destroy_selected',
            ],
            ]);
        Route::get('/create' , [QuestionsController::class, 'create'])->name('create');
        Route::post('/store' , [QuestionsController::class, 'store'])->name('store');
        Route::get('/edit/{question}' , [QuestionsController::class, 'edit'])->name('edit');
        Route::post('/update/{question}' , [QuestionsController::class, 'update'])->name('update');
        Route::get('/destroy/{question}' , [QuestionsController::class, 'destroy'])->name('destroy');
        Route::post('destroy' , [QuestionsController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Rules End --------------- */

    /** -------------- Orders Start --------------- */
    Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\OrderController@index',
            'as' => 'index',
            'title' => 'الطلبات',
            'child' => [
                'admin.orders.index',
                'admin.orders.show',
                'admin.orders.destroy',
                'admin.orders.destroy_selected',
            ]
        ]);

        Route::get('/show/{order}' , [OrderController::class, 'show'])->name('show');
        Route::get('/edit/{category}' , [OrderController::class, 'edit'])->name('edit');
        Route::post('/update/{category}' , [OrderController::class, 'update'])->name('update');
        Route::get('/destroy/{category}' , [OrderController::class, 'destroy'])->name('destroy');
        Route::post('destroy' , [OrderController::class, 'destroySelected'])->name('destroySelected');

    });

    /** -------------- Orders End --------------- */

  /** -------------- Coupons Start --------------- */
    Route::group(['prefix' => 'coupons', 'as' => 'coupons.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\CouponController@index',
            'as' => 'index',
            'title' => 'اكواد الخصم',
            'child' => [
                'admin.coupons.index',
                'admin.coupons.edit',
                'admin.coupons.create',
                'admin.coupons.update',
                'admin.coupons.store',
                'admin.coupons.destroy',
                'admin.coupons.destroy_selected',
            ]
        ]);

        Route::get('/create' , [CouponController::class, 'create'])->name('create');
        Route::post('/store' , [CouponController::class, 'store'])->name('store');
        Route::get('/edit/{category}' , [CouponController::class, 'edit'])->name('edit');
        Route::post('/update/{category}' , [CouponController::class, 'update'])->name('update');
        Route::get('/destroy/{category}' , [CouponController::class, 'destroy'])->name('destroy');
        Route::post('destroy' , [CouponController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Coupons End --------------- */

    /** -------------- Category Start --------------- */
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\CategoryController@index',
            'as'=>'index',
            'title'=>'الاقسام الرئيسيه',
            'child'=>[
                'admin.categories.index',
                'admin.categories.create',
                'admin.categories.store',
                'admin.categories.show',
                'admin.categories.edit',
                'admin.categories.update',
                'admin.categories.destroy',
                'admin.categories.destroy_selected',
            ],
            ]);
        Route::get('/create' , [CategoryController::class, 'create'])->name('create');
        Route::post('/store' , [CategoryController::class, 'store'])->name('store');
        Route::get('/show' , [CategoryController::class, 'show'])->name('show');
        Route::get('/edit/{category}' , [CategoryController::class, 'edit'])->name('edit');
        Route::post('/update/{category}' , [CategoryController::class, 'update'])->name('update');
        Route::get('/destroy/{category}' , [CategoryController::class, 'destroy'])->name('destroy');
        Route::post('destroy' , [CategoryController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Category End --------------- */

 /** -------------- Offers Start --------------- */
    Route::group(['prefix' => 'offers', 'as' => 'offers.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\OfferController@index',
            'as'=>'index',
            'title'=>'التخفيضات',
            'child'=>[
                'admin.offers.index',
                'admin.offers.create',
                'admin.offers.store',
                'admin.offers.edit',
                'admin.offers.update',
                'admin.offers.destroy',
                'admin.offers.destroy_selected',
            ],
            ]);
        Route::get('/create' , [OfferController::class, 'create'])->name('create');
        Route::post('/store' , [OfferController::class, 'store'])->name('store');
        Route::get('/edit/{offer}' , [OfferController::class, 'edit'])->name('edit');
        Route::post('/update/{offer}' , [OfferController::class, 'update'])->name('update');
        Route::get('/destroy/{offer}' , [OfferController::class, 'destroy'])->name('destroy');
        Route::post('destroy' , [OfferController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Offers End --------------- */


    /** -------------- Subcategory Start --------------- */
      Route::group(['prefix' => 'subcategories', 'as' => 'subcategories.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\SubcategoriesController@index',
                        'as'=>'index',
                        'title'=>'الاقسام الفرعيه',
                        'child'=>[
                            'admin.subcategories.index',
                            'admin.subcategories.create',
                            'admin.subcategories.store',
                            'admin.subcategories.show',
                            'admin.subcategories.edit',
                            'admin.subcategories.update',
                            'admin.subcategories.destroy',
                            'admin.subcategories.destroySelected',
            ],
        ]);

        Route::get('create', [SubcategoriesController::class , 'create'])->name('create');
        Route::post( '/store' ,   [SubcategoriesController::class , 'store'])->name('store');
        Route::get( '/show/{subcategory}' ,   [SubcategoriesController::class , 'show'])->name('show');
        Route::get( '/edit/{subcategory}' , [SubcategoriesController::class , 'edit'])->name('edit');
        Route::post( '/update/{subcategory}' , [SubcategoriesController::class , 'update'])->name('update');
        Route::delete( '/destroy/{subcategory}' , [SubcategoriesController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [SubcategoriesController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Subcategory End --------------- */


     /** -------------- Product Start --------------- */
      Route::group(['prefix' => 'products', 'as' => 'products.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\ProductController@index',
                        'as'=>'index',
                        'title'=>'الزفات',
                        'child'=>[
                            'admin.products.index',
                            'admin.products.create',
                            'admin.products.store',
                            'admin.products.show',
                            'admin.products.edit',
                            'admin.products.update',
                            'admin.products.destroy',
                            'admin.products.destroySelected',
            ],
        ]);

        Route::get('create', [ProductController::class , 'create'])->name('create');
        Route::post( '/store' ,   [ProductController::class , 'store'])->name('store');
        Route::get( '/show/{product}' ,   [ProductController::class , 'show'])->name('show');
        Route::get( '/edit/{product}' , [ProductController::class , 'edit'])->name('edit');
        Route::post( '/update/{product}' , [ProductController::class , 'update'])->name('update');
        Route::get( '/destroy/{product}' , [ProductController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [ProductController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Product End --------------- */


     /** -------------- Album Start --------------- */
      Route::group(['prefix' => 'albums', 'as' => 'albums.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\AlbumsController@index',
                        'as'=>'index',
                        'title'=>'البوم الصور',
                        'child'=>[
                            'admin.albums.index',
                            'admin.albums.create',
                            'admin.albums.store',
                            'admin.albums.show',
                            'admin.albums.edit',
                            'admin.albums.update',
                            'admin.albums.destroy',
                            'admin.albums.destroySelected',
            ],
        ]);

        Route::get('create', [AlbumsController::class , 'create'])->name('create');
        Route::post( '/store' ,   [AlbumsController::class , 'store'])->name('store');
        Route::get( '/show/{album}' ,   [AlbumsController::class , 'show'])->name('show');
        Route::get( '/edit/{album}' , [AlbumsController::class , 'edit'])->name('edit');
        Route::post( '/update/{album}' , [AlbumsController::class , 'update'])->name('update');
        Route::get( '/destroy/{album}' , [AlbumsController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [AlbumsController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Album End --------------- */


/** -------------- Album Image Start --------------- */
      Route::group(['prefix' => 'albumimages', 'as' => 'albumimages.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\AlbumImageController@index',
                        'as'=>'index',
                        'title'=>'الصور',
                        'child'=>[
                            'admin.albumimages.index',
                            'admin.albumimages.create',
                            'admin.albumimages.store',
                            'admin.albumimages.edit',
                            'admin.albumimages.update',
                            'admin.albumimages.destroy',
                            'admin.albumimages.destroySelected',
            ],
        ]);

        Route::get('create', [AlbumImageController::class , 'create'])->name('create');
        Route::post( '/store' ,   [AlbumImageController::class , 'store'])->name('store');
        Route::get( '/edit/{photo}' , [AlbumImageController::class , 'edit'])->name('edit');
        Route::post( '/update/{photo}' , [AlbumImageController::class , 'update'])->name('update');
        Route::delete( '/destroy/{photo}' , [AlbumImageController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [AlbumImageController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Album Image End --------------- */


    /** -------------- Subjects Start --------------- */
      Route::group(['prefix' => 'subjects', 'as' => 'subjects.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\SubjectController@index',
                        'as'=>'index',
                        'title'=>'المناسبات',
                        'child'=>[
                            'admin.subjects.index',
                            'admin.subjects.create',
                            'admin.subjects.store',
                            'admin.subjects.edit',
                            'admin.subjects.update',
                            'admin.subjects.destroy',
                            'admin.subjects.destroySelected',
            ],
        ]);

        Route::get('create', [SubjectController::class , 'create'])->name('create');
        Route::post( '/store' ,   [SubjectController::class , 'store'])->name('store');
        Route::get( '/edit/{subject}' , [SubjectController::class , 'edit'])->name('edit');
        Route::post( '/update/{subject}' , [SubjectController::class , 'update'])->name('update');
        Route::delete( '/destroy/{subject}' , [SubjectController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [SubjectController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Subjects End --------------- */

    /** -------------- Characters Start --------------- */
      Route::group(['prefix' => 'characters', 'as' => 'characters.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\CharacterController@index',
                        'as'=>'index',
                        'title'=>'اسماء في الزفات',
                        'child'=>[
                            'admin.characters.index',
                            'admin.characters.create',
                            'admin.characters.store',
                            'admin.characters.edit',
                            'admin.characters.update',
                            'admin.characters.destroy',
                            'admin.characters.destroySelected',
            ],
        ]);

        Route::get('create', [CharacterController::class , 'create'])->name('create');
        Route::post( '/store' ,   [CharacterController::class , 'store'])->name('store');
        Route::get( '/edit/{character}' , [CharacterController::class , 'edit'])->name('edit');
        Route::post( '/update/{character}' , [CharacterController::class , 'update'])->name('update');
        Route::delete( '/destroy/{character}' , [CharacterController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [CharacterController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Characters End --------------- */

    /** -------------- Singers Start --------------- */
      Route::group(['prefix' => 'singers', 'as' => 'singers.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\SingerController@index',
                        'as'=>'index',
                        'title'=>'مغنيين',
                        'child'=>[
                            'admin.singers.index',
                            'admin.singers.create',
                            'admin.singers.store',
                            'admin.singers.edit',
                            'admin.singers.update',
                            'admin.singers.destroy',
                            'admin.singers.destroySelected',
            ],
        ]);

        Route::get('create', [SingerController::class , 'create'])->name('create');
        Route::post( '/store' ,   [SingerController::class , 'store'])->name('store');
        Route::get( '/edit/{singer}' , [SingerController::class , 'edit'])->name('edit');
        Route::post( '/update/{singer}' , [SingerController::class , 'update'])->name('update');
        Route::delete( '/destroy/{singer}' , [SingerController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [SingerController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Singers End --------------- */






    /** -------------- Audio Start --------------- */

      Route::group(['prefix' => 'audio', 'as' => 'audio.'], function () {

                    Route::get('/', [
                        'uses'=>'App\Http\Controllers\Dashboard\AudioController@index',
                        'as'=>'index',
                        'title'=>'الملفات الصوتيه',
                        'child'=>[
                            'admin.audio.index',
                            'admin.audio.create',
                            'admin.audio.store',
                            'admin.audio.edit',
                            'admin.audio.update',
                            'admin.audio.destroy',
                            'admin.audio.destroySelected',
            ],
        ]);
        Route::get('create', [AudioController::class , 'create'])->name('create');
        Route::post( '/store' ,   [AudioController::class , 'store'])->name('store');
        Route::get( '/edit/{audio}' , [AudioController::class , 'edit'])->name('edit');
        Route::post( '/update/{audio}' , [AudioController::class , 'update'])->name('update');
        Route::get( '/destroy/{audio}' , [AudioController::class , 'destroy'])->name('destroy');
        Route::post('/selected', [AudioController::class, 'destroySelected'])->name('destroySelected');
    });

    /** -------------- Audio End --------------- */






    /** -------------- Notifications Start --------------- */
    Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\NotificationController@index',
            'as' => 'index',
            'title' => 'الاشعارات',
            'child' => [
                'admin.notification.create',
                'admin.notification.store_sms',
                'admin.notification.store_email',
                'admin.notification.store_notification',
            ]
        ]);
        Route::get('/create', [NotificationController::class, 'create'])->name('create');
        Route::post('/store-sms', [NotificationController::class, 'sendSmsMessage'])->name('store_sms');
        Route::post('/store-email', [NotificationController::class, 'sendEmail'])->name('store_email');
        Route::post('notification/store', [NotificationController::class, 'adminSendNotification'])->name('store_notification');
    });
    /** -------------- Notifications End --------------- */

    /** -------------- Slids Start --------------- */
    Route::group(['prefix' => 'slids' , 'as' => 'slids.'], function () {
        Route::get('/index', [
            'uses' => 'App\Http\Controllers\Dashboard\SlideController@index',
            'as' => 'index',
            'title' => 'خلفية الموقع',
            'child' => [
                'admin.slids.index',
                'admin.slids.create',
                'admin.slids.store',
                'admin.slids.edit',
                'admin.slids.update',
                'admin.slids.destroy',
                'admin.slids.destroy_selected',
            ]
        ]);
        Route::get('/create', [SlideController::class , 'create'])->name('create');
        Route::post( '/store' ,   [SlideController::class , 'store'])->name('store');
        Route::get( '/edit/{slide}' , [SlideController::class , 'edit'])->name('edit');
        Route::post( '/update' , [SlideController::class , 'update'])->name('update');
        Route::get( '/destroy/{product}' , [SlideController::class , 'destroy'])->name('destroy');
        Route::post('destroy/selected', [SlideController::class, 'destroySelected'])->name('destroy_selected');
    });
    /** -------------- Slids End --------------- */



    /** -------------- Faqs Start --------------- */
    Route::group(['prefix' => 'faqs'], function () {
        Route::get('/index', [
            'uses' => 'App\Http\Controllers\Dashboard\FaqsController@index',
            'as' => 'faqs.index',
            'title' => 'الاسئله الشائعه',
            'child' => [
                'admin.faqs.index',
                'admin.faqs.create',
                'admin.faqs.store',
                'admin.faqs.edit',
                'admin.faqs.update',
                'admin.faqs.destroy',
                'admin.faqs.destroy_selected',
            ]
        ]);
        Route::resource('faqs', FaqsController::class)->except('index');
        Route::post('destroy/selected', [FaqsController::class, 'destroySelected'])->name('faqs.destroy_selected');
    });
    /** -------------- Faqs End --------------- */
    #new_routes_here


});


Route::group(['as' => 'admin.'], function () {
    Route::get('them/{them}', [DashboardController::class, 'them'])->name('them');
    Route::get('change/{language}', [SettingController::class, 'changeLanguage'])->name('change.language');

    Route::get('login', [AuthController::class, 'login'])->name('login')->middleware('guest');
    Route::post('login', [AuthController::class, 'postLogin'])->name('post.login');
    Route::post('logout/admin', [AuthController::class, 'logoutAdmin'])->name('logout');
});
