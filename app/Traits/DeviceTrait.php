<?php

namespace App\Traits;


use App\Models\Device;

trait DeviceTrait
{
    public function createOrUpdateDevice($data)
    {
        $device = Device::where(['device_id' => request('device_id'), 'device_type' => request('device_type')])->first();
        if ($device) {
            $device->update([
                'user_id' => $this->id,
                'device_id' => $data['device_id'],
                'device_type' => $data['device_type'],
            ]);
        } else {
            Device::create([
                'user_id' => $this->id,
                'device_id' => $data['device_id'],
                'device_type' => $data['device_type'],
            ]);
        }
    }

    public function deleteDevice($request)
    {
        $device = Device::where(['user_id' => $this->id, 'device_id' => $request['device_id']])->first();

        if ($device) {
            $device->delete();
        }
    }

    public function deleteDevices()
    {
        $devices = Device::where(['user_id' => $this->id])->get();

        foreach ($devices as $device) {
            $device->delete();
        }
    }
}
