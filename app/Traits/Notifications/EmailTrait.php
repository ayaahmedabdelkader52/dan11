<?php

namespace App\Traits\Notifications;

use App\Jobs\Notifications\EmailJob;
use App\Models\MessagePackage;

trait EmailTrait
{

    public function sendEmailMessage($email, $data)
    {
        $job = new EmailJob($email, $data);
        dispatch($job);
    }

    public function checkEmailConfig()
    {
        $checkConfig = MessagePackage::where('type', 'smtp')->first();
        if (!$checkConfig)
            throw new \Exception('Your message dos not sent, please Check your SMTP data in Admin Settings !');
        if (
            $checkConfig->username == "" ||
            $checkConfig->password == "" ||
            $checkConfig->sender_email == "" ||
            $checkConfig->port == "" ||
            $checkConfig->host == ""
        ) {
            throw new \Exception('Your message dos not sent, please Check your SMTP data in Admin Settings !');
        }
        return true;
    }
}
