<?php

namespace App\Traits\Notifications;

use App\Jobs\Notifications\SmsJob;
use App\Models\MessagePackage;
use Illuminate\Support\Facades\DB;

trait SmsTrait
{
    public function sendSms($phone, $msg)
    {
        $activePackage = DB::table('message_packages')
            ->where('active', 'true')
            ->whereIn('type', MessagePackage::SMS_PACKAGES)
            ->first();

        if (!$activePackage) {
            return throw new \Exception('Your message dos not sent, please Check your SMS data in Admin Settings !');
        }

        $service = sprintf("App\\Services\\Messaging\\%sService", ucfirst($activePackage->type));
        if (!is_a($service, \App\Contracts\MessagingService::class, true)) {
            return throw new \Exception('Invalid Service, Check That Service must be the name in the array concatenating with Service. You can read more in README file');
        }

        $job = new SmsJob($phone, $msg, $activePackage, $service);
        dispatch($job);
    }

}
