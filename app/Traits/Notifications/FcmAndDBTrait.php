<?php

namespace App\Traits\Notifications;

use App\Jobs\Notifications\DatabaseJob;
use App\Jobs\Notifications\FcmJob;
use App\Models\MessagePackage;

trait FcmAndDBTrait
{
    /**
     * `devices` must have `user_id`, `device_id`, and `device_type`
     * `data` must have `title`,`key`, `message`, `image`, and `data`
     */
    public function sendNotification($devices, array $data)
    {
        $notifications = [];
        foreach ($devices as $device) {
            $notifications[] = [
                'user_id' => $device['user_id'],
                'notifier_id' => auth()->id(),
                'title' => $data['title'],
                'message' => $data['message'],
                'key' => $data["key"],
                'image' => $data['image'] ?? '',
                'data' => json_encode($data['data'] ?? []),
                'created_at' => date('y-m-d g:i'),
                'updated_at' => date('y-m-d g:i'),
            ];

        }

        $dbNotifications = array_unique($notifications, SORT_REGULAR);
        $this->sendToDatabase($dbNotifications);

        $this->sendToFcm($devices, $data);
    }

    private function sendToDatabase($notifications)
    {
        $job = new DatabaseJob($notifications);
        dispatch($job);
    }

    private function sendToFcm($devices, $data)
    {
        $this->checkFcmConfig();

        if (count($devices) > 0) {
            $job = new FcmJob($devices, $data);
            dispatch($job);
        }
    }

    private function checkFcmConfig()
    {
        $fcm = MessagePackage::where('type', 'fcm')->first();
        if (!$fcm)
            throw new \Exception('Your message dos not sent, please Check your FCM data in Admin Settings !');
        if (
            $fcm->server_key == "" ||
            $fcm->sender_id == ""
        ) {
            throw new \Exception('Your message dos not sent, please Check your FCM data in Admin Settings !');
        }
        return true;
    }

}
