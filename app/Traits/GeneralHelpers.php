<?php

namespace App\Traits;

use App\Models\Setting;

trait GeneralHelpers
{
    function setting($key = false)
    {
        if ($key != false) {
            if ($s = Setting::where('key', '=', $key)->first()) {
                return $s->value;
            }
            return false;
        }
        return false;
    }

    function convert2english($string)
    {
        $newNumbers = range(0, 9);
        $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        return str_replace($arabic, $newNumbers, $string);
    }


}
