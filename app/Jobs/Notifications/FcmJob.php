<?php

namespace App\Jobs\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class FcmJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $iosDevices = [];
    private $androidDevices = [];
    private $data;

    public function __construct($devices, $data)
    {
        $this->setDevices($devices);

        $this->data = $data;
    }

    public function handle()
    {
        if (count($this->androidDevices) > 0) {
            $this->sendToAndroid();
        } elseif (count($this->iosDevices) > 0) {
            $this->sendToIos();
        }
    }

    public function sendToAndroid()
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($this->data['title']);
        $notificationBuilder->setBody($this->data['message'])->setSound('default');

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($this->data);
        $dataBuilt = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($this->androidDevices, $option, null, $dataBuilt);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
    }

    private function sendToIos()
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($this->data['title']);
        $notificationBuilder->setBody($this->data['message'])->setSound('default');

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($this->data);
        $dataBuilt = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($this->iosDevices, $option, $notification, $dataBuilt);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
    }

    private function setDevices($devices)
    {
        $this->iosDevices = $devices->filter(function ($item) {
            return $item->device_type == 'ios';
        })->pluck('device_id')->toArray();

        $this->androidDevices = $devices->filter(function ($item) {
            return $item->device_type == 'android';
        })->pluck('device_id')->toArray();

    }
}
