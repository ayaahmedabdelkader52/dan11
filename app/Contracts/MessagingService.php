<?php

namespace App\Contracts;

interface MessagingService
{
    public function send($phone, $msg, $data);
}
