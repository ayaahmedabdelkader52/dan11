<?php

namespace App\Providers;

use App\Exceptions\ApplicationIsCompletedException;
use App\Models\MessagePackage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class PackagesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        try {
            $fcm = MessagePackage::where('type', 'fcm')->first();
            $smtp = MessagePackage::where('type', 'smtp')->first();

            Config::set('fcm.http.server_key', ($fcm) ? $fcm->server_key : "");
            Config::set('fcm.http.sender_id', ($fcm) ? $fcm->sender_id : "");

            Config::set('mail.driver', 'smtp');
            Config::set('mail.host', ($smtp) ? $smtp->host : "");
            Config::set('mail.port', ($smtp) ? $smtp->port : "");
            Config::set('mail.from.address', ($smtp) ? $smtp->sender_email : "");
            Config::set('mail.from.name', ($smtp) ? $smtp->sender_name : "");
            Config::set('mail.encryption', ($smtp) ? $smtp->encryption : "");
            Config::set('mail.username', ($smtp) ? $smtp->username : "");
            Config::set('mail.password', ($smtp) ? $smtp->password : "");
        } catch (\Exception $exception) {
            // return new ApplicationIsCompletedException($exception->getMessage());
        }
    }
}
