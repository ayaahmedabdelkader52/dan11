<?php

namespace App\Providers;

use App\Traits\PermissionTrait;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    use PermissionTrait;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('hasPermission', function ($route) {
            return $this->authHasPermission($route);
        });
    }
}
