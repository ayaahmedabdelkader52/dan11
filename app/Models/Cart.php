<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class Cart
{
    use HasFactory;

    public $items;
    public $totalQty=0 ;
    public $totalPrice=0 ;

    public function __construct($oldCart)
    {
        if ($oldCart){
        $this->items =$oldCart -> items ;
        $this->totalQty = $oldCart -> totalQty ;
        $this->totalPrice = $oldCart -> totalPrice ;
        }
    }
    public function add($item , $id)
    {
        $storeditem = ['qty'=>0 , 'price' => $item->price_after , 'item'=>$item];
        if ($this->items){

            if(array_key_exists($id , $this->items)){
                $storeditem = $this->items[$id];
            }

        }
        $storeditem['qty']++ ;
        $storeditem['price'] = $item['price'] * $storeditem['qty'] ;
        $this->items[$id]=$storeditem;
        $this->totalQty++ ;
        $this->totalPrice += $item -> price_after ;
    }
    public function reduceByOne ($id)
    {
        $this->items[$id]['qty'] -- ;
        $this->items[$id]['price_after'] =- $this->items[$id]['item']['price_after'];
        $this->totalQty -- ;
        $this->totalPrice =- $this->items[$id]['item']['price_after'];

        if($this->items[$id]['qty'] <= 0)
        {
            unset($this->items[$id]);
        }
    }

    public function removeItem($id)
    {
        $this->totalQty -= $this->items[$id]['item']['qty'] ;
        $this->totalPrice += $this->items[$id]['item']['price_after'] ;
        unset($this->items[$id]);
    }
}
