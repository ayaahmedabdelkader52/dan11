<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessagePackage extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'username',
        'password',
        'sender_email',
        'sender_name',
        'port',
        'host',
        'encryption',
        'number',
        'application_id',
        'authorization',
        'server_key',
        'sender_id',
        'active',
    ];

    const SMS_PACKAGES = [
        'forjawally',
        'gateway',
        'hisms',
        'mesegat',
        'mobily',
        'oursms',
        'unifonic',
        'yamamah',
        'zain',
    ];
}
