<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    use HasFactory;
    protected $fillable=[
        'name_ar',
        'name_en',
        'audio',

    ];

    public function products ()
    {
        return $this -> belongsToMany(Product::class , 'product_audio' );
    }
    public function getAudioPathAttribute()
    {
        return asset('assets/uploads/audio/' . $this -> audio);
    }
}
