<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;
        protected $fillable = [
            'qty' ,
            'product_id' ,
            'order_id' ,
            'price',
            'audio_id',
            'singer_id',
            'character_id',
            'subject_id',
        ];

}
