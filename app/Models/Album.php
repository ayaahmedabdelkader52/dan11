<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;
    protected $fillable = [

        'name_en',
        'name_ar',
        'photo',

        'offer',
        'price_before',
        'price_after',
        'start',
        'end',
        'category_id',
        'subcategory_id',
    ];
    public function getPhotoPathAttribute()
    {
        return asset('assets/uploads/albums/' . $this->photo);
    }
    public function category()
    {
        return $this -> belongsTo(Category::class , 'category_id' , 'id');
    }
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class , 'subcategory_id' , 'id');
    }

    public function photos(){
        return $this->hasMany(AlbumImage::class );
    }
    public function orders ()
    {
        return $this -> belongsToMany(Order::class , 'order_products' );
    }
}
