<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumImage extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_ar',
        'name_en',
        'photo',
        'album_id',
    ];
    public function album()
    {
        return $this -> belongsTo(Album::class);
    }
    public function getPhotoPathAttribute()
    {
        return asset('assets/uploads/albums/' . $this -> photo);
    }
}
