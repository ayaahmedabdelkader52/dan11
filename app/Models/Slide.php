<?php

namespace App\Models;

use App\Traits\DeviceTrait;
use App\Traits\Imageable;
use App\Traits\Notifications\Notifiable;
use App\Traits\ReportTrait;
use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Slide extends Model
{
    use HasApiTokens, HasFactory, ReportTrait, DeviceTrait;
    use Notifiable;

   protected $table = 'slides' ;

    protected $fillable = [
        'title_ar',
        'title_en',
        'image',
    ];
    public function create()
    {
        return view('admin.slids.create');
    }
    public function getImagePathAttribute()
    {
        return asset('assets/uploads/slids/' . $this -> image );
    }

}
