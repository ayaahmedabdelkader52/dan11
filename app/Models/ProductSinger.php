<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSinger extends Model
{

    use HasFactory;
    protected $fillable =[
        'singer_id',
        'product_id',
    ];
}
