<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Singer extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_ar',
        'name_en',

    ];


    public function products ()
    {
        return $this -> belongsToMany(Product::class , 'product_singers' );
    }
}
