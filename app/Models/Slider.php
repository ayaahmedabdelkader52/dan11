<?php

namespace App\Models;

use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Spatie\Translatable\HasTranslations;

class Slider extends Model
{
    use HasFactory, HasTranslations;
    use Uploadable;

    protected $fillable = ['title', 'body', 'image', 'order'];
    protected array $translatable = ['title', 'body'];

    public function getImagePathAttribute(): string
    {
        return asset('assets/uploads/sliders/' . $this->image);
    }

    public function setImageAttribute($image)
    {
        if ($this->image && $this->image != 'default.png') {
            File::delete(public_path('assets/uploads/sliders/' . $this->image));
        }
        $this->attributes['image'] = $this->uploadOne($image, 'sliders', true);
    }
}
