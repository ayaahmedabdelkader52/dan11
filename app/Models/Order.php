<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
      'name',
      'user_id',
      'phone',
      'extra_phone',
      'cart',
      'address',
        'type_payment',
        'order_date',
        'country',
        'city',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class );
    }

    public function products()
    {
        return $this -> belongsToMany(product::class , 'order_products' );
    }
    public function albums()
    {
        return $this -> belongsToMany(Album::class , 'order_products' );

    }
}
