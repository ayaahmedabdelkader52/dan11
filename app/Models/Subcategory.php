<?php

namespace App\Models;

use App\Traits\DeviceTrait;
use App\Traits\Notifications\Notifiable;
use App\Traits\ReportTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Subcategory extends Model
{
    use HasApiTokens, HasFactory, ReportTrait, DeviceTrait;
    use Notifiable;

    protected $fillable =[
        'name_ar',
        'name_en',
        'photo',
        'category_id',
    ];

    public function categories()
    {
        return $this ->belongsTo(Category::class , 'category_id' , 'id');
    }
    public function products()
    {
        return $this ->hasMany(Product::class , 'subcategory_id' , 'id');
    }
    public function albums()
    {
        return $this ->hasMany(Album::class , 'subcategory_id' , 'id');
    }

    public function getPhotoPathAttribute()
    {
        return asset('assets/uploads/subcategories/' . $this->photo);
    }





}
