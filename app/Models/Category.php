<?php

namespace App\Models;

use App\Notifications\SendVerificationCode;
use App\Traits\DeviceTrait;
use App\Traits\Notifications\Notifiable;
use App\Traits\ReportTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Category extends Authenticatable
{

    use HasApiTokens, HasFactory, ReportTrait, DeviceTrait;
    use Notifiable;
    protected $table='categories';

    protected $fillable=[
         'name_en',
         'name_ar',
         'description_ar',
         'description_en',
         'photo',
     ];
    public function getPhotoPathAttribute()
    {
        return asset('assets/uploads/categories/' . $this->photo);
    }
public function offer()
{
    return $this->hasOne(Offer::class,'category_id' , 'id');
}
 public function subcategories(){
      return  $this -> hasMany(Subcategory::class ,'category_id' , 'id');
 }
 public function products(){
      return  $this -> hasMany(Product::class ,'category_id' , 'id');
 }
 public function albums(){
      return  $this -> hasMany(Album::class ,'category_id' , 'id');
 }

}
