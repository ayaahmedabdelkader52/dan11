<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable =[
        'name_en',
        'name_ar',
        'photo',
        'description_en',
        'description_ar',
        'price',
        'offer',
        'price_before',
        'price_after',
        'start',
        'end',
        'category_id',
        'subcategory_id',
    ];

    public function category()
    {
        return $this -> belongsTo(Category::class , 'category_id' , 'id');
    }
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class , 'subcategory_id' , 'id');
    }
    public function photo()
    {
        return $this -> hasMany(Album::class , 'product_id' , 'id');
    }

    public function audio()
    {
        return $this -> belongsToMany(Audio::class , 'product_audio' );
    }

    public function getPhotoPathAttribute()
    {
        return asset('assets/uploads/products/' . $this->photo);
    }
    public function singers ()
    {
        return $this -> belongsToMany(Singer::class , 'product_singers' );
    }
    public function characters ()
    {
        return $this -> belongsToMany(Character::class , 'product_characters' );
    }
    public function subjects ()
    {
        return $this -> hasMany(Subject::class , 'product_id' , 'id' );
    }
    public function orders ()
    {
        return $this -> belongsToMany(Order::class , 'order_products' );
    }
}
