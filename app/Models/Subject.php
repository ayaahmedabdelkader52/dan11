<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_ar',
        'name_en',
        'product_id',
    ];


    public function products ()
    {
        return $this -> belongsTo(Product::class , 'product_id' , 'id');
    }
}
