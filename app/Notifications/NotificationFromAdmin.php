<?php

namespace App\Notifications;

class NotificationFromAdmin
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function via()
    {
        return ['database', 'fcm'];
    }

    /**
     * `message`, `key`, and `message`  key is required
     */
    public function toDatabase()
    {
        return [
            'notifier_id' => auth()->id(),
            'title' => $this->request['title'],
            'message' => $this->request['message'],
            'data' => json_encode([]),
            'key' => 'from_admin',
            'created_at' => date('y-m-d g:i'),
            'updated_at' => date('y-m-d g:i'),
        ];
    }

    /**
     * `title`, `message` key is required
     */
    public function toFcm()
    {
        return [
            'key' => 'from_admin',
            'title' => $this->request['title'],
            'message' => $this->request['message'],
        ];
    }

}
