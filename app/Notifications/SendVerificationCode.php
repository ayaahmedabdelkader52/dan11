<?php

namespace App\Notifications;

use App\Traits\GeneralHelpers;

class SendVerificationCode
{
    use GeneralHelpers;

    private string $msg;

    public function __construct($code)
    {
        $msg = __('auth.your_code') . ' ' . $this->setting('site_name') . ':';
        $this->msg = $msg . $code;
    }

    public function via()
    {
        return ['sms'];
    }

    /**
     * `message` key is required
     */
    public function toSms()
    {
        return [
            'message' => $this->msg,
        ];
    }
}
