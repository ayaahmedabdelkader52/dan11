<?php

namespace App\Notifications;

class EmailFromAdmin
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function via()
    {
        return ['email'];
    }

    /**
     * `message` key is required
     */
    public function toEmail()
    {
        return [
            'header' => 'Welcome Awamer.',
            'content' => $this->request['message'],
        ];
    }

}
