<?php


namespace App\Exports;


use App\Models\City;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CitiesExport implements FromArray, WithHeadings
{
    public function headings(): array
    {
        return [
            __('dashboard.main.name_in_ar'),
            __('dashboard.main.name_in_en'),
            __('dashboard.main.Created At'),
        ];
    }

    public function array(): array
    {
        $cities = City::all();

        foreach($cities as $city){
            $data[] = [
                __('dashboard.main.name_in_ar') => $city->getTranslation('name', 'ar'),
                __('dashboard.main.name_in_en') => $city->getTranslation('name', 'en'),
                __('dashboard.main.Created At') => date('Y-m-d H:i', strtotime($city->created_at))
            ];
        }

        return $data;
    }
}
