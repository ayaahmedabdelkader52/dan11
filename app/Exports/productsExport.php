<?php

namespace App\Exports;

use App\Models\Subcategory;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class productsExport implements FromArray, WithHeadings
{
    public function __construct($type){
        $this->type = $type;
    }
    public function headings(): array
    {
        return [
            __('dashboard.main.price'),
            __('dashboard.main.name_in_ar'),
            __('dashboard.main.name_in_en'),
            __('dashboard.main.audioName_ar'),
            __('dashboard.main.audioName_en'),
            __('dashboard.main.description_in_en'),
            __('dashboard.main.description_in_ar'),
            __('dashboard.main.Created At'),
        ];
    }

    public function array(): array
    {
        $products = Subcategory::select('name_ar', 'name_en','description_ar', 'description_en', 'audioName_ar' ,'audioName_en', 'price', 'created_at')->latest()->get();

        foreach($products as $product){


            $data[] = [
                __('dashboard.main.name_in_ar') => $product->name_ar,
                __('dashboard.main.name_in_en') => $product->name_en,
                __('dashboard.main.price') => $product->price,
                __('dashboard.main.audioName_ar') => $product ->audioName_ar,
                __('dashboard.main.audioName_en') => $product ->audioName_en,
                __('dashboard.main.description_in_ar')=>$product -> description_ar ,
                __('dashboard.main.description_in_en')=>$product -> description_en ,
                __('dashboard.main.Created At') => date('Y-m-d H:i', strtotime($product->created_at))
            ];
        }

        return $data;
    }

}
