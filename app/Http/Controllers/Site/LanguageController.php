<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiTrait;
use App\Traits\Uploadable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Models\Language;
use Session;
use Illuminate\Support\Facades\Validator;

class LanguageController extends Controller
{

    public static function lang()
    {

        if (session()->has('lang')) {
            if (session()->get('lang') == 'ar') {
                App::setlocale('ar');
                $lang = 'ar';
            } else {
                App::setlocale('en');
                $lang = 'en';
            }
//            dd($lang);
        } else {
            $lang = 'ar';
            App::setlocale('ar');
        }
        return $lang;
    }

}
