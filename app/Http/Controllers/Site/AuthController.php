<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Auth\CheckloginRequest;
use App\Http\Requests\Site\Auth\PasswordUpdateRequest;
use App\Http\Requests\Site\Auth\StorregisterRequest;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\PermissionTrait;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    use Uploadable, HasResponse, PermissionTrait;


    public function login()
    {
        return view('site.login');
    }

    public function checkLogin(CheckloginRequest $request){

            $validator =$request->validated();
        $check = $request->only('phone', 'password');

        if($request->validated()){

            if(!Auth::attempt( $check )){
                return response()->json([
                    'key'   => 'fail',
                    'msg' => Session::has('lang') && Session::get('lang') == 'en' ? 'incorrect information.':'البيانات المستخدمة غير صحيحة.',

                ]);
            }

            if(Auth::user()-> status == 'blocked' ){

                return response()->json([
                    'key'   => 'fail',
                    'msg' =>  Session::has('lang') && Session::get('lang') == 'en' ? 'This Account has been blocked':'تم حظر هذا الحساب',

                ]);

            }else{
                $id = Auth::user();
                if(Auth::user()->status == 'pending'){
                    $id -> sendVerificationCode();
                    $msg = route('confirmation' , compact('id'));

                }else {

                    if(Auth::user()->type=="admin"){

                        $msg = route('index');

                    }else{

                        $msg = route('index');

                    }
                }


                return response()->json([
                    'key' => 'success',
                    'msg' => $msg
                ]);
            }


        }else {
            foreach ((array) $validator->error() as $value) {

                if (isset($value['phone'])) {
                    $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'enter phone number.' : 'ادخل رقم جوال.';
                    return response()->json([
                        'key' => 'fail',
                        'msg' => $msg
                    ]);

                } elseif (isset($value['password'])) {
                    $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'password is too short (min:6characters).' : 'يجب ألا تقل كلمه المرور عن ٦ حروف';
                    return response()->json([
                        'key' => 'fail',
                        'msg' => $msg
                    ]);

                } else {
                    $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'something went wrong.' : 'حدث خطأ ما.';
                    return response()->json([
                        'key' => 'fail',
                        'msg' => $msg,
                    ]);
                }
            }
        }
    }

    public function profile()
    {
        $orders = Order::where('name' , auth()->user()->name)->get();
        if ($orders -> count() > 0) {
            foreach ($orders as $order) {
                $products = OrderProduct::where('order_id', $order->id)->get();

            }
        }else{
            $products=null;

        }

        return view('site.profile',compact('orders','products'));
    }

    public function storeuser(Request $request){

        $data = $request -> all();
        if ($request->has('avatar'))
        {
            $data['avatar']=  $this ->uploadOne($request->avatar , 'users' , true , 250 , null );
        }
        if ($request -> has('phone')){
            $filteredPhone = User::cleanPhone($data['phone']);
            $data['phone'] = $request['phone'] = $filteredPhone['phone'];
            $data['country_key'] = $filteredPhone['country_key'];
            $data['password'] = Hash::make($data['password']);
        }
        $user=Auth::user();
        $user -> update($data);
        if (! $user)
        {
            $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'something went wrong.' : 'حدث خطأ ما.';

            return response()->json([
                'key' => 'fail' ,
                'msg' => $msg ,
            ]);
        }
        return response()->json([
            'key' => 'success' ,
            'msg' => route('profile'),
        ]);

    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'password' => 'required|confirmed',
        ]);
        if($validator->passes()) {
            $user = Auth::user();
            $hashedPassword = Auth::user()->password;

            if (Hash::check($request->oldpassword, $hashedPassword)) {
                $user->update(['password' => $request->password]);
                return response()->json([
                    'key' => 'success',
                    'msg' => route('profile'),
                ]);

            } else {
                $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'Old password is wrong.' : 'كلمه السر القديمه خاطئه.';

                return response()->json([
                    'key' => 'fail',
                    'msg' => $msg,
                ]);
            }
        }
        $msg = Session::has('lang') && Session::get('lang') == 'en' ? "Password doesn't confirmed" : 'لم يتم تاكيد كلمه السر.';

        return response()->json([
            'key' => 'fail',
            'msg' => $msg,
        ]);
    }

    public function register(){
        return view('site.register');
    }

    public function store(StorregisterRequest $request){

        $phone = $request ->phone ;
        $user = User::where('phone' , $phone)->get();
        if($request->validated()) {
            $data = $request -> except('password_confirmation');
           if ($request->has('avatar'))
           {
               $data['avatar']=  $this ->uploadOne($request->avatar , 'users' , true , 250 , null );
           }
            $filteredPhone = User::cleanPhone($data['phone']);
            $data['phone'] = $request['phone'] = $filteredPhone['phone'];
            $data['country_key'] = $filteredPhone['country_key'];
            $data['password'] = Hash::make($data['password']);

           $user = User::create($data);

           $user -> sendVerificationCode();
           $id = $user -> id;
                  return response()->json([
                      'key'   => 'success',
                      'msg' =>  route('confirmation',compact('id')),

                 ]);


        }else{
           if (empty($request->name)){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'Name is Required':'الاسم مطلوب';
            }elseif(empty($request->email)){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'Email is Required':'الايميل مطلوب';
            }elseif(empty($request->password)){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'Password is Required':'كلمة السر مطلوبة';
            }elseif(empty($request->password_confirmation)){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'Password Confirmation is Required':'تاكيد كلمه السر مطلوب';
            }elseif(empty($request->phone)){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'Phone is Required':'رقم الجوال مطلوب';
            }elseif($request->password !== $request->password_confirmation){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'Password is not confirmed':'تاكيد كلمه السر غير متطابق';
            }elseif($user -> count > 0){
                $msg =  Session::has('lang') && Session::get('lang') == 'en' ? ' Phone is already registered ':'هذا الرقم مسجل من قبل';
            }

            return response()->json([
                'key'   => 'fail',
                'msg' =>  $msg ,

            ]);

        }

    }

    public function active(User $id)
    {
        return view('site.active-account' ,compact('id'));
    }

    public function activeAccount(Request $request , User $id)
    {

        if($request -> code = $id -> code ) {
            $id->update([
                'status' => 'active',
                'code' => null,
            ]);
            return response()->json([
                'key' => 'success',
                'msg' => route('index'),

            ]);
        }
        $msg =  Session::has('lang') && Session::get('lang') == 'en' ? 'code is Uncorrect':'الكود غير صحيح ';

        return  response()->json([
            'key' => 'fail',
            'msg' => $msg,

        ]);
    }

    public function forget()
    {
        return view('site.forget');
    }

    public function forgetData(Request $request)
    {

        $phone = $request ->phone ;
        $user = User::where('phone' , $phone)->first();

       if (!$user){
           $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'This number is not found' : 'هذا الرقم غير مسجل.' ;
           return \response()->json([
               'key'=>'fail',
                'msg'=>$msg,
               ]);
       }

        $user -> sendVerificationCode();

       return \response()->json([
           'key'=>'success',
           'msg'=>route('sendcode' , compact('user')),
       ]);
    }

    public function sendcode(User $user )
    {

        return view('site.sendcode' , compact('user'));
    }

    public function passwordupdate(User $user , PasswordUpdateRequest $request)
    {

        if($request->validated) {

            if ($request->code == $user->code) {
                if ($request->password !== $request->password_confirmation) {
                    $msg = Session::has('lang') && Session::get('lang') == 'en' ? ' password not confirmed' : 'لم يتم تاكيد الباسورد.';
                    return \response()->json([
                        'key' => 'fail',
                        'msg' => $msg,
                    ]);
                }
                $user->update([
                    'code' => null,
                    'password' => $request->password,
                ]);
                return \response()->json([
                    'key' => 'success',
                    'msg' => route('login'),
                ]);
            }
            $msg = Session::has('lang') && Session::get('lang') == 'en' ? ' Code is Uncorrect' : 'كود التفعيل خطأ.';

            return \response()->json([
                'key' => 'fail',
                'msg' => $msg,
            ]);
        }
        $msg = Session::has('lang') && Session::get('lang') == 'en' ? 'something went wrong.' : 'حدث خطا ما.';

        return \response()->json([
            'key' => 'fail',
            'msg' => $msg,
        ]);
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');

    }
}
