<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Attribute;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Character;
use App\Models\Coupon;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Rate;
use App\Models\Singer;
use App\Models\Slide;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session ;

class ProductController extends Controller
{
    public function products(Category $category)
    {
        $singers = Singer::all();
        $characters = Character::all();
        $subjects = Subject::all();
        $slides = Slide::all();
        $products = Product::where('category_id',$category->id)->get();
        $albums = Album::where('category_id',$category->id)->get();
        if ( $products -> count() < 1 )
        {
            if ($albums -> count() < 1 )
            {
                return redirect() -> route('index');
            }else{
                foreach ($albums as $album)
                    $rates = Rate::where('album_id' , $album->id)->get();

                return view('site.department-details', compact('albums' , 'category' , 'rates'));
            }
        }else{
            foreach ($products as $product)
                $rates = Rate::where('product_id' , $product->id)->get();

            return view('site.department1-details', compact('products' , 'category',
                'singers','characters','subjects','slides','rates'));
        }

    }

    public function product(Product $product)
    {
        $rate = Rate::where('product_id' , $product->id)->first();
        return view('site.department1-product' , compact('product','rate'));

    }


    public function getFilter(Request $request)
    {
        $singers = Singer::all();
        $characters = Character::all();
        $subjects = Subject::all();
        $singer=$request['singer_filter'];
        $character=$request['char_filter'];
        $subject=$request['subject_filter'];

        if($request['singer_filter']||$request['char_filter']||$request['subject_filter']) {


            $products = DB::table('products')
            ->leftJoin('product_singers', 'products.id', '=', 'product_singers.product_id')
                ->leftJoin('product_characters', 'products.id', '=', 'product_characters.product_id')
                ->Where('products.category_id', $request['category_id'])
                ->when($singer, function ($query, $singer) {
                    return $query->where('product_singers.singer_id',$singer);
                }, function ($query) {
                    return $query->distinct('id');
                })
                ->when($character, function ($query, $character) {
                    return $query->where('product_characters.character_id',$character);
                }, function ($query) {
                    return $query->distinct('id');
                })
                ->when($subject, function ($query, $subject) {
                    return $query->where('products.subject_id',$subject);
                }, function ($query) {
                    return $query->distinct('id');
                })

                ->select('products.*')
                ->distinct('id')
                ->get();
//            dd($products);
        }
        else {
            $products = Product::where('category_id', $request['category_id'])->get();
        }
        $view = view("site.filter",compact('products','singers','characters','subjects'))->render();
        return response()->json(['html'=>$view]);

    }


    public function album(Album $album){
        $rate = Rate::where('album_id' , $album->id)->first();

        return view('site.product' , compact('album','rate'));

    }

    public function addCart(Request $request)
    {
        $id =$request->id;
        $product = Product::find($id);
        $oldCart=Session::has('cart') ? Session::get('cart') : null ;
        $cart = new cart ($oldCart);
        $cart -> add($product , $product->id);

        $request -> session()->put('cart' , $cart);

        return response()->json([
            'key' => 'success',
            ]);
    }

    public function getCart(){
        if (!Session::has('cart')) {
            return view('site.cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $order =Order::all();

        return view('site.cart' , compact('order'), ['products' => $cart->items , 'totalPrice' => $cart->totalPrice]);
    }

    public function getReduceByOne($id)
    {
        $oldCart=Session::has('cart') ? Session::get('cart') : null ;
        $cart = new cart($oldCart) ;
        $cart -> reduceByOne($id);
        Session::put('cart' , $cart);

        return redirect()->route('cart');
    }

    public function getRemoveItem($id)
    {
        $oldCart=Session::has('cart') ? Session::get('cart') : null ;
        $cart = new cart($oldCart) ;
        $cart -> removeItem($id);
        Session::put('cart' , $cart);

        return redirect()->route('cart');
    }


    public function getCheckout()
    {
        if (! Session::has('cart')){
            return view('site.bill');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart -> totalPrice ;
        $products = $cart -> items ;
        return view('site.bill' , compact('total' , 'products'));
    }
    public function Checkout(Request $request )
    {

        $data = $request->all();
        $data['name'] = auth()->user()->name;

        $oldCart = Session::get('cart');
        $data['cart'] = serialize($oldCart);

        $order =  Order::create($data);

        $items =$oldCart -> items ;
        $offers =Offer::all();
        foreach ($items as $item)
        {
            $product = $item['item'];
            $price_after =$product->price_after;
            foreach ($offers as $offer) {
                if ($product->category_id == $offer->category_id)
                {
                    if ($offer->type == 'percentage')
                    {
                    $price_after = $product->price_after * ($offer->value / 100);
                }else{
                        $price_after = $product->price_after - $offer->value ;
                    }
                }
            }
            if ($request->has('coupon')) {
                $coupon = Coupon::where('code' , $request->coupon)->first();
                if ($coupon->type == 'percent')
                {
                    $price_after = $product->price_after * ($coupon->value / 100);
                }else{
                    $price_after = $product->price_after - $coupon->value ;
                }
            }
            OrderProduct::create([
                'product_id'=> $product->id,
                'order_id'=>$order->id,
                'price'=>$price_after,
                'qty'=>$item['qty'],
            ]);
        }

             Session::forget('cart');

        return redirect() -> route('index');
    }

}
