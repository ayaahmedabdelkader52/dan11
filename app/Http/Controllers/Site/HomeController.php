<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Complain;
use App\Models\Favourite;
use App\Models\Product;
use App\Models\Question;
use App\Models\Rate;
use App\Models\Rule;
use App\Models\Slide;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use function Matrix\add;

class HomeController extends Controller
{
    public function index()
    {
        $sliders = Slide::all();
        $categories = Category::latest()->get();
        return view('site.index' , compact('sliders' , 'categories'));
    }


    public function likes()
    {
        $favorites = Favourite::where('user_id', auth()->user()->id)->get();
        foreach ($favorites as $favorite)
            $products = Product::where('id', $favorite->product_id)->get();
            if ($favorites -> count() < 1){
                return view('site.likes' , compact('favorites'));
            }else {
                return view('site.likes', compact('products' , 'favorites'));
            }

    }
    public function like(Request $request){


       $favourite = Favourite::where('product_id' , $request->id )->first();
       $user = auth()->user();
        if(! $favourite)
        {
            Favourite::create([
                'product_id'=>$request->id,
                'user_id'=>$user-> id ,
            ]);

            return response()->json('success') ;
        }else{
            $favourite -> delete();
            return response()->json('false');
        }
    }


    public function rate(Request $request){


        if ($request->type == 'product') {
            Rate::create([
                'user_id' => auth()->user()->id,
                'rate' => $request->rate,
//                'type' => $request->type,
                'product_id' => $request->id,

            ]);
            return response()->json('success');
        }else{
            Rate::create([
                'user_id' => auth()->user()->id,
                'rate' => $request->rate,
//                'type' => $request->type,
                'album_id' => $request->id,

            ]);
            return response()->json('success');
        }


    }

    public function questions(){
        $questions = Question::all();
        return view('site.questions' , compact('questions'));
    }
    public function store(Request $request){

        if ($request->has('name')){
            $user =User::where('name' , $request->name)->first();
        }else{
            $user=auth()->user();
        }

        Complain::create([
            'user_id'=> $user -> id,
            'subject'=> $request -> subject,
            'text' => $request->text,
            'phone'=> $user -> phone,
        ]);


        return response()->json([
            'key'=> 'success',
        ]);
    }

    public function rules(){
        $rules = Rule::all();
        return view('site.rules' , compact('rules'));
    }




}
