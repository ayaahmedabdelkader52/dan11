<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function about(){
        return view('site.about');
    }

    public function privacy(){
        return view('site.privacy');
    }
    public function contact(){
        return view('site.contact');
    }
    public function joinus(){
        return view('site.joinus');
    }
}
