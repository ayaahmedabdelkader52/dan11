<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\EmailFromAdmin;
use App\Notifications\SmsFromAdmin;
use App\Traits\Notifications\Notifiable;
use App\Traits\Uploadable;
use App\Http\Requests\Messaging\{
    SendEmailRequest,
    SendNotificationRequest,
    SendSMSRequest,
};

class NotificationController extends Controller
{
    use Notifiable;
    use Uploadable;

    public function index()
    {
        $notifications = auth()->user()->notifications;

        foreach ($notifications as $notification)
            $notification->markAsRead();
        return view('admin.notifications.index', compact('notifications'));
    }

    public function create()
    {
        return view('admin.notifications.create');
    }

    public function sendSmsMessage(SendSMSRequest $request)
    {
        $users = User::select('id', 'country_key', 'phone')->filter($request)->get();

        foreach ($users as $user) {
            $user->notify(new SmsFromAdmin($request));
        }

        auth()->user()->saveReport('إرسال رسالة SMS  الي ' . __('dashboard.user.' . $request->user_type));
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function sendEmail(SendEmailRequest $request)
    {
        $this->checkEmailConfig();
        $users = User::filter($request)->get();

        foreach ($users as $user) {
            $user->notify(new EmailFromAdmin($request));
        }

        auth()->user()->saveReport('ارسال رسالة Email  الي ' . __('dashboard.user.' . $request->user_type));
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function adminSendNotification(SendNotificationRequest $request)
    {
        $devices = User::filter($request)->withDevices()->get();

        if ($request->image) {
            $image = $this->uploadOne($request->image, 'notifications', true, 250);
        }

        $data = [
            'title' => $request['title'],
            'message' => $request['message'],
            'key' => 'from_admin',
            'image' => $image ?? '',
            'data' => [],
        ];

        $this->sendNotification($devices, $data);

        auth()->user()->saveReport('إرسال إشعار  الي ' . __('dashboard.user.' . $request->user_type));
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

}
