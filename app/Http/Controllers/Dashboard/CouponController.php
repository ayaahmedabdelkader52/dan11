<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CouponRequest\StoreRequest;
use App\Http\Requests\CouponRequest\UpdateRequest;
use App\Models\Coupon;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class CouponController extends Controller

{
    use HasResponse;

    public function index()
    {
        $coupons = Coupon::all();

        return view('admin.coupons.index', compact('coupons'));
    }


    public function create(){

        return view('admin.coupons.create');
    }
    public function store(StoreRequest $request){


        $data = $request->validated();

        Coupon::create($data);

        auth()->user()->saveReport('حفظ الكود');

        return redirect()->route('admin.coupons.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Coupon $coupon)
    {

        return view('admin.coupons.edit' , compact('coupon' ));
    }

    public function update(UpdateRequest $request , Coupon $coupon){

        $data = $request->validated();

        $coupon->update($data);

        auth()->user()->saveReport('تعديل كود');

        return redirect()->route('admin.coupons.index')->with('success', trans('dashboard.created_successfully'));

    }


    public function destroy(Coupon $coupon)
    {


        $coupon ->delete();

        auth()->user()->saveReport(__('dashboard.coupon.delete_product'));

        return self::successReturn( '' , $coupon);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $coupons = Coupon::find($ids);


        Coupon::destroy($coupons);

        auth()->user()->saveReport('حذف الاكواد');

        return self::successReturn('', $ids);

    }
}
