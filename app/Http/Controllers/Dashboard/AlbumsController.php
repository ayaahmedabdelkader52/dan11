<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Album\storeRequest;
use App\Models\Album;
use App\Models\AlbumImage;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AlbumsController extends Controller
{
    use Uploadable;
    use HasResponse;
    public function index()
    {

        $albums=Album::all();
        $photos = AlbumImage::all();
        return  view('admin.albums.index' , compact('albums','photos'));
    }
    public function create()
    {
        $categories = Category::all();
        $subcategories =Subcategory::all();
        return view('admin.albums.create',compact('subcategories','categories'));
    }
    public function store(storeRequest $request)
    {


         $data = $request -> validated();
        if ($request->has('photo')) {

            $data['photo'] = $this->uploadOne($request->photo, 'albums', true, 250, null);
        }

        if($request->offer == 0) {
            $data['price_after'] = $request->price_before;
            $data['end'] = null;
            $data['start'] = null;
        }
        $subcategory =Subcategory::where('id' , $request->subcategory_id)->first();
        $data['category_id'] = $subcategory->category_id;
        Album::create($data);

        auth()->user()->saveReport('حفظ ');

        return redirect()->route('admin.albums.index')->with('success', trans('dashboard.created_successfully'));


    }
    public function edit(Album $album)
    {
        $categories = Category::all();
        $subcategories =Subcategory::all();
        return view('admin.albums.edit' , compact('categories','subcategories','album'));
    }
    public function update(storeRequest $request , Album $album)
    {

        $data = $request -> validated();
        if ($request->has('photo')) {

            File::delete(public_path('assets/uploads/albums/' .$album->photo));

            $data['photo'] = $this->uploadOne($request->photo, 'albums', true, 250, null);
        }else{
            $data['photo'] = $album -> photo ;
        }

        $album -> update($data);
        auth()->user()->saveReport(' تعديل ');

        return redirect()->route('admin.albums.index')->with('success', trans('dashboard.created_successfully'));


    }
    public function show(Album $album)
    {
        $categories = Category::all();
        $subcategories =Subcategory::all();
        $photos=AlbumImage::all();
        return view('admin.albums.show' , compact('album' , 'categories','photos' , 'subcategories'));
    }
    public function destroy(Album $album)
    {
        File::delete(public_path("assets/uploads/albums/" . $album -> photos ->photo));
        File::delete(public_path("assets/uploads/categories/" . $album->photo));
        $album ->delete();

        auth()->user()->saveReport(__('dashboard.categories.delete_product'));

        return self::successReturn( '' , $album);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $albums = Album::find($ids);
        foreach ($albums as $album)
        {
            File::delete(public_path('assets/uploads/albums/' .$album->photo));

        }

        Category::destroy($albums);

        auth()->user()->saveReport('حذف الالبومات');

        return self::successReturn('', $ids);

    }
}
