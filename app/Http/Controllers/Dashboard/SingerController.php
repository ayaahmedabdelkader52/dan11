<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductSinger;
use App\Models\Singer;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class SingerController extends Controller
{
    use HasResponse;
    public function index()
    {

        $singers=Singer::all();
        return view('admin.singers.index' , compact('singers'));
    }

    public function create()
    {
        $products= Product::all();

        return view('admin.singers.create' , compact('products'));
    }

    public function store(Request $request){

        $singer = new Singer();
        $singer -> name_ar =$request -> name_ar ;
        $singer -> name_en =$request -> name_en ;
        $singer ->save();
        $products_id = $request -> products_id ;

        foreach ($products_id as $product_id) {
            ProductSinger::create([

                'singer_id' => $singer->id ,
                'product_id' => $product_id,

            ]);
        }

        auth()->user()->saveReport('حفظ ');

        return redirect()->route('admin.singers.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Singer $singer)
    {
        $products= Product::all();
        return view('admin.singers.edit' , compact('singer','products'));
    }

    public function update(Request $request , Singer $singer){

        $data = $request->all();

        $singer->update($data);

        auth()->user()->saveReport('اضافه مغني ');

        return redirect()->route('admin.singers.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function destroy(Singer $singer)
    {

        $singer ->delete();

        auth()->user()->saveReport(__('dashboard.singers.delete_product'));

        return self::successReturn( '' , $singer);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $singers = Singer::find($ids);


        Singer::destroy($singers);

        auth()->user()->saveReport('حذف المغنيين');

        return self::successReturn('', $ids);

    }
}
