<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\CitiesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\City\StoreRequest;
use App\Http\Requests\City\UpdateRequest;
use App\Models\City;
use App\Models\Region;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CityController extends Controller
{
    use Uploadable, HasResponse;

    public function index(Region $region)
    {
        $cities = $region->cities;

        return view('admin.cities.index', compact('cities', 'region'));
    }

    public function create(Region $region)
    {
        return view('admin.cities.create', compact('region'));
    }

    public function store(StoreRequest $request)
    {
        City::create($request->validated());

        auth()->user()->saveReport('اضافة دولة');
        return redirect()->route('admin.regions.cities', $request->input('region_id'))->with('success', trans('dashboard.created_successfully'));
    }

    public function show(City $city)
    {
        return view('admin.cities.show', compact('city'));
    }

    public function edit(City $city)
    {
        return view('admin.cities.edit', compact('city'));
    }

    public function update(UpdateRequest $request, City $city)
    {
        $city->update($request->validated());

        auth()->user()->saveReport('تعديل دولة');
        return redirect()->route('admin.regions.cities', $request->input('region_id'))->with('success', trans('dashboard.alerts.updated_successfully'));
    }

    public function destroy(City $city)
    {
        $city->delete();

        auth()->user()->saveReport('حذف دولة');
        return $this->successReturn();
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->cities;
        $cities = City::find($ids);

        City::destroy($cities);

        auth()->user()->saveReport('حذف دول');
        return self::successReturn('', $ids);
    }

    public function downloadAllCities()
    {
        auth()->user()->saveReport('تنزيل الدول كملف Excel');
        return Excel::download(new CitiesExport(), 'all-cities.xlsx');
    }

}
