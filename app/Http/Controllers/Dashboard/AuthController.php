<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Models\User;
use App\Traits\GeneralHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use GeneralHelpers;
    public function login()
    {
        return view('admin.auth.login');
    }

    public function postLogin(AdminLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $remember_me = $request->has('remember_me');

        if (is_numeric($request->input('email'))) {
            $x = $this->convert2english(request('email'));
            $phone = User::cleanPhone($x)['phone'];

            $credentials = ['phone' => $phone, 'password' => $request->get('password')];
        } elseif (filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {

            $credentials = ['email' => $request['email'], 'password' => $request->get('password')];
        }

        if (Auth::attempt($credentials, $remember_me)) {
            return redirect()->route('admin.admin');
        } else {
            return redirect()->back()->with(['error' => __('auth.failed')]);
        }

    }

    public function logoutAdmin()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }

}
