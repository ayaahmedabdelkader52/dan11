<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $users = User::where('role_id', 2)->orderBy('created_at', 'desc')->get();
        $admins = User::where('role_id', 1)->orderBy('created_at', 'desc')->get();

        return view('admin.index', compact('users', 'admins'));
    }

    public function them($them)
    {
        Session()->has('them') ? Session()->forget('them') : '';
        $them == 'dark' ? Session()->put('them', 'dark') : Session()->put('them', 'light');
        return back();
    }
}
