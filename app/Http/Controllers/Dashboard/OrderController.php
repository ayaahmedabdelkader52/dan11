<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();

        return view('admin.orders.index',compact('orders',));
    }
    public function show(Order $order)
    {
        $products=OrderProduct::where('order_id' , $order->id)->get();

        return view('admin.orders.show',compact('products','order'));
    }
    public function edit(Order $order)
    {
        $products=OrderProduct::where('order_id' , $order->id)->get();

        return view('admin.orders.show',compact('products','order'));

    }
}
