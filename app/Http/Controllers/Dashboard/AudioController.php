<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddRequest;
use App\Http\Requests\AudioRequest;
use App\Models\Audio;
use App\Models\Product;
use App\Models\ProductAudio;
use App\Models\Subcategory;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AudioController extends Controller
{
    use Uploadable;
    use HasResponse;

    public function index()
    {
        $manyaudio = Audio::all();

        return view('admin.audio.index', compact('manyaudio'));
    }


    public function create(){

       $products = Product::all();

        return view('admin.audio.create' , compact('products'));
    }
    public function store(AudioRequest $request){


        $data = $request->validated();

               $data['audio'] = $this->uploadFile($request -> audio, 'audio');


               $audio =new Audio ;
               $audio -> name_ar =$data['name_ar'];
               $audio -> name_en =$data['name_en'];
               $audio -> audio =$data['audio'];
               $audio -> save();

        $products_id = $request -> product_id ;

        foreach ($products_id as $product_id) {
            ProductAudio::create([
                'product_id' => $product_id,
                'audio_id' => $audio->id,
            ]);
        }
        auth()->user()->saveReport('حفظ الملف');

        return redirect()->route('admin.audio.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Audio $audio)
    {
        $subcategories = Subcategory::all() ;

        return view('admin.audio.edit' , compact('audio' , 'subcategories'));
    }

    public function update(AudioRequest $request , Audio $audio){

        $data = $request->validated();
        if ($request ->has('audio')) {
            File::delete(public_path('assets/uploads/audio/' . $audio->audio));
            $data['audio'] = $this->uploadFile($request->audio, 'audio');
        }else{
            $data['audio'] = $audio -> audio;
        }
        $audio->update($data);

        auth()->user()->saveReport('اضافه قسم');

        return redirect()->route('admin.audio.index')->with('success', trans('dashboard.created_successfully'));

    }


    public function destroy(Audio $audio)
    {
       File::delete(public_path("assets/uploads/audio/" . $audio->audio));

         $audio ->delete();

        auth()->user()->saveReport(__('dashboard.audio.delete_product'));

        return self::successReturn( '' , $audio);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $audio = Audio::find($ids);


        Audio::destroy($audio);

        auth()->user()->saveReport('حذف الاقسام');

        return self::successReturn('', $ids);

    }
}
