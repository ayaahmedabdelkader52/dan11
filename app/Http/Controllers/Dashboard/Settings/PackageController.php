<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\MessagePackage;
use App\Http\Requests\Settings\{
    FcmRequest,
    SmsRequest,
    SmtpRequest,
};

class PackageController extends Controller
{
    private $except = ['fcm', 'smtp'];

    public function smtp(SmtpRequest $request)
    {
        $data = $request->validated();
        $data['active'] = ($request->active == 'on') ? 'true' : 'false';
        $data['type'] = 'smtp';

        MessagePackage::where('type', 'smtp')->updateOrCreate([
            'type' => 'smtp'
        ], $data);

        auth()->user()->saveReport('تحديث بيانات SMTP');

        return redirect()->back()->with(['success' => 'تم الحفظ بنجاح']);
    }

    public function updateSms(SmsRequest $request)
    {
        $request = $request->all();
        $type = $data['type'] = $request['type'];
        $data["username"] = $request["username"][$type];
        $data["password"] = $request["password"][$type];
        $data["sender_name"] = $request["sender_name"][$type];
        $data['active'] = 'true';

        $package = MessagePackage::where('type', $type)->updateOrCreate([
            'type' => $type
        ], $data);

        MessagePackage::whereNotIn('type', $this->except)->where('id', '<>', $package['id'])->update(['active' => 'false']);


        auth()->user()->saveReport('تحديث بيانات بقات ال SMS');

        return redirect()->back()->with(['success' => 'تم الحفظ بنجاح']);
    }

    public function updateFcm(FcmRequest $request)
    {
        $data = $request->validated();
        $data['type'] = 'fcm';

        MessagePackage::updateOrCreate([
            'type' => 'fcm'
        ], $data);

        auth()->user()->saveReport('تحديث بيانات FCM');

        return redirect()->back()->with(['success' => 'تم الحفظ بنجاح']);
    }

}
