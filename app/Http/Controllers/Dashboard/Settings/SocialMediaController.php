<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\SocialRequest;
use App\Http\Requests\Settings\SocialUpdateRequest;
use App\Models\Social;
use App\Traits\HasResponse;

class SocialMediaController extends Controller
{
    use HasResponse;

    public function store(SocialRequest $request)
    {
        Social::create($request->validated());

        return redirect()->back()->with('success', __('dashboard.alerts.created_successfully'));
    }

    public function update(SocialUpdateRequest $request, Social $social)
    {
        $social->update($request->validated());

        return redirect()->back()->with('success', __('dashboard.alerts.updated_successfully'));
    }

    public function destroy(Social $social)
    {
        $social->delete();

        return $this->successMsg();
    }

}
