<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlbumImages\StoreRequest;
use App\Http\Requests\AlbumImages\UpdateRequest;
use App\Models\Album;
use App\Models\AlbumImage;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AlbumImageController extends Controller
{
    use HasResponse;
    use Uploadable;

    public function index()
    {
        $photos = AlbumImage::all();
        return view('admin.albumImages.index',compact('photos'));
    }

    public function create()
    {
        $albums = Album::all();
        return view('admin.albumImages.create',compact('albums'));
    }

    public function store (StoreRequest $request)
    {
        $data = $request -> validated();

        if ($request ->has('photo'))
             $data['photo'] = $this->uploadOne($request->photo, 'albums', true, 250, null);

        AlbumImage::create($data);

        auth()->user()->saveReport('saved ');

        return redirect()->route('admin.albumimages.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(AlbumImage $photo)
    {
        $albums = Album::all();
        return view('admin.albumImages.edit' , compact('photo' , 'albums'));
    }

    public function update(AlbumImage $photo, UpdateRequest $request)
    {

            $data = $request -> validated();

        if ($request->has('photo')) {

            File::delete(public_path('assets/uploads/albums/' .$photo -> photo));

            $data['photo'] = $this->uploadOne($request->photo, 'albums', true, 250, null);

             }else{

            $data['photo'] = $photo -> photo ;
        }
          $photo -> update($data);

            auth()->user()->saveReport('saved ');

            return redirect()->route('admin.albumimages.index')->with('success', trans('dashboard.created_successfully'));


    }

    public function destroy(AlbumImage $photo)
    {
        File::delete(public_path("assets/uploads/albums/" . $photo -> photo));

        $photo ->delete();

        auth()->user()->saveReport(__('dashboard.categories.delete_product'));

        return self::successReturn( '' , $photo);
    }

    public function destroySelected(Request $request)
    {

        $ids = $request -> ids;

        $photos = AlbumImage::find($ids);
        foreach ($photos as $photo)
        {
            File::delete(public_path("assets/uploads/albums/" . $photo -> photo));

        }

        AlbumImage::destroy($photos);

        auth()->user()->saveReport('حذف الصور');

        return self::successReturn('', $ids);

    }

}
