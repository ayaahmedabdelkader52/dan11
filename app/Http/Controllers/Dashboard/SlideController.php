<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\SlideRequest;
use App\Http\Requests\UpdateAdsRequest;
use App\Models\Slide;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SlideController extends Controller
{
    use Uploadable, HasResponse;

    public function index()
    {
        $slides = Slide::all();
        return view('admin.slids.index', compact('slides'));
    }

    public function create()
    {
        return view('admin.slids.create');
    }

    public function store(SlideRequest $request)
    {

        $data = $request ->validated();

        if ($request->has('image')) {

            $data['image'] = $this->uploadOne($request -> image, 'slids', true, 250, null);

        }
        $slids = new Slide();
        $slids -> title_ar = $request ->title_ar;
        $slids -> title_en = $request ->title_en;
        $slids -> image = $data ['image'];
        $slids ->save();

        auth()->user()->saveReport('اضافة الخلفيه');
        return redirect()->route('admin.slids.index')->with('success', 'تم اضافه الخلفيه بنجاح');

    }

    public function edit(Slide $slide)
    {
        return view('admin.slids.edit', compact('slide'));
    }

    public function update(UpdateAdsRequest $request, Slide $slide)
    {
        $data = $request->validated();

        if ($request->image)
            $data['image'] = $request->image ? $this->uploadOne($request->image, 'slids', true, 250, null) : '';

        $slide->update($data);

        auth()->user()->saveReport('تعديل الخلفية');
        return redirect()->route('admin.slids.index')->with(['success' => 'تم تعديل الخلفيه بنجاح']);
    }

    public function destroy(Slide $slide)
    {
        File::delete(public_path("assets/uploads/slids/" . $slide->photo));

        $data = ['adId' => $slide ->id,];
        $slide->delete();

        auth()->user()->saveReport('حذف الخلفيه');
        return self::successReturn('', $data);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request-> slide;
        $slide = Slide::find($ids);

        Slide::destroy($slide);

        auth()->user()->saveReport('حذف الخلفيات');
        return self::successReturn('', $ids);
    }
}
