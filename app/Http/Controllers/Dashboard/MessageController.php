<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\Notifications\Notifiable;
use App\Http\Requests\Messaging\{
    SendEmailRequest,
    SendNotificationRequest,
    SendSMSRequest,
};
use App\Notifications\{
    EmailFromAdmin,
    NotificationFromAdmin,
    SmsFromAdmin,
};

class MessageController extends Controller
{
    use Notifiable;

    public function smsToAllUsers(SendSMSRequest $request)
    {
        $users = User::users()->get();
        foreach ($users as $user) {
            $user->notify(new SmsFromAdmin($request));
        }

        auth()->user()->saveReport('إرسال رسالة SMS الي كل العملاء');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function emailAllUsers(SendEmailRequest $request)
    {
        $this->checkEmailConfig();
        $users = User::users()->get();

        foreach ($users as $user) {
            $user->notify(new EmailFromAdmin($request));
        }

        auth()->user()->saveReport('ارسال رسالة Email الي كل العملاء');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function notificationAllUsers(SendNotificationRequest $request)
    {
        $devices = User::users()->withDevices()->get();
        $data = [
            'title' => $request['title'],
            'message' => $request['message'],
            'key' => 'from_admin',
            'data' => [],
        ];

        $this->sendNotification($devices, $data);

        auth()->user()->saveReport('ارسال اشعار الي كل العملاء');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function smsToSingleUser(SendSMSRequest $request)
    {
        $request->validate(['user_id' => 'required',]);
        $user = User::findOrFail($request->user_id);
        $user->notify(new SmsFromAdmin($request));

        auth()->user()->saveReport('ارسال رسالة SMS الي عميل');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function emailToSingleUser(SendEmailRequest $request)
    {
        $user = User::findOrFail($request->user_id);
        $user->notify(new EmailFromAdmin($request));

        auth()->user()->saveReport('ارسال رسالة ايميل الي عميل');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function notificationToSingleUser(SendNotificationRequest $request)
    {
        $user = User::findOrFail($request->user_id);
        $user->notify(new NotificationFromAdmin($request));

        auth()->user()->saveReport('ارسال اشعار الي عميل');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

}
