<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Rules\StoreRequest;
use App\Http\Requests\Rules\UpdateRequest;
use App\Models\Rule;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;

class RuleController extends Controller
{

    use Uploadable;
    use HasResponse;

    public function index()
    {
        $rules = Rule::all();

        return view('admin.rules.index', compact('rules'));
    }
    public function create(){

        return view('admin.rules.create');
    }

    public function store(StoreRequest $request){

        $data = $request->validated();


        Rule::create($data);

        auth()->user()->saveReport('تعديل الاحكام');

        return redirect()->route('admin.rules.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Rule $rule )
    {
        return view('admin.rules.edit' , compact('rule'));
    }

    public function update(UpdateRequest $request , Rule $rule)
    {

        $data = $request->validated();


        $rule->update($data);

        auth()->user()->saveReport('تعديل الاحكام');

        return redirect()->route('admin.rules.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }

    public function destroy(Rule $rule)
    {

        $rule ->delete();

        auth()->user()->saveReport(__('dashboard.rules.delete_product'));

        return self::successReturn( '' , $rule);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $rules = Rule::find($ids);

        Rule::destroy($rules);

        auth()->user()->saveReport('حذف الاحكام');

        return self::successReturn('', $ids);

    }
}
