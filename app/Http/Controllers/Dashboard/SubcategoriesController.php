<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Subcategory\StoreRequest;
use App\Http\Requests\Subcategory\UpdateRequest;
use App\Models\Category;
use App\Models\Subcategory;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\productsExport;

class SubcategoriesController extends Controller
{

    use Uploadable;
    use HasResponse;

    public function index()
    {
        $subcategories = Subcategory::all()  ;


        return view('admin.subcategories.index', compact('subcategories' ));
    }

    public function create()
    {

        $categories = Category::all();

        return view('admin.subcategories.create' , compact(  'categories'));
    }

    public function store(StoreRequest $request)
    {

        if($request->has('photo')) {
            $photo = $this->uploadOne($request->photo, 'subcategories', true, 250, null);
        }else{
            $photo = null;
        }

        $subcategory = new Subcategory();
        $subcategory -> name_ar = $request -> name_ar ;
        $subcategory -> name_en = $request -> name_en ;
        $subcategory -> photo = $photo ;
        $subcategory -> category_id = $request -> category_id ;
        $subcategory ->save();

        auth()->user()->saveReport(__('dashboard.subcategories.create_products'));

        return redirect()->route('admin.subcategories.index')->with('success', __('dashboard.alerts.created_successfully'));
    }

    public function edit(Subcategory $subcategory)
    {
        $categories = Category:: all();

        return view('admin.subcategories.edit', compact('subcategory','categories'));
    }

    public function update(Request $request, Subcategory $subcategory)
    {
        $data = $request->all();

        if ($request->has('photo')) {

            $data['photo'] = $this->uploadOne($request->photo, 'subcategories', true, 250, null);

        }

        $subcategory->update($data);

        auth()->user()->saveReport(__('dashboard.subcategories.edit_product'));

        return redirect()->route('admin.subcategories.index')->with('success', trans('dashboard.alerts.updated_successfully'));
    }

    public function destroy(Subcategory $subcategory)
    {
        File::delete(public_path("assets/uploads/subcategories/" . $subcategory->photo));

        $subcategory->delete();

        auth()->user()->saveReport(__('dashboard.subcategories.delete_product'));

        return self::successReturn('', $subcategory);

    }

    public function destroySelected(Request $request)
    {

        $ids = $request-> ids ;

        $subcategories = Subcategory::find($ids);

        foreach ($subcategories as $subcategory)
        {
            File::delete(public_path("assets/uploads/subcategories/" . $subcategory->photo));
        }
        Subcategory::destroy($subcategories);

        auth()->user()->saveReport('حذف الخدمات');
        return self::successReturn('', $ids);
    }



}
