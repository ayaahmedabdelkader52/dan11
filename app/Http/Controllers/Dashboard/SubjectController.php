<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Subject;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    use HasResponse;
    public function index()
    {
        $subjects=Subject::all();
        return view('admin.subjects.index' , compact('subjects'));
    }

    public function create()
    {
        $products= Product::all();

        return view('admin.subjects.create' , compact('products'));
    }

    public function store(Request $request){

        $subject = new Subject();
        $subject -> name_ar =$request -> name_ar ;
        $subject -> name_en =$request -> name_en ;
        $subject->product_id = $request -> product_id ;
        $subject ->save();



        auth()->user()->saveReport('حفظ ');

        return redirect()->route('admin.subjects.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Subject $subject)
    {
        $products= Product::all();
        return view('admin.subjects.edit' , compact('subject','products'));
    }

    public function update(Request $request , Subject $subject){

        $data = $request->all();

        $subject->update($data);

        auth()->user()->saveReport('تعديل مناسبه ');

        return redirect()->route('admin.subjects.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function destroy(Subject $subject)
    {

        $subject ->delete();

        auth()->user()->saveReport(__('dashboard.subjects.delete_product'));

        return self::successReturn( '' , $subject);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $subjects = Subject::find($ids);


        Subject::destroy($subjects);

        auth()->user()->saveReport('حذف الخواص');

        return self::successReturn('', $ids);

    }
}
