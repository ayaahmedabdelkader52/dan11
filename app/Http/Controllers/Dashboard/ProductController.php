<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\StoreproductRequest;
use App\Http\Requests\Product\UpdateproductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    use HasResponse;
    use Uploadable;
    public function index()
    {
        $products = Product::all();
        return  view('admin.products.index' , compact('products'));
    }
    public function create()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();

        return view('admin.products.create' , compact('categories','subcategories'));
    }
    public function store(StoreproductRequest $request)
    {
        $data = $request -> validated();


        if ($request -> has('photo'))
            $data['photo'] = $this -> uploadOne($request->photo , 'products' , true, 250, null ) ;

        if($request->offer == 0) {
            $data['price_after'] = $request->price_before;
            $data['end'] = null;
            $data['start'] = null;
        }
        $subcategory =Subcategory::where('id' , $request->subcategory_id)->first();
        $data['category_id'] = $subcategory->category_id;

        Product::create($data);

        auth()->user()->saveReport('saved');

        return redirect()->route('admin.products.index')->with('success', trans('dashboard.created_successfully'));

    }
    public function show(Product $product){

        return view('admin.products.show' ,compact( 'product'));
    }
    public function edit(Product $product)
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('admin.products.edit' , compact('categories' ,'product', 'subcategories'));
    }
    public function update(UpdateproductRequest $request ,Product $product)
    {


        $data = $request -> validated();

        if ($request->has('photo')) {

            File::delete(public_path('assets/uploads/products/' .$product -> photo));

            $data['photo'] = $this->uploadOne($request->photo, 'products', true, 250, null);
        }else{
            $data['photo'] = $product -> photo ;
        }

        if($request->offer == 0) {
            $data['price_after'] = $request->price_before;
            $data['end'] = null;
            $data['start'] = null;
        }
        $subcategory =Subcategory::where('id' , $request->subcategory_id)->first();
        $data['category_id'] = $subcategory->category_id;

        $product->update($data);

        auth()->user()->saveReport('saved');

        return redirect()->route('admin.products.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }
    public function destroy(Product $product)
    {
        File::delete(public_path("assets/uploads/products/" . $product->photo));

        $product ->delete();

        auth()->user()->saveReport(__('dashboard.categories.delete_product'));

        return self::successReturn( '' , $product);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $products = Product::find($ids);
        foreach ($products as $product)
        {
            File::delete(public_path("assets/uploads/products/" . $product->photo));

        }

        Category::destroy($products);

        auth()->user()->saveReport('حذف الزفات');

        return self::successReturn('', $ids);

    }
}
