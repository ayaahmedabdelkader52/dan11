<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Models\User;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    use HasResponse;

    public function index()
    {
        $reports = Report::orderBy('created_at', 'desc')->get();
        $i = 1;
        $users = User::all();
        return view('admin.reports.index', compact('reports', 'i', 'users'));
    }

    public function destroy(Report $report)
    {
        $report->delete();

        auth()->user()->saveReport('حذف تقرير');
        return self::successReturn();
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->reports;

        Report::destroy($ids);

        auth()->user()->saveReport('حذف متعدد للتقارير');
        return self::successReturn('', $ids);
    }

}
