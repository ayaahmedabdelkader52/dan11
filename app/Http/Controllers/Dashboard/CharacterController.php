<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Character;
use App\Models\Product;
use App\Models\ProductCharacter;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    use HasResponse;
    public function index()
    {
        $characters=Character::all();
        return view('admin.characters.index' , compact('characters'));
    }

    public function create()
    {
        $products= Product::all();

        return view('admin.characters.create' , compact('products'));
    }

    public function store(Request $request){

        $character = new Character();
        $character -> name_ar =$request -> name_ar ;
        $character -> name_en =$request -> name_en ;
        $character ->save();
        $products_id = $request -> products_id ;

        foreach ($products_id as $product_id) {
            ProductCharacter::create([

                'character_id' => $character->id ,
                'product_id' => $product_id,

            ]);
        }

        auth()->user()->saveReport('حفظ ');

        return redirect()->route('admin.characters.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Character $character)
    {
        $products= Product::all();
        return view('admin.characters.edit' , compact('character','products'));
    }

    public function update(Request $request ,Character $character){

        $data = $request->all();

        $character->update($data);

        auth()->user()->saveReport('حذف اسم ');

        return redirect()->route('admin.characters.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function destroy(Character $character)
    {

        $character ->delete();

        auth()->user()->saveReport(__('dashboard.characters.delete_product'));

        return self::successReturn( '' , $character);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $characters = Character::find($ids);


        Character::destroy($characters);

        auth()->user()->saveReport('حذف اسماء');

        return self::successReturn('', $ids);

    }
}
