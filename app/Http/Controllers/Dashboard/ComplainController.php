<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Mail\PublicMessage;
use App\Models\Complain;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ComplainController extends Controller
{
    use HasResponse, Uploadable;
    public function index()
    {
        $complains = Complain::latest()->get();

        return view('admin.complains.index', compact('complains'));
    }

    public function show(Complain $complain)
    {
        $complain->markAsRead();

        return view('admin.complains.show')->with(['message' => $complain]);
    }

    public function destroy(Complain $complain){
        $complain->delete();

        auth()->user()->saveReport('حذف شكوي');
        return $this->successReturn();
    }

    public function destroySelected(Request $request)
    {
        $complains = Complain::find($request->messages);

        Complain::destroy($complains);

        auth()->user()->saveReport('حذف متعدد للشكاوي');
        return $this->successReturn();
    }

    public function changeStatus(Complain $complain, Request $request)
    {
        $request->validate(['status' => 'required']);

        $complain->update(['status' => $request->status]);

        auth()->user()->saveReport('تغيير حالة الشكوي');
        return $this->successReturn(__('dashboard.alerts.updated_successfully'));
    }

    // *** Reply message to Single User: Start ** //
    public function replySmsToSingleUser(SendSMSRequest $request)
    {
        $request->validate(['user_phone' => 'required',]);

        $number = $request->user_phone;

        $this->sendSms($number, $request->sms_message_content);

        auth()->user()->saveReport('الرد ب SMS علي الشكوي');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function replyEmailToSingleUser(SendEmailRequest $request)
    {
        $request->validate(['user_email' => 'required',]);

        if (!$this->checkEmailConfig()) {
            return back()->with('error', 'لم يتم ارسال الرساله ! .. يرجى مراجعة بيانات ال SMTP');
        }

        Mail::to($request->user_email)->send(new PublicMessage($request->email_message_content));

        auth()->user()->saveReport('الرد ب Email علي الشكوي');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function replyNotificationToSingleUser(SendNotificationRequest $request)
    {
        if (!$request->user_id)
            return back()->with('error', 'هذا المستخدم غير موجود !');

        $user = User::findOrFail($request->user_id);

        $key = 'from_admin';
        $data = ['title' => $request->notification_message_title, 'message' => $request->notification_message_content, 'key' => $key];

        $this->sendNotification($user, $key, $data, false);

        auth()->user()->saveReport('الرد باشعار علي الشكوي');
        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }
    // *** Reply message to Single User: End ** //
}
