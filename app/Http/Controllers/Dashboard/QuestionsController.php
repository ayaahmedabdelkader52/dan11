<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions=Question::all();
        return view('admin.questions.index' , compact('questions'));
    }
    public function create(){

        return view('admin.questions.create');
    }

    public function store(Request $request){

        $data = $request->all();


        Question::create($data);

        auth()->user()->saveReport('تعديل الاسئله');

        return redirect()->route('admin.questions.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Question $question )
    {
        return view('admin.questions.edit' , compact('question'));
    }

    public function update(Request $request , Question $question)
    {

        $data = $request->all();


        $question->update($data);

        auth()->user()->saveReport('تعديل الاسئله');

        return redirect()->route('admin.questions.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }

    public function destroy(Question $question)
    {

        $question ->delete();

        auth()->user()->saveReport(__('dashboard.questions.delete_product'));

        return self::successReturn( '' , $question);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $questions = Question::find($ids);

        Question::destroy($questions);

        auth()->user()->saveReport('حذف الاسئله');

        return self::successReturn('', $ids);

    }
}
