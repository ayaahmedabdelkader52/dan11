<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Offer\StoreRequest;
use App\Http\Requests\Offer\UpdateRequest;
use App\Models\Category;
use App\Models\Offer;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class OfferController extends Controller

{

    use HasResponse;

    public function index()
    {
        $offers = Offer::all();

        return view('admin.offers.index', compact('offers'));
    }
    public function create(){
        $categories = Category::all();

        return view('admin.offers.create' , compact('categories'));
    }

    public function store(StoreRequest $request){

        $data = $request->validated();


        Offer::create($data);

        auth()->user()->saveReport('حفظ التخفيض');

        return redirect()->route('admin.offers.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Offer $offer )
    {
        $categories = Category::all();

        return view('admin.offers.edit' , compact('offer' , 'categories'));
    }

    public function update(UpdateRequest $request , Offer $offer)
    {

        $data = $request->validated();


        $offer->update($data);

        auth()->user()->saveReport('تعديل التخفيض');

        return redirect()->route('admin.offers.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }

    public function destroy(Offer $offer)
    {

        $offer ->delete();

        auth()->user()->saveReport(__('dashboard.offers.delete_product'));

        return self::successReturn( '' , $offer);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $offer = Offer::find($ids);

        Offer::destroy($offer);

        auth()->user()->saveReport('حذف التخفيضات');

        return self::successReturn('', $ids);

    }
}
