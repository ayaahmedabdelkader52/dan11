<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\HasResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    use HasResponse;

    public function show()
    {
        $user = auth('api')->user();
        $data['user'] = new UserResource($user);

        return $this->dataReturn($data);
    }

    public function update(Request $request)
    {
        if ($request->has('phone')) {
            $request['phone'] = User::cleanPhone($request->input('phone'));
        }

        $validator = Validator::make($request->all(), [
            'name' => 'nullable|string|min:3|max:191',
            'phone' => ['nullable', 'unique:users,phone'],
            'email' => ['nullable', 'email', 'unique:users,email'],
            'avatar' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return $this->requestFailsReturn($validator);
        }

        $user = auth('api')->user();
        $user->update($request->all());

        $data['user'] = new UserResource($user);
        return $this->successReturn('', $data);
    }
}
