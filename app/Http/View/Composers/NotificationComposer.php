<?php

namespace App\Http\View\Composers;

use App\Models\Complain;
use App\Models\Contact;
use Illuminate\View\View;

class NotificationComposer
{
    private $notifications = [];
    private $messages = [];

    public function __construct()
    {
        $contacts = Contact::where('showOrNow', 0)->get();
        $complains = Complain::where('showOrNow', 0)->get();
        $notifications = auth()->check() ? auth()->user()->unreadNotifications : [];

        foreach ($contacts as $contact) {
            $this->messages[] = [
                'type' => 'contact',
                'data' => $contact,
            ];
        }

        foreach ($complains as $complain) {
            $this->messages[] = [
                'type' => 'complain',
                'data' => $complain,
            ];
        }

        $this->notifications = $notifications;
    }

    public function compose(View $view)
    {
        $view->with([
            'messages' => $this->messages,
            'notifications' => $this->notifications,
        ]);
    }
}
