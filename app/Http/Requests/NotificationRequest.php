<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg'
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'من فضلك قم بادخال عنوان الاشعار',
            'content.required'=>'من فضلك قم بادخال محتوي الاشعار',
            'image.mimes' => 'يجب ادخال صوره الاشعار بشكل  صحيح',
        ];
    }
}
