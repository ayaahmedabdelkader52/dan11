<?php

namespace App\Http\Requests\Album;

use Illuminate\Foundation\Http\FormRequest;

class storeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'=>'required',
            'name_en'=>'required',
            'subcategory_id'=>'required',
            'photo'=>'image|mimes:jpg,jpeg,svg,png',
            'offer'=>'',
            'start'=>'',
            'end'=>'',
            'price_before'=>'numeric',
            'price_after'=> 'nullable|numeric|lt:price_before',
        ];
    }
}
