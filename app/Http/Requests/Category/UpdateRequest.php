<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name_ar'=>'required|string',
            'name_en'=>'required|string',
            'description_ar'=>'required|string',
            'description_en'=>'required|string',
            'photo'=>'image|mimes:jpg,jpeg,svg,png',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
