<?php

namespace App\Http\Requests\Settings;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class AdminProfileReqeust extends FormRequest
{

    public function rules()
    {
        return [
            'avatar' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => ['required', 'phone:SA,EG', Rule::unique('users')->ignore($this->user()->id, 'id')],
            'email' => ['nullable', 'email', Rule::unique('users')->ignore($this->user()->id, 'id')],

        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => User::cleanPhone(Request::input('phone'))['phone']
        ]);
    }

    public function authorize()
    {
        return true;
    }
}
