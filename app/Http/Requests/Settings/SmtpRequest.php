<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class SmtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'nullable|min:1|max:190',
            'password' => 'nullable|min:1|max:190',
            'sender_email' => 'nullable|min:1|max:190',
            'sender_name' => 'nullable|min:1|max:190',
            'port' => 'nullable|integer',
            'host' => 'nullable|min:1|max:190',
            'encryption' => 'nullable|min:1|max:190',
        ];
    }
}
