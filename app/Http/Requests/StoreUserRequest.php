<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    public function rules()
    {

        return [
            'phone'=> ['required', 'unique:users,phone'],
            'name' => 'required|string|min:3|max:191',
            'email'=> ['nullable', 'email', 'unique:users,email'],
            'password' => 'required|confirmed|min:8',
            'avatar' => 'image|mimes:jpg,jpeg,svg,png',
            'role_id'=>'required',
            'address'=>'nullable',
        ];
    }

    public function authorize()
    {
        return true;
    }

}
