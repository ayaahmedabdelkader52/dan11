<?php

namespace App\Http\Requests\CouponRequest;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'unique:coupons',
            'value' => 'numeric|required',
            'cart_value' => 'nullable|numeric',
            'type' => 'required',
        ];
    }
}
