<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_ar' => 'string|max:255',
            'title_en' => 'string|max:255',
            'image' => 'required||mimes:jpeg,png,jpg,gif,svg',
        ];
    }

    public function messages()
    {
        return [
            'title_ar.required' => 'يجب ادخال العنوان  بالعربي',
            'title_en.required' => 'يجب ادخال العنوان بالانجليزي ',
            'image.required' => 'يجب ادخال الصوره ',
            'image.mimes' => 'يحب ادخال الصوره  بشكل صحيح',
        ];
    }
}
