<?php

namespace App\Http\Requests\Messaging;

use Illuminate\Foundation\Http\FormRequest;

class SendNotificationRequest extends FormRequest
{
    public function rules()
    {
        return [
            'message' => 'required',
            'title' => 'required',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
