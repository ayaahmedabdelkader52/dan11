<?php

namespace App\Http\Requests\Messaging;

use Illuminate\Foundation\Http\FormRequest;

class SendSMSRequest extends FormRequest
{
    public function rules()
    {
        return [
            'message' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
