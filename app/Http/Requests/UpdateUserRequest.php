<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone'=> ['required', Rule::unique('users')->ignore($this->id, 'id')],
            'name' => 'required|string|min:3|max:141',
            'email'=> ['required', 'email', Rule::unique('users')->ignore($this->user->id, 'id')],
            'avatar' => 'image|mimes:jpg,jpeg,svg,png',
            'role_id'=>'required',
            'address'=>'nullable',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
