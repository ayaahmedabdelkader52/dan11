<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Common\RegisterRequest as MainRegisterRequest;

class RegisterRequest extends MainRegisterRequest
{
    public function rules()
    {
        return $this->getRules() + [
                'name' => 'required|string|max:255',
                'device_id' => 'required',
                'device_type' => 'required|in:ios,android',
            ];
    }

}
