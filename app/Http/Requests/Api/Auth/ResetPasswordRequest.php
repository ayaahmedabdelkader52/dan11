<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Rules\CheckCodeRule;

class ResetPasswordRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'code' => ['required', new CheckCodeRule()],
            'password' => 'required|confirmed|min:8',
        ];
    }


    public function authorize()
    {
        return true;
    }
}
