<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Models\User;
use App\Rules\CheckCodeRule;
use Illuminate\Support\Facades\Request;

class ForgetPasswordRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'phone' => ['required', 'phone:SA,EG'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => User::cleanPhone(Request::input('phone'))['phone']
        ]);
    }

    public function authorize()
    {
        return true;
    }
}
