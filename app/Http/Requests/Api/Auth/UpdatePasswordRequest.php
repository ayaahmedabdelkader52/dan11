<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Rules\CheckPassword;

class UpdatePasswordRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'old_password' => ['required', 'string', new CheckPassword()],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function authorize()
    {
        return true;
    }
}
