<?php

namespace App\Http\Requests\Api;

use App\Traits\HasResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ApiRequest extends FormRequest
{
    use HasResponse;

    protected array $mainAuthRules = [
        'name' => 'required|string|min:3|max:255',
        'phone' => ['required', 'unique:users,phone', 'phone:SA,EG'],
        'email' => ['nullable', 'email', 'unique:users,email'],
        'password' => 'required|confirmed|min:8',
    ];

    protected function failedValidation(Validator $validator)
    {
        $this->validationResponse($validator);
    }
}
