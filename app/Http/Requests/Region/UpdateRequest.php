<?php

namespace App\Http\Requests\Region;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name.ar' => 'required|string',
            'name.en' => 'required|string',
            'code' => 'nullable|string',
            'population' => 'nullable|integer',
            'country_id' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
