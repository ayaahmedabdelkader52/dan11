<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreproductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name_ar'=>'required',
           'name_en'=>'required',
           'description_ar'=>'required',
           'description_en'=>'required',
           'subcategory_id'=>'required',
            'offer'=>'',
            'price_before'=>'numeric',
            'price_after'=> 'nullable|numeric|lt:price_before',
            'end'=> '' ,
            'start'=> '',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
    }
}
