<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'expiry_date' => 'required',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'يجب ادخال عنوان الاعلان',
            'content.required' => 'يجب ادخال محتوي الاعلان',
            'name' => 'يجب ادخال تاريخ انتهاء الاعلان',
            'image.mimes' => 'يحب ادخال صوره الاعلان بشكل صحيح',
        ];
    }
}
