<?php

namespace App\Http\Middleware;

use Closure;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(session()->get('lang'), app()->getLocale());
        if (session()->has('lang')) {
            session()->replace(['lang', app()->getLocale()]);
            app()->setLocale(session()->get('lang'));
        }
        // dd(session()->all());
        // dd(session()->get('lang'), app()->getLocale());
        return $next($request);
    }
}
