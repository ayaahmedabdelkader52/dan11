<?php

namespace App\Http\Middleware;

use App\Models\Device;
use App\Traits\HasResponse;
use Carbon\Carbon;
use Closure;

class APiToken
{
    use HasResponse;
    public function handle($request, Closure $next)
    {
        # MissedToken
        if (!$request->header('ApiToken')) {
            $msg  =  'من فضلك قم بتسحيل الدخول';
            return $this->failReturn($msg );
        }

}
}
