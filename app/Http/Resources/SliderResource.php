<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->id,
            'title' => (string)$this->title,
            'body' => (string)$this->body,
            'image' => (string)$this->imagePath,
            'order' => (int)$this->order,
        ];
    }
}
