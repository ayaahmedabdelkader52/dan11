<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Notification */
class NotificationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (integer)$this->id,
            'url' => (string)$this->url,
            'title' => (string)$this->title,
            'message' => (string)$this->message,
            'type' => (string)$this->type,
            'created_at' => (string)$this->created_at,
        ];
    }
}
