<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'email' => (string)$this->email,
            'country_key' => (string)$this->country_key,
            'phone' => (string)$this->fullPhone,
            'avatar' => (string)$this->avatarPath,
            'status' => (string)$this->status,
            'type' => (string)$this->type,
            'gender' => (string)$this->gender,
        ];
    }


}
