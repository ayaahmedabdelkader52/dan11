<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class ApplicationDirectionViewComposer
{
    public function compose(View $view)
    {
        $appDirection = (app()->getLocale() == 'ar') ? '-rtl' : '';
        $view->with([
            'dir' => $appDirection,
        ]);
    }
}
