<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class HismsService implements MessagingService
{

    public function send($phone, $msg, $data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $sender = urlencode($data['sender']);
        $msg = urlencode( $msg);

        $url  = "https://www.hisms.ws/api.php?send_sms&username=$username&password=$password&numbers=$phone&sender=$sender&message=$msg";
        $result = file_get_contents($url,true);
        if($result === false){
            return false;
        }else{
            return true;
        }
    }
}
