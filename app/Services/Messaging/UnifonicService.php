<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class UnifonicService implements MessagingService
{

    public function send($phone, $msg, $data)
    {
        $text     = urlencode( $msg);
        $numbers  = explode(',', $phone);
        $username = $data['username'];
        $password = $data['password'];
        $sender = urlencode($data['sender']);

        $url = "http://api.unifonic.com/wrapper/sendSMS.php?userid=$username&password=$password&msg=$text&sender=$sender&to=$numbers&encoding=UTF8";
        $result = @file_get_contents($url,true);
        if($result === false){
            return false;
        }else{
            return true;
        }
    }
}
