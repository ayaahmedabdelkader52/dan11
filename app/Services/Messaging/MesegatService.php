<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class MesegatService implements MessagingService
{

    public function send($phone, $msg, $data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $sender = $data['sender'];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.msegat.com/gw/sendsms.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        $fields = <<<EOT
        {
        "userName": "$username",
        "numbers": "$phone",
        "userSender": "$sender",
        "apiKey": "$password",
        "msg": "$msg"
        }
        EOT;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json"
        ));

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return true;
    }
}
