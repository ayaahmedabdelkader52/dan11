<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class ZainService implements MessagingService
{

    public function send($phone, $msg, $data)
    {
        sleep(1);
        $username = $data['username'];
        $password = $data['password'];
        $sender = $data['sender'];
        $to = $phone; // Should be like 966530007039
        $text = urlencode($msg . '   ');

        $link = "https://www.zain.im/index.php/api/sendsms/?user=$username&pass=$password&to=$to&message=$text&sender=$sender";

        /*
         *  return  para      can be     [ json , xml , text ]
         *  username  :  your username on safa-sms
         *  passwpord :  your password on safa-sms
         *  sender    :  your sender name
         *  numbers   :  list numbers delimited by ,     like    966530007039,966530007039,966530007039
         *  message   :  your message text
         */

        /*
         * 100   Success Number
         */

        if (function_exists('curl_init')) {
            $curl = @curl_init($link);
            @curl_setopt($curl, CURLOPT_HEADER, FALSE);
            @curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            @curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
            @curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
            $source = @curl_exec($curl);
            @curl_close($curl);
            if ($source) {
                return $source;
            } else {
                return @file_get_contents($link);
            }
        } else {
            return @file_get_contents($link);
        }
    }
}
