<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class MobilyService implements MessagingService
{
    public function send($phone, $msg, $data)
    {
        sleep(1);
        $url = 'http://api.yamamah.com/SendSMS';
        $username = $data['username'];
        $password = $data['password'];
        $sender = $data['sender'];
        $to = $phone; // Should be like 966530007039
        $text = urlencode($msg);
        $sender = urlencode($sender);
        $fields = array(
            "Username" => $username,
            "Password" => $password,
            "Tagname" => $sender,
            "Message" => $text,
            "RecepientNumber" => $to,
        );
        $fields_string = json_encode($fields);
        //open connection
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            CURLOPT_POSTFIELDS => $fields_string,
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
