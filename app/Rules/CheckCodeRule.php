<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class CheckCodeRule implements Rule
{

    public function __construct(User $user = null)
    {
        if ($user) {
            $this->user = $user;
        } else {
            $this->user = auth()->user();
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->user->code == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('auth.invalid_code');
    }
}
