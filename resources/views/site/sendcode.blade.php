{{ App\Models\Language::find(1)->{app('lang')} }}

<!doctype html>
<html lang="ar">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('Site/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/css/animate.css')}}">

    <!-- plugins CSS -->
    <link rel="stylesheet" href="{{asset('Site/assets/plugins/owl-carousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/plugins/nice-select/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/plugins/select2/css/select2.min.css')}}">

    <!-- my CSS -->
    <link rel="stylesheet" href="{{asset('Site/assets/css/style.css')}}">
    <!-- <link rel="" href="css/style-ltr.css"> -->
    <!-- title logo -->
    <link rel="icon" href="{{asset('Site/assets/imgs/logo.png')}}" type="image/x-icon" />


</head>

<body>
    <!-- Start body-content -->

    <section class="login-content">
        <div class="loginform">
            <form   id="codeForm">
                @csrf
                <img class="form-logo" src="{{asset('Site/assets/imgs/logo-w.png')}}">
                <p>
                    {{Session::has('lang')&&Session::get('lang')=='ar'? $setting['sendcode_description_ar'] : $setting['sendcode_description_en'] }}
                </p>
                <div class="form-group">
                    <label for="">  {{trans('site.auth.code')}}    </label>
                    <input type="text" name="code" class="form-control">

                </div>
                <div class="form-group">
                    <label for="">    {{trans('site.auth.new_password')}}    </label>
                    <input type="password" name="password" class="form-control">

                </div>
                <div class="form-group">
                    <label for="">  {{trans('site.auth.password_confirmation')}}    </label>
                    <input type="password" name="password_confirmation" class="form-control">

                </div>

                <div class="text-center">


                    <button type="submit" id="codebtn" class="btn-black">
                        {{trans('site.contact_us.send')}}
                    </button>




                </div>

            </form>
        </div>

    </section>
    <!-- end body-content -->









    <!--========================== Start Loading Page ======================-->

    <div class="loader">
        <img src="{{asset('Site/assets/imgs/logo.png')}}" alt="">
    </div>

    <!--========================= End Loading Page =========================-->



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('Site/assets/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('Site/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('Site/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Site/assets/js/wow.min.js')}}"></script>

    <!-- plugins JS -->
    <script src="{{asset('Site/assets/plugins/owl-carousel/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('Site/assets/plugins/nice-select/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('Site/assets/plugins/select2/js/select2.min.js')}}"></script>

    <!-- my JS -->
    <script src="{{asset('Site/assets/js/main.js')}}"></script>
    <script>
        new WOW().init();
    </script>

    <script>
        $("#codebtn").on('click',function(e) {

            var form = $('#codeForm').get(0);
            var formData = new FormData(form);
            var oldText = $(this).text();
            $(this).prop('disabled', true).css({
                opacity: '0.5',

            }).text("{{trans('site.action.loading')}}");
            console.log(oldText)
            $.ajax({
                url:"{{route('send' , $user)}}",
                type:"POST",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    $('#codebtn').removeAttr("disabled").css({
                        opacity:'1'
                    }).text(oldText);
                    if(data.key =='success'){
                        location.assign(data.msg);
                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })

                    }
                }
            });

        })



    </script>

</body>

</html>
