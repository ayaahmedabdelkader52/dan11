{{ App\Models\Language::find(1)->{app('lang')} }}
@extends('site.layout.master')

@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Join us':'انضم الينا'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="about">
            <div class="container">
                <div class="page-title">
                    <h5>  </h5>
                    <h4> {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['join_us_name_ar'] : $setting['join_us_name_en'] !!} </h4>

                </div>
                <div class=" joinus-imgs sec-padd">
                    <div class="img-filter">
                        <img src="{{asset('assets/uploads/settings/'.$setting['join_us_photo1'])}}" alt="">

                    </div>
                    <div class="img-filter">
                        <img src="{{asset('assets/uploads/settings/'.$setting['join_us_photo2'])}}" alt="">

                    </div>
                    <div class="img-filter">
                        <img src="{{asset('assets/uploads/settings/'.$setting['join_us_photo3'])}}" alt="">

                    </div>

                    <div class="img-filter">
                        <img src="{{asset('assets/uploads/settings/'.$setting['join_us_photo4'])}}" alt="">

                    </div>
                </div>
                <div class="about-p " style="text-align: justify;">
                    <p>
                            {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['join_us_description_ar'] : $setting['join_us_description_en'] !!}
                    </p>



                </div>
                <div class="pt-20 pb-20">
                    <a href="https://wa.me/{{$setting['site_phone']}}" class=" btn-main " target="_blank ">
                        <i class="fa fa-whatsapp" aria-hidden="true"></i> {{trans('site.contact_us.join')}}
                    </a>
                </div>

            </div>
        </div>



    </section>
@endsection
