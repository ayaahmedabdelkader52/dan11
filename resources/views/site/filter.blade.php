


<div id="filtration_area">
    <div class="row product-boxs pt-20">
        @foreach($products as $product)
            <div class="col-md-4">
                <div class="product-box">
                    <a href="{{route('product',  $product->id)}}" class="product-img">
                        <img src="{{asset('assets/uploads/products/'.$product->photo)}}" alt="">
                        <div class="product-abs">
                            <h5>{{Session::has('lang')&&Session::get('lang')=='en'?$product->name_en:$product->name_ar}}</h5>
                            <p> {{trans('main.seemore')}} </p>
                        </div>
                    </a>
                    <div class="product-info">
                        <div class="price d-flex">

                                <h4> {{$product->price_after}} </h4>

                        </div>
                        <div class="d-flex">

                            <button style="margin-left: 10px;" id="favoritebtn" class="icon like_active"> <i class="fa fa-heart"> </i> </button>

                            <form id="addForm">
                                @csrf
                                <input required="" type="hidden" name="id" value="{{$product->id}}" >
                                <button  id="addCart"  > <i class="fa fa-shopping-basket"> </i> </button>
                            </form>
                            <a class="icon" href="{{route('product',  $product->id)}}"> <i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                        </div>

                    </div>
                    <div class="rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>


                        <span>   {{trans('main.rates')}}</span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
