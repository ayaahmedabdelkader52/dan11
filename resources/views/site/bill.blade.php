{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title',' نموذج الفاتوره ')

@section('content')

    <section class="body-content">
        <div class="container pt-20 pb-20">
            <div class="page-title text-right">
                <h4 class="main-color"> {{__('site.bill.bill')}} </h4>


            </div>

            <form action="{{route('checkout')}}" method="post">
                @csrf

                <div class="bg-gray p-15 ">
                    <h6 class="gray-color f-b">
                        معلومات عامه
                    </h6>
                </div>

                <div class="row contact-form bill-form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                {{__('site.users.phone')}}
                                <i class="red-color">*</i>
                            </label>
                          <input type="number" name="phone" class="form-control" placeholder="رقم الجوال">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                         <label>
                             {{__('site.users.phone_extra')}}
                            <i class="red-color ">*</i>
                         </label>
                            <input type="number" name="extra phone" class="form-control" placeholder="رقم الجوال">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                {{__('site.users.e-mail')}}
                                <i class="red-color ">*</i>
                            </label>
                             <input type="email" name="email" class="form-control" placeholder=" الإيميل  ">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                {{__('site.country.country')}}
                            </label>
                            <select type="number" name="country" class="form-control select-2">
                                <option> مصر</option>
                                <option> مصر</option>
                                <option> مصر</option>
                                <option> مصر</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                {{__('site.country.city')}}
                            </label>
                            <select type="number" name="city" class="form-control select-2">
                                <option> مصر</option>
                                <option> مصر</option>
                                <option> مصر</option>
                                <option> مصر</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                {{__('site.advert.address')}}
                            </label>

                            <input type="text" name="address" class="form-control ">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                {{__('site.advert.order_date')}}
                            </label>

                            <input type="date" name="order_date" class="form-control ">

                        </div>
                    </div>
                </div>

                <div class="bg-gray d-flex justify-content-between mb-20 p-15">
                    <h6 class="gray-color f-b">
                        {{__('site.bill.payment_type')}}
                    </h6>

                    <label class="gray-color ">
                        <input type="radio" value="cash" name="type_payment">
                        <span> {{__('site.bill.cash')}}</span>
                    </label>

                    <label class="gray-color ">
                        <input type="radio" value="online" name="type_payment">
                        <span> {{__('site.bill.online')}} </span>
                    </label>

                </div>



                <div class="page-title text-right">
                    <h4 class="main-color"> {{__('site.bill.bill')}} </h4>
                </div>

                <div class="bg-gray d-flex   mb-20 p-15">
                    <h6 class="gray-color f-b mr-10 ml-10"> {{trans('site.bill.final_price')}} </h6>
                    @if($total > 0)
                    <h6 class="black-color f-b mr-10 ml-20 "> {{$total}} </h6>
                    @else
                    <h6 class="black-color f-b mr-10 ml-20 "> 0 </h6>
                    @endif

                    <h6 class="gray-color f-b mr-10 ml-10"> الخصم أن وجد : </h6>
                    <h6 class="black-color f-b mr-10"> 13 ريال </h6>
                </div>

                @foreach($products as $product)

                <h6 class="mt-20 mb-20 f-b">{{__('site.bill.billdan')}}</h6>
                <ul class="bill-card">
                    <li>
                        <h6>{{trans('site.users.name')}}</h6>
                        <h6> {{Session::has('lang')&&Session::get('lang')=='ar'? $product['item']['name_ar'] : $product['item']->name_en}}</h6>
                    </li>
{{--                    @if( $product['item']->characters ->count() > 0 )--}}
{{--                        @foreach($product['item']->characters as $character)--}}
{{--                    <li>--}}
{{--                        <h6> {{trans('site.bill.selectednames')}} </h6>--}}
{{--                        <h6> {{Session::has('lang')&&Session::get('lang')=='ar'?$character['name_ar'] : $character->name_en}} </h6>--}}
{{--                    </li>--}}
{{--                        @endforeach--}}
{{--                    @endif--}}
{{--                    @if( $product['item']->singers ->count() > 0 )--}}
{{--                        @foreach($product['item']->singers as $singer)--}}
{{--                    <li>--}}
{{--                        <h6>{{trans('site.bill.singer')}}</h6>--}}
{{--                        <h6> {{Session::has('lang')&&Session::get('lang')=='ar'? $product['item']->singers['name_ar'] : $product['item']->singers->name_en}}</h6>--}}
{{--                    </li>--}}
{{--                        @endforeach--}}
{{--                    @endif--}}
                    <li>
                        <h6>
                            {{trans('site.bill.qty')}}
                        </h6>
                        <h6>
                            {{$product['qty']}}
                        </h6>
                    </li>

                </ul>

                @endforeach

                <button class="btn-main mb-20"> {{trans('site.main.send_order')}}</button>

            </form>
        </div>




    </section>

@endsection

