{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Profile':'معلومات'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="profile-page sec-padd">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-img">
                            <img src="{{ auth()->user()->avatar ?asset('assets/uploads/users/' . auth()->user()->avatar) : asset('assets/uploads/users/default.png')  }}" alt="">
                        </div>
                        <a class="logout" href="{{route('logout')}}">
                            {{trans('site.auth.logout')}}
                          </a>
                    </div>
                    <div class="col-md-8">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">
                                    معلومات الأساسيه
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">تعديل الحساب </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">طلباتي</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="profile-info pt-20">
                                    <h3>
                                        المعلومات الأساسيه

                                    </h3>
                                    <h6>
                                        {{auth()->user()->name}}

                                    </h6>
                                    <p> {{auth()->user()->phone}}</p>
                                    <p> {{auth()->user()-> email }}</p>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                                <form class=" profile-form pt-20 pb-20" id="editForm">
                                    @csrf
                                    <div class="images-upload-block   ">
                                        <label class="upload-img">
                                          <input type="file" name="avatar" id="image" accept="image/*" class="image-uploader">
                                              <img src="{{ auth()->user()->avatar ?asset('assets/uploads/users/' . auth()->user()->avatar) : asset('assets/uploads/users/default.png')  }}">
                                                  <i> + </i>
                                         </label>

                                        <div class="upload-area"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for=""> {{trans('site.users.name')}}      </label>
                                        <input type="text" class="form-control" name="name" value="{{auth()->user()->name}}">

                                    </div>

                                    <div class="form-group">
                                        <label for="">    {{trans('site.users.e-mail')}}        </label>
                                        <input type="email" class="form-control" name="email" value="{{auth()->user()->email}}">

                                    </div>

                                    <div class="form-group">
                                        <label for="">   {{trans('site.users.phone')}}     </label>
                                        <input type="number" name="phone" class="form-control" value="{{auth()->user()->phone}}">


                                    </div>

                                    <button type="submit" id="editbtn" class="btn-main mt-20">
                                        {{trans('site.action.save')}}
                                            </button>

                                </form>


                                <hr>
                                <div class="more-setings">
                                    <h5> أعدادات الحساب </h5>
                                    <div class="setings">
                                        <h6> تغير كلمه المرور</h6>
                                        <button class="font-20" data-toggle="modal" data-target="#password_modal">
                                            <i class="fa fa-angle-left" aria-hidden="true"></i>

                                        </button>
                                    </div>
                                    <h5> الاعدادات </h5>
                                    <div class="setings">
                                        <h6> أرسال اشعار علي الأيميل </h6>

                                        <label class="switch">
                                            <input type="checkbox" >
                                            <span class="span-check round">  </span>
                                            </label>
                                    </div>
                                    <h5> اللغه </h5>
                                    <div class="setings">
                                        <h6> English </h6>
                                        <div class="d-flex">
                                            <label class="switch">
                                            <input type="checkbox" checked>
                                            <span class="span-check round">  </span>
                                            </label>
                                            <h6> العربيه</h6>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="orders">
                                    <ul>
                                        @if($orders -> count() > 0)
                                            @foreach($orders as $order)
                                        @foreach($products as $product)
                                        <li class="order-item">
                                            <h6>
                                                <span class="d-block">

                                                </span>
                                                <span class="d-block"> اليوم </span>
                                            </h6>
                                            <div>
                                                <p>

                                                </p>

                                                <p class="f-b">
                                                    {{$product->price}}
                                                </p>
                                            </div>
                                            <button class="open-details main-color">
                                                مشاهدة المزيد
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="show-order-details">
                                    <button class="back-to-orders f-b font-20 main-color ">
                                        <i class="fa fa-angle-right"></i>
                                    </button>
                                    <div class="order-details">
                                        <div>
                                            <h6>
                                                تفاصيل الطلب
                                            </h6>
                                            <h6>
                                                الاجمالي
                                            </h6>
                                            <p>

                                            </p>
                                        </div>

                                        <hr>
                                        <div class="bg-color">
                                            <h6 class="main-color">
                                                طريقه الدفع

                                            </h6>
                                            <h6 class="main-color">
                                                @if($order -> type_payment == 'cash')
                                                    كاش
                                                @else
                                                    اونلاين
                                                @endif

                                            </h6>
                                        </div>
                                        <div>
                                            <h6 class="black-color"> قيمه الخصم </h6>
                                            <h6 class="black-color"> 2 % _ 20 ريال</h6>
                                        </div>
                                        <div>
                                            <h6 class="black-color"> الاجمالي </h6>
                                            <h6 class="black-color"> 300 ريال</h6>
                                        </div>
                                        <hr>
                                        <h6 class="f-b"> تفاصيل المستخدم</h6>
                                        <div class="more-details">
                                            <h6>
                                                <span> أسم موظفه المبيعات</span>
                                                <span> Dalia </span>
                                            </h6>
                                            <h6>
                                                <span>      رقم او رمز الطلب </span>
                                                <span> {{$order -> id}} </span>
                                            </h6>
                                            <h6>
                                                <span>   رقم الجوال </span>
                                                <span>  {{$order->phone}} </span>
                                            </h6>
                                            <h6>
                                                <span>    رقم جوال اضافي   </span>
                                                <span>   {{$order->extra_phone}} </span>
                                            </h6>
                                            <h6>
                                                <span>     الإيميل       </span>
                                                <span>   {{auth()->user()->email}}  </span>
                                            </h6>
                                            <h6>
                                                <span>     الدوله       </span>
                                                <span>   {{$order->country}}  </span>
                                            </h6>
                                            <h6>
                                                <span>     المدينه       </span>
                                                <span>   {{$order->city}}  </span>
                                            </h6>
                                            <h6>
                                                <span>     العنوان       </span>
                                                <span>   {{$order -> address}}  </span>
                                            </h6>
                                        </div>
                                        <div class="bg-color">
                                            <h6 class="main-color">
                                                حاله الطلب
                                            </h6>
                                            <h6 class="main-color">
                                                @if($order -> status == 'wait')
                                                    في الانتظار
                                                @elseif($order -> status == 'paid')
                                                    مدفوع
                                                @else
                                                    منتهي
                                                @endif

                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endforeach
                                        @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="modal fade" id="password_modal" tabindex="-1" role="dialog" aria-labelledby="password_modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <form class=" contact-form pt-20 pb-20" id="passwordForm">

                        @csrf

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">    كلمة المرور القديمة      </label>
                                <input type="password" name="oldpassword" class="form-control">

                            </div>

                            <div class="form-group">
                                <label for="">     كلمة المرور الجديده        </label>
                                <input type="password" name="password" class="form-control">

                            </div>

                            <div class="form-group">
                                <label for="">   تاكيد كلمة المرور الجديده      </label>
                                <input type="password"  name="password_confirmation" class="form-control">


                            </div>

                        </div>
                        <div class="modal-footer">
                            <button class="btn-main btn-small ml-20 mr-20" id="savebtn" data-dismiss="modal">
                                حفظ                        </button>
                            <button class="btn-main btn-small ml-20 mr-20  btn-main-w" data-dismiss="modal">
                                الغاء
                            </button>

                        </div>
                    </form>

                </div>
            </div>
        </div>


    </section>

@endsection
@section('scripts')
    <script>
        $("#editbtn").on('click',function(e){

            var form = $('#editForm').get(0);
            var formData = new FormData(form);
            var oldText = $(this).text();
            $(this).prop('disabled', true).css({
                opacity:'0.5'
            }).text("{{trans('site.action.loading')}}");


            $.ajax({
                url:"{{route('storeuser')}}",
                type:"POST",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    $('#editbtn').removeAttr("disabled").css({
                        opacity:'1'
                    }).text(oldText);
                    if(data.key == 'success'){
                        location.assign(data.msg)
                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });

        $("#savebtn").on('click',function(e){

            var form = $('#passwordForm').get(0);
            var formData = new FormData(form);
            var oldText = $(this).text();
            $(this).prop('disabled', true).css({
                opacity:'0.5'
            }).text("{{trans('site.action.loading')}}");


            $.ajax({
                url:"{{route('updatePassword')}}",
                type:"POST",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    $('#savebtn').removeAttr("disabled").css({
                        opacity:'1'
                    }).text(oldText);
                    if(data.key == 'success'){
                        location.assign(data.msg)
                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });
    </script>
@endsection
