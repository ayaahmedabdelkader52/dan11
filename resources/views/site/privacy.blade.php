{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Privacy ':'سياسه الخصوصيه'}}
@endsection

@section('content')

    <section class="body-content">

        <div class="rules-page container sec-padd">
            <div class="page-title text-right m-0 mb-20">
                {!!Session::has('lang')&&Session::get('lang')=='ar'? $setting['privacy_title_ar'] : $setting['privacy_title_en'] !!}
            </div>
        </div>
            <div class="container">

                {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['privacy_content_ar'] : $setting['privacy_content_en'] !!}


        </div>


    </section>
@endsection
