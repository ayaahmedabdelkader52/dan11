{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title' )
    {{Session::has('lang')&&Session::get('lang')=='en'?'Cart':' السله'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="container pt-20 pb-20">
            <div class="page-title text-right">
                <h4 class="main-color"> {{__('site.main.cart')}} </h4>

            </div>
            <form action="{{route('bill')}}" method="get">
                @if(Session::has('cart'))
                <div class="table-responsive ">
                    <table class="cart-table  table  table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">{{trans('site.users.image')}}</th>
                                <th scope="col">{{trans('site.main.product')}} </th>
                                <th scope="col"> {{trans('site.advert.address')}} </th>
                                <th scope="col"> {{trans('site.main.price_one')}} </th>
                                <th scope="col"> {{trans('site.Messages.delete')}} </th>
                            </tr>
                        <tr> {{__('site.main.total')}} </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>
                                    <img src="{{$product['item']['photopath']}}">
                                </td>
                                <td>
                                    <div>
                                        <h6>{{Session::has('lang')&&Session::get('lang')=='ar'? $product['item']['category']['name_ar'] : $product['item']['category']['name_en']}} </h6>
                                        <h5>
                                            {{Session::has('lang')&&Session::get('lang')=='ar'? $product['item']['name_ar'] : $product['item']['name_en']}}
                                            <a class="main-color" href="{{route('product' , $product['item']['id'])}}">
                                                ({{__('site.cart.view_product')}})
                                            </a>
                                        </h5>

                                    </div>

                                </td>

                                <td>{{auth()->user()->address}}</td>

                                <td>
                                    {{ $product ['item']['price_after']}}
                                </td>
                                <td> <a href="{{route('reduce',$product['item']['id'])}}"><i class="fa fa-trash"></i> </a> </td>
                            </tr>
                        @endforeach
                        <tr>{{$totalPrice}}</tr>
                        </tbody>
                    </table>
                </div>


                <div class="bg-gray p-15 d-flex justify-content-between align-items-center mb-20">
                    <h6 class="grey-color f-b">
                        {{trans('site.main.have_coupon')}}
                    </h6>
                    <i class="fa fa-caret-left main-color" aria-hidden="true"></i>
                </div>

                <div class="code-form">
                    <input type="text" name="coupon" placeholder="{{trans('site.main.coupon')}}">
                    <button type="submit">
                        {{trans('site.main.activation')}}
                     </button>
                </div>




                <button class="btn-main mb-20" type="submit">
                    {{trans('site.main.send_order')}}
            </button>
            </form>
        </div>

        @else
            <form action="{{route('index')}}" method="get">
            <div class="table-responsive">
                <table class="cart-table  table  table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">{{trans('site.users.image')}}</th>
                        <th scope="col">{{trans('site.main.product')}} </th>
                        <th scope="col"> {{trans('site.advert.address')}} </th>
                        <th scope="col"> {{trans('site.main.price_one')}} </th>
                        <th scope="col"><a href="{{route('reduce')}}"> {{trans('site.Messages.delete')}} </a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="5">{{trans('site.main.no_items')}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>



                <button class="btn-main mb-20" type="submit">
                    {{__('site.main.make_order')}}
                </button>
            </form>
        @endif


    </section>

    <!-- end body-content -->




@endsection
