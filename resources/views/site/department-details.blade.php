{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')
@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Albums':'الالبومات'}}
@endsection
@section('content')

    <section class="body-content">
        <div class="top-cover">
            <div class="top owl-carousel owl-theme">

                <div class="item">
                    <img src="{{asset('Site/assets/imgs/slider2.png')}}" width="100%">
                    <div class="item-abs">
                        <h5>
                            اول ستديو تصوير بجميع الخدمات
                            <span class="main-color"> تعريفي عنه </span>
                        </h5>
                    </div>
                </div>

            </div>
            <div class="top-info">

                <div>
                    <p>العديد من المنتجات</p>
                    <h6> أختر المنتج الذي تريده</h6>
                </div>
                <div>
                    <p>  {{__('site.action.add_cart')}}</p>
                    <h6> أسهل الطرق للشراء </h6>
                </div>
                <div>
                    <p> {{__('site.action.pay')}} </p>
                    <h6> عمليه الدفع والشحن </h6>
                </div>
                <div>
                    <a href="{{route('cart')}}">أبدء الشراء  الآن
                        <i class="fa fa-long-arrow-left" aria-hidden="true"></i>

                    </a>
                </div>
            </div>

        </div>
        <div class="department-details sec-padd">
            <div class="container">
                <div class="title">
                    <h6> وصف </h6>
                    <p>
                     {{Session::has('lang')&&Session::get('lang')=='ar'? $category -> description_ar : $category -> description_en}}
                    </p>

                </div>
                <div class="row pt-20">
                @foreach($albums as $album)
                    <div class="col-md-4">
                        <a href="{{route('album' , $album)}}" class="box">
                            <div class="imgs">
                                <img src="{{$album -> photoPath}}" alt="">
                            </div>
                            <h6>{{$album -> name_ar}}</h6>
                        </a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>



    </section>
@endsection

