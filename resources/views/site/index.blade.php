{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Home':'الصفحه الرئيسيه'}}
@endsection

@section('content')


    <section class="body-content">


        <div class="top-cover">
            @foreach($sliders as $slide)
            <div class="top owl-carousel owl-theme">
                <div class="item">
                    <img src="{{ asset('assets/uploads/slids/' . $slide -> image) }}" width="100%">
                    <div class="item-abs">
                        <h5>
                            {{Session::has('lang')&&Session::get('lang')=='ar'? $slide -> title_ar : $slide->title_en }}

                        </h5>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
        <div class="departments">
            <div class="container">
                <div class="title">
                    <h6>{{trans('site.main.categories')}} </h6>
                    <p>
                        {{Session::has('lang')&&Session::get('lang')=='ar'? 'يمكنك أختيار مايناسبك وأضافه المزيد من الأختيارات' : 'You Can Choose what you need and add more'}}
                     </p>
                 </div>
                 <div class="row">

                 @foreach($categories as $category)

                     <div class="div1">
                         <a href="{{route('products' , $category)}}" class="box">
                            <div class="imgs">
                                <img src="{{$category -> photoPath}}" alt="">

                            </div>
                            <h6> {{Session::has('lang')&&Session::get('lang')=='ar'?$category -> name_ar:$category->name_en}} </h6>
                        </a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

    </section>
@endsection








