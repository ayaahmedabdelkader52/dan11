@extends('site.layout.master')

@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Favourites ':'المفضله'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="container sec-padd">
            <div class="title m-0 mb-20">
                <h6>{{Session::has('lang')&&Session::get('lang')=='ar'?'قائمه المفضله لديك':'Favourite Menu'}} </h6>

            </div>
            @if($favorites -> count() > 0)
                <div class="row product-boxs pt-20">
                @foreach($products as $product)
                    <div class="col-md-4">
                        <div class="product-box" id="like-{{$product->id}}">
                            <a href="{{route('product' , $product)}}" class="product-img">
                                <img src="{{$product -> photoPath}}" alt="">
                                <div class="product-abs">
                                    <h5> {{Session::has('lang')&&Session::get('lang')=='ar'? $product -> name_ar : $product -> name_en}} </h5>
                                </div>
                            </a>

                            <div class="product-info">
                                <h6>{{ $product->price_after }} </h6>
                                <div class="d-flex">

                                    <button class="icon favoritebtn  @if(App\Models\Favourite::where('user_id',Auth::user()->id)->where('product_id',$product->id)->first()) like_active @endif"  data-like="{{$product->id}}" id="like"> <i class="fa fa-heart"> </i> </button>
                                    <button class="icon" name="cart" > <a  href="{{route('add-to-cart' , $product->id)}}"> <i class="fa fa-shopping-basket"> </i> </a> </button>
                                    <a class="icon" href="{{route('product' , $product)}}"> <i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

                                </div>

                            </div>

                            <div class="rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                                <span>   7 تقيمات</span>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
            @else
                <div class="center">
                    {{__('site.main.no_likes')}}
                </div>
            @endif
        </div>



    </section>

@endsection
@section('scripts')
    <script>
        $(document).on('click','.favoritebtn',function(e){
            e.preventDefault();
            var id   = $(this).data('like')
            var _token = $("._token").val();
            if($(this).hasClass('like_active')){
                $(this).removeClass('like_active');
                $('#like-'+ id).remove();
            }else{
                $(this).addClass('like_active');
            }


            $.ajax({
                url:"{{route('like')}}",
                type:"POST",
                data:{
                    id: id,
                    _token:_token
                },
                dataType: "json",
                success:function(data){

                }
            });

        });
    </script>
@endsection
