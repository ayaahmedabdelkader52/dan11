<!doctype html>
<html >

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>

    @if(Session::has('lang') && Session::get('lang')=='en')
        <link rel="stylesheet" href="{{asset('Site/assets/css/style-ltr.css')}}">
    @endif

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('Site/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/css/animate.css')}}">
    <!-- SweetAlert css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">


    <!-- plugins CSS -->
    <link rel="stylesheet" href="{{asset('Site/assets/plugins/owl-carousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/plugins/nice-select/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('Site/assets/plugins/select2/css/select2.min.css')}}">


        <!-- my CSS -->
    <link rel="stylesheet" href="{{asset('Site/assets/css/style.css')}}">
    <!-- <link rel="" href="css/style-ltr.css"> -->
    <!-- title logo -->
    <link rel="icon" href="{{asset('assets/uploads/settings/' . $setting['logo'])}}" type="image/x-icon" />


</head>

<body>
<!-- BEGIN: Header-->
    @include('site.includes.header')
<!-- END: Header-->

<a class="whatsapp-div" href="https://wa.me/{{ $setting['site_phone'] }}" target="_blank" data-toggle="tooltip" title="تواصل معنا عبر الواتساب">
    <img src="{{asset('Site/assets/imgs/whatsapp (1).png')}}" width="30px" height="30px">
</a>

<!-- Start body-content -->
@yield('content')
<!-- end body-content -->


@include('site.includes.footer')


<!--========================== Start Loading Page ======================-->

<div class="loader">
    <img src="{{asset('Site/assets/imgs/logo.png')}}" alt="">
</div>

<!--========================= End Loading Page =========================-->



<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('Site/assets/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('Site/assets/js/popper.min.js')}}"></script>
<script src="{{asset('Site/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Site/assets/js/wow.min.js')}}"></script>

<!-- SweetAlert js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>

<!-- plugins JS -->
<script src="{{asset('Site/assets/plugins/owl-carousel/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('Site/assets/plugins/nice-select/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('Site/assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- my JS -->
<script src="{{asset('Site/assets/js/main.js')}}"></script>
<script>
    new WOW().init();
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('scripts')

</body>

</html>
