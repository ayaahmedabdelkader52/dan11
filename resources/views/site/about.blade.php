{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')
@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'About Us':' عن الموقع'}}
@endsection
@section('content')

    <section class="body-content">
        <div class="about">
            <div class="container">
                <div class="page-title">

                    <h4>  {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['about_us_name_ar'] : $setting['about_us_name_en'] !!}</h4>

                </div>
                <div class="row about-imgs sec-padd">
                    <div class="col-md-4 padd-5">
                        <img src="{{asset('assets/uploads/settings/'.$setting['about_us_photo1'])}}" class="about-img ">
                    </div>
                    <div class="col-md-4 padd-5">
                        <img src="{{asset('assets/uploads/settings/'.$setting['about_us_photo2'])}}" class="about-img img-s ">
                        <img src="{{asset('assets/uploads/settings/'.$setting['about_us_photo3'])}}" class="about-img img-s">
                    </div>
                    <div class="col-md-4 padd-5">
                        <img src="{{asset('assets/uploads/settings/'.$setting['about_us_photo4'])}}" class="about-img ">
                    </div>
                </div>
                <div class="about-p">

                    <p>
                        {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['about_us_description_ar'] : $setting['about_us_description_en'] !!}
                    </p>


                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="about-box">
                            <img src="{{asset('assets/uploads/settings/'.$setting['about_us_card_photo'])}}" alt="">

                            <h5>
                            {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['about_us_card_ar'] : $setting['about_us_card_en'] !!}
                            </h5>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="about-box">
                            <img src="{{asset('assets/uploads/settings/'.$setting['about_us_messege_photo'])}}" alt="">
                            <h5> {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['about_us_rate_ar'] : $setting['about_us_rate_en'] !!} </h5>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="about-box">
                            <img src="{{asset('assets/uploads/settings/'.$setting['about_us_rate_photo'])}}" alt="">
                            <h5> {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['about_us_message_ar'] : $setting['about_us_message_en'] !!}</h5>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </section>


@endsection






