{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')


@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Questions':'الاسئله الشائعه'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="question-page">
            <div class="container sec-padd">
                <div class="row">

                    <div class="col-md-6">
                        <div class="page-title text-right">
                            <h4>{!! Session::has('lang')&&Session::get('lang')=='ar'?$setting['faqs_name_ar'] : $setting['faqs_name_en'] !!} </h4>

                        </div>
                        <p>
                            {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['faqs_description_ar'] : $setting['faqs_description_en']  !!}
                        </p>
                    </div>

                    <div class="col-md-6">
                        <form class="contact-form pt-20" id="question-form">
                            @csrf
                            @if(! auth()->check())
                            <div class="form-group">
                                <label for="">   {{trans('site.users.name')}}      </label>
                                <input type="text" name="name" class="form-control" value="اوامر الشبكه">

                            </div>
                            @endif
                            <div class="form-group">
                                <label for="">    {{trans('site.contact_us.subject_name')}}      </label>
                                <input type="text" name="subject" class="form-control" placeholder="اكتب ما تريد">

                            </div>

                            <div class="form-group">
                                <label for="">     {{trans('site.contact_us.subject')}}         </label>
                                <textarea type="text" name="text" class="form-control" placeholder="اكتب ما تريد"></textarea>

                            </div>


                            <button class="btn-main" id="question-btn">
                                {{trans('site.main.send')}}
                                    </button>

                    </form>

                </div>
            </div>

                <div class="sec-padd">
                    <div id="map" style="width: 100%; height: 10px; ">
                    </div>
                </div>

                <div class="before-q sec-padd">
                    <div class="page-title ">

                       {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['view_questions_ar'] : $setting['view_questions_en'] !!}


                    </div>
            </div>
            <div class="container">
                <form id="question-form">
                    <div class="question-search">
                        <input type="text" placeholder=" {{trans('site.main.search')}}  ">
                        <button type="submit" class="btn-footer">   {{trans('site.main.search')}} </button>
                    </div>
                </form>


                <div id="accordion">
                    @foreach($questions as $question)
                    <div class="card">
                        <div class="card-header" id="headingOne">

                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <span>
                                  {{Session::has('lang')&&Session::get('lang')=='ar'? $question -> question_ar : $question -> question_en}}

                                </span>

                                     <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </button>

                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">

                                <p>   {{Session::has('lang')&&Session::get('lang')=='ar'? $question -> answer_ar : $question -> answer_en}} </p>

                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script>
    $("#question-btn").on('click',function(e){

    var form = $('#question-form').get(0);
    var formData = new FormData(form);
    var oldText = $(this).text();
    $(this).prop('disabled', true).css({
    opacity:'0.5'
    }).text("{{trans('site.action.loading')}}");


    $.ajax({
    url:"{{route('storefaqs')}}",
    type:"POST",
    data: formData,
    dataType: "json",
    processData: false,
    contentType: false,
    cache: false,
    success:function(data){
    $('#question-btn').removeAttr("disabled").css({
    opacity:'1'
    }).text(oldText);
    if(data.key == 'success'){
        Swal.fire({
            title: data.msg,
            position:'top',
            timer: 3000,
            type:'success',
            showCloseButton: false,
            showConfirmButton:false,
            animation: true
        })
    }else{
    Swal.fire({
    title: data.msg,
    position:'top',
    timer: 3000,
    type:'error',
    showCloseButton: false,
    showConfirmButton:false,
    animation: true
    })
    }
    }
    });
    });
    </script>
@endsection
