{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')
@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Album':'البوم الصور'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="product-page container  pt-20">
            <div class="page-title text-right d-flex justify-content-between mb-0">

                <h4> {{$album -> name_ar}} </h4>
                <div class="d-flex algin-items-center">
                    <div class=" share-div   ">
                        <a  id="fb_share" href="" class="icon">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a id="twitter_share" href="" class="icon">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a  id="whatsapp_share" href="" class="icon">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </div>
                    <button class="open-share icon">
                         <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </button>
                    @if(auth()->check())
                    <button class="icon font-20 favoritebtn  @if(App\Models\Favourite::where('user_id',Auth::user()->id)->where('product_id',$album->id)->first()) like_active @endif" data-like="{{$album->id}}">
                        <i class="fa fa-heart"> </i>
                    </button>
                    @endif

                </div>


            </div>
            <div class="d-flex align-items-center">
                <div class="rating">
                    @if($rate->rate == 0)
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star"></i>
                    @elseif($rate->rate == 1)
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star"></i>
                    @elseif($rate->rate == 2)
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star"></i>
                    @elseif($rate->rate == 3)
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star "></i>
                        <i class="fa fa-star"></i>
                    @elseif($rate->rate == 4)
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star"></i>
                    @elseif($rate->rate == 5)
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                        <i class="fa fa-star active"></i>
                    @endif
                    <span>{{$rate->rate}}</span>
                </div>
                @if(auth()->check())
                <button class="btn-main btn-small ml-20 mr-20" data-toggle="modal" data-target="#rating_modal">
                    قيم الان
                </button>
                @endif
            </div>
            <div class="price d-flex">
                @if($album->offer == 1)
                    <h4> {{$album -> price_after}} </h4>
                    <h4 class="old-price">
                        {{$album -> price_before}}
                    </h4>
                @endif
                @if($album->offer == 0)

                    <h4> {{$album -> price}} </h4>
                @endif
            </div>
            @if(auth()->check())
            <a data-id="{{$album->id}}" class="btn-main mt-20 mb-20 addTocart">
                {{__('site.action.add_cart')}}
            </a>
            @endif


            <div class="product-cover">

                <img src="{{$album -> photoPath}}">

            </div>

            <div class="row">
                @foreach($album -> photos as $photo)
                <div class="col-md-4">
                    <div class="box">
                        <div class="imgs">
                            <img src="{{$photo -> photoPath}}" alt="">

                        </div>
                        <h6> {{Session::has('lang')&&Session::get('lang')=='ar'? $photo -> name_ar :$photo -> name_en }} </h6>
                    </div>
                </div>
                @endforeach

            </div>
        </div>

    </section>

@endsection
@section('scripts')

    {{--  Favourite --}}
    <script>
        $(document).on('click','.favoritebtn',function(e){
            e.preventDefault();
            if($(this).hasClass('like_active')){
                $(this).removeClass('like_active');
            }else{
                $(this).addClass('like_active');
            }
            var id   = $(this).data('like')
            var _token = $("._token").val();

            $.ajax({
                url:"{{route('like')}}",
                type:"POST",
                data:{
                    id: id,
                    _token:_token
                },
                dataType: "json",
                success:function(data){
                }
            });
        });
    </script>

    {{--  Cart --}}
    <script>
        $(document).on('click','.addTocart',function(e){
            e.preventDefault();

            var id   = $(this).data('id')
            var _token = $("._token").val();


            $.ajax({
                url:"{{route('add-to-cart')}}",
                type:"POST",
                data:{

                    id: id,
                    _token:_token
                },
                dataType:"json",
                success:function(data){

                    if(data.key =='success'){
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'success',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })

                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });
    </script>

    {{-- Share --}}

    <script>
        window.onload = function() {
            var fb_url='http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href)+'&display=popup';
            $('#fb_share').attr('href', fb_url);
            var tw_share='http://twitter.com/intent/tweet?url=' + encodeURIComponent(location.href)+"&hashtags=dan,website";
            $('#twitter_share').attr('href', tw_share);
            var whatsapp_share='whatsapp://send?text=' + encodeURIComponent(location.href);
            $('#whatsapp_share').attr('href', whatsapp_share);
        }
    </script>
@endsection
