{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title')
{{Session::has('lang')&&Session::get('lang')=='en'?'Contact Us':' تواصل معانا'}}
@endsection

@section('content')


    <section class="body-content">
        <div class="contact-page container sec-padd">
            <div class="page-title">
                <h5> {{__('site.site.contact_us')}} </h5>
                <h4> </h4>

            </div>
            <div class="plr-50 ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="about-box">
                            <img src="{{asset('assets/uploads/settings/'.$setting['contact_us_photo1'])}}" alt="">
                            <h5> {{__('site.contact_us.contact')}} </h5>
                            <p>
                               <a href=" {{$setting['site_phone']}}">  {{$setting['site_phone']}}</a>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="about-box">
                            <img src="{{asset('assets/uploads/settings/'.$setting['contact_us_photo3'])}}" alt="">
                            <h5> {{__('site.users.e-mail')}} </h5>
                            <p>
                                {{$setting['site_email']}}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="about-box">
                            <img src="{{asset('assets/uploads/settings/'.$setting['contact_us_photo4'])}}" alt="">
                            <h5> {{__('site.users.address')}} </h5>
                            <p>
                                {{$setting['site_address']}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sec-padd">
                <div id="map" style="width: 100%; height: 10px; ">
                </div>
            </div>

            <form class="contact-form" id="question-form">
                @csrf
                <div class="row">

                    <div class="col-md-12">

                        @if(! auth()->check())

                        <div class="form-group">
                                <label for="">      {{__('site.users.name')}}    </label>
                                <input type="text" name="name" class="form-control" value="">

                            </div>

                        @endif
                        <div class="form-group">
                            <label for="">   {{trans('site.contact_us.subject_name')}}       </label>
                            <input type="text" name="subject" class="form-control" placeholder="">

                        </div>

                        <div class="form-group">
                            <label for=""> {{trans('site.contact_us.subject')}}      </label>
                            <textarea type="text" name="text" class="form-control" placeholder="{{trans('site.main.typing')}}"></textarea>

                        </div>


                        <button class="btn-main" id="question-btn">
                            {{trans('site.main.send')}}
                        </button>


                    </div>
                </div>
            </form>

        </div>



    </section>


@endsection
@section('scripts')
    <script>
        $("#question-btn").on('click',function(e){

            var form = $('#question-form').get(0);
            var formData = new FormData(form);
            var oldText = $(this).text();
            $(this).prop('disabled', true).css({
                opacity:'0.5'
            }).text("{{trans('site.action.loading')}}");


            $.ajax({
                url:"{{route('storefaqs')}}",
                type:"POST",
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    $('#question-btn').removeAttr("disabled").css({
                        opacity:'1'
                    }).text(oldText);
                    if(data.key == 'success') {
                        Swal.fire({
                            title: data.msg,
                            position: 'top',
                            timer: 3000,
                            type: 'success',
                            showCloseButton: false,
                            showConfirmButton: false,
                            animation: true
                        })

                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });
    </script>
@endsection
