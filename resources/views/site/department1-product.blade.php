{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')
@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Product':'الزفات'}}
@endsection

@section('content')

    <section class="body-content">
        <div class="product-page container  pt-20">
            <div class="page-title text-right d-flex justify-content-between mb-0">

                <h4> {{$product -> name_ar}} </h4>
                <div class="d-flex">
                    <div class=" share-div    pt-10">
                        <a  id="fb_share" href="" class="icon">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a id="twitter_share" href="" class="icon">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a  id="whatsapp_share" href="" class="icon">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </div>
                    <button class="open-share icon">
                         <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </button>
                    @if(auth()->check())
                    <button class="icon font-20 favoritebtn  @if(App\Models\Favourite::where('user_id',Auth::user()->id)->where('product_id',$product->id)->first()) like_active @endif" data-like="{{$product->id}}"> <i class="fa fa-heart"> </i> </button>
                    @endif
                </div>


            </div>
            <div class="d-flex align-items-center mb-20">
                <div class="rating">
                    @if($rate->rate == 0)
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star"></i>
                    @elseif($rate->rate == 1)
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star"></i>
                    @elseif($rate->rate == 2)
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star"></i>
                    @elseif($rate->rate == 3)
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star "></i>
                    <i class="fa fa-star"></i>
                    @elseif($rate->rate == 4)
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star"></i>
                    @elseif($rate->rate == 5)
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    <i class="fa fa-star active"></i>
                    @endif
                    <span>{{$rate->rate}}</span>
                </div>
                @if(auth()->check())
                <button data-toggle="modal" data-target="#rating_modal" class="btn-main btn-small ml-20 mr-20">
                    قيم الان
                </button>
                @endif
            </div>
            <div class="product-cover">
                <img src="{{$product->photoPath}}">
            </div>
            <p class="gray-color ">
                {{Session::has('lang')&&Session::get('lang')=='ar'? $product -> category -> name_ar : $product -> category -> name_en}} - {{Session::has('lang')&&Session::get('lang')=='ar'? $product -> subcategory -> name_ar : $product -> subcategory -> name_en}}
            </p>

            <div class="price d-flex">

                <h4> {{$product -> price_after}} </h4>
                @if($product->offer == 1)
                <h4 class="old-price">
                    {{$product -> price_before}}
                </h4>
                @endif

            </div>
            <div class="title">
                <h6> وصف المنتج </h6>
                <p>
                    {{Session::has('lang')&&Session::get('lang')=='ar'?$product -> description_ar : $product -> description_en}}
                </p>
            </div>
                @if( $product -> audio->count() > 0)

                <h6> {{__('site.site.more_option')}}  </h6>

            <ul class="product-additions">

                @foreach($product->audio as $audio)
                <li>
                    <label>
                            <input type="radio" name="product">
                            <span> {{Session::has('lang')&&Session::get('lang')=='ar'?$audio ->name_ar : $audio ->name_en}} </span>
                        </label>
                    <audio controls>
                             <source src="{{$audio -> audioPath}}" type="audio/mpeg">
                           </audio>
                </li>
                @endforeach
            </ul>
                @endif



               <a class="btn-main mt-20 mb-20 addTocart">{{__('site.action.add_cart')}}</a>


        </div>

        <div class="modal fade" id="rating_modal" tabindex="-1" role="dialog" aria-labelledby="password_modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <form class="">
                        @csrf
                        <div class="modal-body text-center">


                            <h5 class="color-black text-center f-b mt-20">
                                تقييم الخدمة
                            </h5>

                            <div class="star-rating">
                                <span class="fa fa-star-o" data-rating="1"></span>
                                <span class="fa fa-star-o" data-rating="2"></span>
                                <span class="fa fa-star-o" data-rating="3"></span>
                                <span class="fa fa-star-o" data-rating="4"></span>
                                <span class="fa fa-star-o" data-rating="5"></span>
                                <input class="rating-value " type="hidden" name="rate" value="" />
                            </div>



                        </div>
                        <div class="modal-footer">
                            <button class="btn-main btn-small ml-20 mr-20 rate" data-product="product" data-id="{{$product->id}}" data-dismiss="modal">
                                ارسال                        </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    {{--  Favourite --}}

    <script>
        $(document).on('click','.favoritebtn',function(e){
            e.preventDefault();
            if($(this).hasClass('like_active')){
                $(this).removeClass('like_active');
            }else{
                $(this).addClass('like_active');
            }
            var id   = $(this).data('like')
            var _token = $("._token").val();

            $.ajax({
                url:"{{route('like')}}",
                type:"POST",
                data:{
                    id: id,
                    _token:_token
                },
                dataType: "json",
                success:function(data){
                }
            });
        });
    </script>

    {{--  Cart --}}
    <script>
        $(document).on('click','.addTocart',function(e){
            e.preventDefault();

            var id   = $(this).data('id')
            var _token = $("._token").val();


            $.ajax({
                url:"{{route('add-to-cart')}}",
                type:"POST",
                data:{

                    id: id,
                    _token:_token
                },
                dataType:"json",
                success:function(data){

                    if(data.key =='success'){
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'success',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })

                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });
    </script>

    {{-- Share --}}

    <script>
        window.onload = function() {
            var fb_url='http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href)+'&display=popup';
            $('#fb_share').attr('href', fb_url);
            var tw_share='http://twitter.com/intent/tweet?url=' + encodeURIComponent(location.href)+"&hashtags=dan,website";
            $('#twitter_share').attr('href', tw_share);
            var whatsapp_share='whatsapp://send?text=' + encodeURIComponent(location.href);
            $('#whatsapp_share').attr('href', whatsapp_share);
        }
    </script>

    {{-- Rate--}}
    <script>
        $(document).on('click','.rate',function(e){
            e.preventDefault();

            var rate   = $('.rating-value ').val()
            var type = $(this).data('product')
            var id = $(this).data('id')
            var _token = $("._token").val();


            $.ajax({
                url:"{{route('rate')}}",
                type:"POST",
                data:{

                     rate,
                    id,
                    type,
                    _token:_token
                },
                dataType:"json",
                success:function(data){

                    if(data.key =='success'){
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'success',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })

                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });
    </script>

@endsection
