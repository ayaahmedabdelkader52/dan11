{{ App\Models\Language::find(1)->{app('lang')} }}
@extends('site.layout.master')



@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Rules':' الشروط و الاحكام'}}
@endsection

@section('content')

    <section class="body-content">

        <div class="rules-page container sec-padd">
            <div class="page-title text-right m-0 mb-20">
                {!! Session::has('lang')&&Session::get('lang')=='ar'? $setting['title_rules_ar'] : $setting['title_rules_en'] !!}
            </div>
            <ul class="rules">
                @foreach($rules as $rule)
                <li>
                    <h5>
                        <i> 1 </i>
                        {{Session::has('lang')&&Session::get('lang')=='ar'?$rule -> title_ar : $rule -> title_en}}
                    </h5>
                    <p>
                        {{Session::has('lang')&&Session::get('lang')=='ar'? $rule -> description_ar : $rule -> description_en}}
                    </p>
                </li>
                @endforeach
            </ul>
            <div class="pt-20">
                <label>
                    <input type="checkbox" checked>
                    <span> ا{{Session::has('lang')&&Session::get('lang')=='ar'?'لموافقه ع الشروط والاحكام' : 'Agree to all rules'}}</span>
                </label>
            </div>
        </div>


    </section>

@endsection
