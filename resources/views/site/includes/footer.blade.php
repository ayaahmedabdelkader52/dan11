<!-- start footer-->

<footer>
    <div class="container">
        <div class="row  ">
            <div class="col-lg-3 col-md-4 ">
                <a class="footer-logo" href="{{route('index')}}">
                    <img src="{{asset('assets/uploads/settings/' . $setting['logo'])}}">
                </a>
                <p>
                    {{$setting['policies_ar']}}
                </p>
                <div class="soc-media">
                    <a href="#" target="_blank">
                        <img src="{{asset('Site/assets/imgs/snapchat.png')}}">
                    </a>
                    <a href="#" target="_blank">
                        <img src="{{asset('Site/assets/imgs/facebook (1).png')}}">
                    </a>
                    <a href="#" target="_blank">
                        <img src="{{asset('Site/assets/imgs/twitter (1).png')}}">
                    </a>
                    <a href="#" target="_blank">
                        <img src="{{asset('Site/assets/imgs/instagram (2).png')}}">
                    </a>
                    <a href="#" target="_blank">
                        <img src="{{asset('Site/assets/imgs/youtube.png')}}">
                    </a>
                </div>


            </div>
            <div class="col-lg-2">
                <ul>
                    @if(! auth()->check())
                    <li> <a href="{{route('login')}}"> {{__('site.auth.login')}}</a></li>
                    @endif
                    @if( auth()->check())
                    <li> <a href="{{route('cart')}}">  عربة التسوق</a></li>
                    <li> <a href="{{route('likes')}}">  المفضله  </a></li>
                    <li> <a href="{{route('profile')}}">  اعدادات الحساب </a></li>
                    @endif
                </ul>
            </div>
            <div class="col-lg-2">
                <ul>
                    <li> <a href="{{route('contact')}}"> {{__('site.site.contact_us')}}  </a></li>
                    <li> <a href="{{route('rules')}}"> {{__('site.site.terms_and_conditions')}} </a></li>
                    <li> <a href="{{route('privacy')}}">  {{__('site.site.policies')}}  </a></li>
                    <li> <a href="{{route('questions')}}">   {{__('site.site.complaints')}}     </a></li>

                </ul>
            </div>
            <div class="col-lg-4">
                <ul>
                    <li> أشترك الأن </li>
                    <li>
                        <p class="f-n"> أحصل الأن علي العروض الجديده
                        </p>
                    </li>
                    <li>
                        <form>
                            <div class="subscription">
                                <input type="email" placeholder="{{__('site.users.e-mail')}}">
                                <button type="submit" class="btn-footer"> {{__('site.contact_us.send')}} </button>
                            </div>
                        </form>
                    </li>
                    <li>
                        <p class="f-n">
                            {{$setting['copyright_ar']}}
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <a href="#" class="copy-link">
                    {{$setting['copyright_ar']}}
                </a>
            </div>

        </div>
    </div>

</footer>
<!-- end footer -->
