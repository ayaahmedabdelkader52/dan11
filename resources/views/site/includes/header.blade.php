<!-- Start header  -->

<header>
    <!-- Start Navbar-->
    <div class="menu">
        <div class="container">
            <div class="menu-content">
                <div class="nav-links">
                    <a class="logo" href="{{route('index')}}">
                        <img src="{{ asset('assets/uploads/settings/' . $setting['logo'])  }}" class="logo1" />
                        <img src="{{ asset('assets/uploads/settings/' . $setting['logo'])  }}" class="logo2" />

                    </a>

                    <ul>
                        <li><a href="{{route('index')}}">{{__('site.Home')}}</a></li>
                        <li><a href="{{route('about')}}"> {{__('site.site.who_are_we')}} </a></li>
                        <li><a href="{{route('questions')}}">   {{__('site.site.complaints')}}    </a></li>
                        <li><a href="{{route('contact')}}">{{__('site.site.contact_us')}}</a></li>
                        <li><a href="{{route('joinus')}}"> {{__('site.site.join_us')}}  </a></li>
                        <li>
                            <a href="" class="btn-search default"> <img src="{{asset('Site/assets/imgs/search.png')}}" width="18px"> </a>
                            <form class="header-search">
                                <input type="text">
                                <button type="button" class="close-search"> x </button>
                            </form>
                        </li>

                    </ul>
                </div>
                <div class="overlay"></div>
                <div class="top-links">





                    <!-- start after login div  -->
                    <a href="{{route('profile')}}" class="top-link">
                        <img src="{{asset('Site/assets/imgs/icon-user1.png')}}" alt="">
                        <img src="{{asset('Site/assets/imgs/icon-user2.png')}}" alt="">
                    </a>
                    <a href="{{route('cart')}}" class="top-link ">
{{--                        <span>{{Session::has('cart') ? Session::get('cart')->totalQty : '0' }}</span>--}}
                        <img src="{{asset('Site/assets/imgs/icon-cart1.png')}}" alt="">
                        <img src="{{asset('Site/assets/imgs/icon-cart2.png')}}" alt="">
                    </a>


                    <a href="{{route('likes')}}" class="top-link ">
                        <img src="{{asset('Site/assets/imgs/icon-heart1.png')}}" alt="">
                        <img src="{{asset('Site/assets/imgs/icon-heart2.png')}}" alt="">
                    </a>
                    <div class="dropdown">
                        <button class="top-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{asset('Site/assets/imgs/icon-world1.png')}}" alt="">

                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{'lang/ar'}}"> العربيه</a>
                            <a class="dropdown-item" href="{{'lang/en'}}"> English</a>

                        </div>
                    </div>

                    <!-- <a href="login.blade.php" class="top-button ">
                        تسجيل دخول
                     </a> -->




                    <!-- end  after login div -->






                    <div class="mob-collaps"><span></span><span></span><span></span></div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Navbar-->
</header>


<!-- end header -->
