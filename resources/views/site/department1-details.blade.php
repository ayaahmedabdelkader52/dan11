{{ App\Models\Language::find(1)->{app('lang')} }}

@extends('site.layout.master')

@section('title')
    {{Session::has('lang')&&Session::get('lang')=='en'?'Products':'الخدمات'}}
@endsection

@section('content')

    <section class="body-content">

        <div class="top-cover">
            <div class="top owl-carousel owl-theme">
        @foreach($slides as $slide)
                <div class="item">
                    <img src="{{ $slide->imagePath }}" width="100%">
                    <div class="item-abs">
                        <h5>

                            {{Session::has('lang')&&Session::get('lang')=='ar'? $slide -> title_ar : $slide -> title_en}} <span class="main-color"> {{trans('site.main.service')}}   </span>

                        </h5>
                    </div>
                </div>
        @endforeach
            </div>

        </div>

        <div class="department-details sec-padd">
            <div class="container">
                <div id="filtertion">
                <form action="#" id="filterForm">
                    @csrf

                    <div class="row search-product">

                        <input required="" type="hidden" name="category_id" value="{{$category->id}}" >

                        <div class="col-md-4" >

                            <select name="singer_filter"  class="form-control select-2">
                                <option value="">{{trans('site.main.selectsinger')}}</option>
                                @foreach($singers as $singer)


                                    <option value="{{ $singer->id }}">

                                        {{Session::has('lang')&&Session::get('lang')=='ar'? $singer->name_ar : $singer->name_en}}



                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4">

                            <select class="form-control select-2 " name="char_filter">

                                <option value="">{{trans('site.main.selectevents')}}</option>

                                @foreach($subjects as $subject)


                                        <option value="{{$subject->id}}"> {{ Session::has('lang')&&Session::get('lang')=='ar'? $subject -> name_ar : $subject -> name_en }} </option>

                                @endforeach

                            </select>
                        </div>


                        <div class="col-md-4">

                            <select class="form-control select-2" name="subject_filter">
                                <option  value="">{{trans('site.main.selectnames')}}</option>

                            @foreach($characters as $character)


                                  <option value="{{$character->id}}"> {{ Session::has('lang')&&Session::get('lang')=='ar'?$character -> name_ar : $character -> name_en }} </option>


                            @endforeach


                            </select>

                        </div>
                    </div>
                </form>
                </div>

            <div id="filtration_area">
                <div class="row product-boxs pt-20">
                @foreach($products as $product)
                    @foreach($rates as $rate)
                    <div class="col-md-4">
                        <div class="product-box">
                            <a href="{{route('product' , $product)}}" class="product-img">
                                <img src="{{$product -> photoPath}}" alt="">
                                <div class="product-abs">
                                    <h5> {{Session::has('lang')&&Session::get('lang')=='ar'?$product -> name_ar : $product -> name_en}} </h5>
                                </div>
                            </a>

                            <div class="product-info">
                                <h6>{{ $product->price_after }} </h6>

                                <div class="d-flex">
                                    @if(auth()->check())
                                    <button class="icon favoritebtn  @if(App\Models\Favourite::where('user_id',Auth::user()->id)->where('product_id',$product->id)->first()) like_active @endif"  data-like="{{$product->id}}" id="like"> <i class="fa fa-heart"> </i> </button>
                                    <button data-id="{{$product->id}}"  class="addTocart" type="submit"> <i class="fa fa-shopping-basket"> </i> </button>
                                    @endif
                                    <a class="icon" href="{{route('product' , $product)}}"> <i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

                                </div>
                            </div>


                            @if($rate->rate == 0)
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star"></i>
                            @elseif($rate->rate == 1)
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star"></i>
                            @elseif($rate->rate == 2)
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star"></i>
                            @elseif($rate->rate == 3)
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star "></i>
                                <i class="fa fa-star"></i>
                            @elseif($rate->rate == 4)
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                            @elseif($rate->rate == 5)
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                            @endif
                            <span>{{$rate->rate}}</span>

                        </div>
                    </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
            </div>
            </div>

    </section>
@endsection
@section('scripts')

{{--   Filter --}}
<script>
    $("#filtertion").change(function() {
        var singer = $('#singer_id').val();
        var form = $('#filterForm').get(0);
        var formData = new FormData(form);

        console.log(formData);
        console.log(singer);

        $.ajax({
            url:"{{route('filter')}}",
            type:"Post",
            data: formData,
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            success:function(data){

                $('#filtration_area').html(data.html);
                // console.log(data)
            }
        });
    });
</script>


{{--  Favourite --}}

    <script>
        $(document).on('click','.favoritebtn',function(e){
            e.preventDefault();
            if($(this).hasClass('like_active')){
                $(this).removeClass('like_active');
            }else{
                $(this).addClass('like_active');
            }
            var id   = $(this).data('like')
            var _token = $("._token").val();

            $.ajax({
                url:"{{route('like')}}",
                type:"POST",
                data:{
                    id: id,
                    _token:_token
                },
                dataType: "json",
                success:function(data){
                }
            });
        });
    </script>

{{--  Cart --}}
    <script>
        $(document).on('click','.addTocart',function(e){
            e.preventDefault();

            var id   = $(this).data('id')
            var _token = $("._token").val();


            $.ajax({
                url:"{{route('add-to-cart')}}",
                type:"POST",
                data:{

                    id: id,
                    _token:_token
                },
                dataType:"json",
                success:function(data){

                    if(data.key =='success'){
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'success',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })

                    }else{
                        Swal.fire({
                            title: data.msg,
                            position:'top',
                            timer: 3000,
                            type:'error',
                            showCloseButton: false,
                            showConfirmButton:false,
                            animation: true
                        })
                    }
                }
            });
        });
    </script>

@endsection
