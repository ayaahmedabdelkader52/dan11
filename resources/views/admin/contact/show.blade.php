@extends('admin.master')
@section('title', 'عرض رسالة')

@section('content')

    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{ __('dashboard.main.Inbox') }}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.inbox.index') }}">{{ __('dashboard.main.Inbox') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('dashboard.action.show') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <div class="app-content-overlay"></div>
        <div class="email-app-area">
            <!-- Detailed Email View -->
            <div class="">
                <div class="row">
                    <div class="col-12">
                        <div class="card px-1">
                            <div class="card-header email-detail-head ml-75">
                                <div class="user-details d-flex justify-content-between align-items-center flex-wrap">
                                    <div class="avatar mr-75">
                                        <img src="{{ ($message->user ) ? asset('assets/uploads/users/' . $message->user->avatar) : asset('assets/uploads/users/default.png') }}" alt="avtar" width="61" height="61">
                                    </div>
                                    <div class="mail-items">
                                        <h4 class="list-group-item-heading mb-0">{{ $message->name }}</h4>

                                        <span class="font-small-3">{{ $message->email }}</span>
                                    </div>
                                </div>
                                <div class="mail-meta-item">
                                    <div class="mail-time mb-1">{{ $message->created_at->format('H:i') }}</div>
                                    <div class="mail-date">{{ $message->created_at->toDateString() }}</div>
                                </div>
                            </div>
                            <div class="card-body mail-message-wrapper pt-2 mb-0">
                                <div class="mail-message">
                                    <p>{{ $message->message }}</p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="" data-toggle="modal" data-target="#messageModal" class="send-message btn btn-primary"
                                   data-user-phone="{{ $message->phone }}" data-user-email="{{ $message->email }}" data-user-id="{{ $message->user_id }}"
                                   title="{{ __('dashboard.action.reply_message') }}" >رد</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Detailed Email View -->
        </div>

    </div>
    @include('admin.contact.includes.message-to-single-user')

@endsection
@section('scripts')
    <script>
        $('.send-message').on('click', function() {
            var userEmail = $(this).data('user-email'),
                userId = $(this).data('user-id'),
                userPhone = $(this).data('user-phone');

            // console.log('hello');
            console.log(userEmail);
            console.log(userPhone);

            $('.user-phone').val(userPhone);
            $('.user-email').val(userEmail);
            $('.user-id').val(userId);
        });

    </script>
@stop
