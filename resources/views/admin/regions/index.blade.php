@extends('admin.master')
<!--
=> regions => your title
=> regions => your title
=> regions => your title
=> $regions => your plural variable
=> $region => your single variable
=> regions => your route name
=> regions => your plural variable
=> region => your single variable
=> regions => your plural variable
=> region => your single variable

-->
@section('title', 'المناطق')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الدول</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الدول</a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.countries.show', $country) }}">{{ $country->name }}</a>
                            </li>
                            <li class="breadcrumb-item active">المناطق</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('admin.countries.regions_create', $country) }}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        <a class="dropdown-item action-download" href="{{ route('admin.regions.downloadExcel') }}" ><i class="feather icon-file"></i>{{ __('dashboard.action.download_excel') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    @foreach(config('app.languages') as $key => $lang)
                        <th>{{ __('dashboard.main.name_in_' . $key) }}</th>
                    @endforeach

                    <th>الكود</th>
                    <th>التعداد السكاني</th>
                    <th>{{ __('dashboard.main.Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($regions as $region)
                    <tr id="region-{{$region->id}}">
                        <td data-id="{{ $region->id }}"></td>
                        @foreach(config('app.languages') as $key => $lang)
                            <td>{{ $region->getTranslation('name', $key) }}</td>
                        @endforeach

                        <td>
                            <div class="chip @if($region->code) chip-success @else chip-danger @endif">
                                <div class="chip-body">
                                    <div class="chip-text">{{ ($region->code ) ? $region->code : 'غير معروف' }}</div>
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="chip @if($region->population) chip-success @else chip-danger @endif">
                                <div class="chip-body">
                                    <div class="chip-text">{{ ($region->population ) ? $region->population : 'غير معروف' }}</div>
                                </div>
                            </div>
                        </td>

                        <td class="product-action">
                            <a href="{{ route('admin.regions.edit', $region) }}">
                                    <span data-id="{{ $region->id }}" >
                                        <i class="feather icon-edit"></i>
                                    </span>
                            </a>
                            <span class="action-delete" data-id="{{ $region->id }}" title="{{ __('dashboard.action.delete') }}">
                                    <i class="feather icon-trash"></i>
                                </span>

                            <a href="{{ route('admin.regions.show', $region) }}">
                                <span title="{{ __('dashboard.action.show') }}">
                                    <i class="feather icon-eye"></i>
                                </span>
                            </a>

                            <a href="{{ route('admin.regions.cities', $region) }}">
                                <span title="المدن">
                                    <i class="fa fa-map-pin"></i>
                                </span>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->

    </section>
    <!-- Data list view end -->

@endsection

@section('scripts')
    <script>

        var regions = [];

        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {
            var region = $(this).data('id'),
                url = '{{ route("admin.regions.destroy", ":id") }}',
                newUrl = url.replace(':id', region);

            console.log( region);
            console.log( newUrl);

            Swal.fire({
                title: '{{ __('dashboard.alerts.do_you_want_to_delete_this_row') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#region-' + region).remove();
                        }
                    });
                }
            })
        });

        $('.delete-all').click(function () {
            var regions = [];
            $('.dt-checkboxes:checked').each(function() {
                regions.push( $(this).parent().data('id') );
                // console.log($(this).parent().data('region-id') )
            });
            console.log(regions);
            // console.log(regions.length);
            if(regions.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.alerts.do_you_want_to_delete_selected_data') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.regions.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                regions
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                regions.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#region-' + element).remove();
                                } );

                                // console.log(regions)
                            }
                        });
                    }
                })

            }

        });

    </script>
@endsection
