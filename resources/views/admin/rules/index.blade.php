@extends('admin.master')
@section('title', 'الاحكام والشروط')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">

        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاحكام والشروط</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">كل الاحكام والشروط</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <section id="data-list-view" class="data-list-view-header">

            <div class="action-btns d-none">
                <div class="btn-dropdown mr-1 mb-1">
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('dashboard.main.Actions') }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('admin.rules.create')}}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                            <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Data list view Ends -->
            <!-- DataTable starts -->
            <div class="table-responsive">

                <table class="table data-list-view">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('dashboard.main.name_in_ar') }}</th>
                        <th>{{ __('dashboard.main.name_in_en') }}</th>
                        <th>{{__('dashboard.main.description_in_ar')}}</th>
                        <th>{{__('dashboard.main.description_in_en')}}</th>
                        <th>{{ __('dashboard.main.Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rules as $rule)
                        <tr id="rule-{{$rule->id}}">
                            <td data-rule-id = '{{$rule -> id}}' ></td>
                            <td>{{$rule -> title_ar  }}</td>
                            <td>{{$rule -> title_en  }}</td>
                            <td>{{$rule -> description_ar  }}</td>
                            <td>{{$rule -> description_en  }}</td>


                            <td>

                                <a href="#">
                                 <span class="action-delete" data-report-id="{{ $rule->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>
                                </a>

                                <a href="{{ route('admin.rules.edit', $rule) }}">
                                <span data-rule-id="{{$rule -> id}}"
                                      data-name_ar="{{ $rule->title_ar }}" data-name_en="{{ $rule->title_en }}"
                                       title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>

        var rule = [];

        $('.action-delete').on('click', function () {

            var rule = $(this).data('report-id'),
                url = '{{ route("admin.rules.destroy", ":id") }}',
                newUrl = url.replace(':id', rule);

            console.log( rule);
            console.log( newUrl);

            Swal.fire({
                title: 'هل تريد حذف القسم ؟',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم احذفه ',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: 'الغاء',
                buttonsStyling: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'GET',
                         {{--_token: "{{ csrf_token() }}",--}}
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#rule-' + rule).remove();
                        }
                    });
                }
            })
        });



        $('.delete-all').click(function () {

            var rules = [];

            $('.dt-checkboxes:checked').each(function() {
                rules.push( $(this).parent().data('rule-id') );
            });
             // console.log(rules.length);
            if(rules.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاقسام المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.rules.destroy_selected') }}",
                            method: 'POST',
                            {{-- headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
                            {{--_token: "{{ csrf_token() }}",--}}
                            data: {
                                ids : rules
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                rules.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#rule-' + element).remove();
                                });
                            }
                        });
                    }
                })

            }

        });
    </script>
@endsection
