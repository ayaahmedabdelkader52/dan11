@extends('admin.master')
@section('title', 'الطلبات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الزفات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.orders.index') }}">الزفات</a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.orders.show' , $order -> id ) }}">{{ $order->name }}</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="content-body">
            <!-- page users view start -->
            <section class="page-users-view">
                <div class="row">
                    <!-- account start -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">عرض تفاصيل الطلب </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                        <label for="ar-name">{{ __('dashboard.inbox.Client_Name') }}</label>
                                            <input type="text" id="ar-name" class="form-control" name="name_ar"
                                                   placeholder="{{ __('dashboard.inbox.Client_Name') }}" disabled value="{{$order -> name_ar}}">
                                        </div>

                                        <div class="col-md-6 col-12">
                                        <label for="address">{{ __('dashboard.user.address') }}</label>
                                        <input type="text" id="address" class="form-control" name="address"
                                               placeholder="{{ __('dashboard.user.address') }}" disabled value="{{ $order -> address }}">
                                        </div>

                                        <div class="col-md-6 col-12">
                                            <label for="date">تاريخ المناسبه</label>
                                            <input type="text" id="date" class="form-control" name="order_date"
                                                   placeholder="تاريخ المناسبه" disabled value="{{$order -> order_date }}" >
                                        </div>

                                        <div class="col-md-6 col-12">
                                            <label for="payment">طريقه الدفع </label>
                                            <input type="text" id="payment" class="form-control" name="type_payment"
                                                   placeholder="طريقه الدفع " disabled value="{{$order -> type_payment }}" >
                                        </div>

                                        <div class="col-md-6 col-12">
                                            <label for="price">رقم الجوال</label>
                                            <input type="text" id="price" class="form-control" name="price"
                                                   placeholder="رقم الجوال" disabled value="{{$order -> phone }}" >
                                        </div>

                                        <div class="col-md-6 col-12">
                                            <label for="extra_phone">رقم جوال اضافي</label>
                                            <input type="text" id="extra_phone" class="form-control" name="extra_phone"
                                                   placeholder="{{ __('dashboard.main.end') }}" disabled value="{{$order -> extra_phone }}" >
                                        </div>

                                        <div class="col-md-6 col-12">
                                        <label for="status"> حالة الطلب </label>
                                        <input type="text" id="status" class="form-control" name="status"
                                               placeholder="حالة الطلب" disabled value="
                                            @if($order -> status == 'wait')في الانتظار@endif
                                             @if($order -> status == 'paid')مدفوع@else
                                            منتهي@endif">
                                        </div>
                                    @foreach($products as $product)
                                        <div class="col-md-6 col-12">
                                        <label for="price">{{ __('dashboard.main.price') }}</label>
                                        <input type="text" id="price" class="form-control" name="price"
                                               placeholder="{{ __('dashboard.main.price') }}" disabled value="{{$product -> price }}" >
                                        </div>
                                            <div class="col-md-6 col-12">

                                        <a class="btn"  href="{{route('admin.products.show',$product->product_id)}}">
                                            <span title="عرض الزفه">
                                        <i class="feather icon-eye"></i>
                                        </span>
                                        </a>
                                            </div>

{{--                                          <div class="col-md-6 col-12">--}}
{{--                                        <label for="order_hours">مدة الزفه</label>--}}
{{--                                        <input type="text" id="order_hours" class="form-control" name="order_hours"--}}
{{--                                               placeholder="مدة الزفة" disabled value="{{$product -> order_hours }}" >--}}
{{--                                        </div>--}}

                                        <div class="col-md-6 col-12">
                                        <label for="price"> الكمية </label>
                                        <input type="text" id="price" class="form-control" name="price"
                                               placeholder="الكمية" disabled value="{{$product -> qty }}" >
                                        </div>

                                        @endforeach


                                    </div>

                                </div>
                            </div>



    </div>
@endsection
