@extends('admin.master')
@section('title', 'الطلبات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الطلبات </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">كل الطلبات </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <section id="data-list-view" class="data-list-view-header">
            <div class="action-btns d-none">
                <div class="btn-dropdown mr-1 mb-1">
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('dashboard.main.Actions') }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data list view Ends -->
            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table data-list-view">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('dashboard.inbox.Client_Name') }}</th>
                        <th>{{ __('dashboard.user.address') }}</th>
                        <th>حاله الطلب</th>
                        <th> طريقه الدفع  </th>
                        <th>{{ __('dashboard.main.Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)

                        <tr id="order-{{$order->id}}">
                            <td data-order-id ="{{$order->id}}"></td>
                            <td>{{$order -> name  }}</td>
                            <td>{{$order -> address  }}</td>
                            <td> @if($order -> status == 'wait')
                                   في الانتظار
                                @elseif($order -> status == 'paid')
                                    مدفوع
                                @else
                                    منتهي
                                @endif</td>
                            <td> @if($order -> type_payment == 'cash')
                            كاش
                                @else
                                اونلاين
                                @endif
                            </td>
                            <td>
                                <a href="#">
                                 <span class="action-delete" data-order="{{ $order->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>
                                </a>
                                <a href="{{ route('admin.orders.show', $order) }}">
                                <span title="{{ __('dashboard.action.show') }}">
                                    <i class="feather icon-eye"></i>
                                </span>
                                </a>
                                <a href="{{ route('admin.orders.edit', $order) }}">
                                <span data-order-id="{{$order -> id}}" title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                                </a>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
@section('scripts')

    <script>

        var order = [];

        $('.action-delete').on('click', function () {

            var order = $(this).data('order'),

                url = '{{ route("admin.orders.destroy", ":id") }}',
                newUrl = url.replace(':id',order);

            console.log(order);
            console.log(newUrl);

            Swal.fire({
                title: 'هل تريد حذف الخدمه ؟',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم احذفه ',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: 'الغاء',
                buttonsStyling: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'GET',
                           _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#order-' + order).remove();
                        }
                    });
                }
            })
        });
    </script>

    <script>
        $('.delete-all').click(function () {
            var selectedorders = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedorders.push( $(this).parent().data('order-id') );
            });
            // console.log(selectedorders.length);
            if(selectedorders.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاعلانات المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.orders.destroySelected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                ids: selectedorders
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedorders.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#order-' + element).remove();
                                } );

                            }
                        });
                    }
                })

            }

        });
    </script>
@endsection
