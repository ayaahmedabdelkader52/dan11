@extends('admin.master')
@section('title','اضافه صور ')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">الصور </h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.albums.index') }}">الصور</a></li>
                        <li class="breadcrumb-item active">اضافة صوره</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{route('admin.albumimages.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="ar-name" class="form-control" name="name_ar"
                                           placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus value="{{ old('name_ar') }}">
                                    <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                                </div>
                            </div>

                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="en-name" class="form-control" name="name_en"
                                           placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus value="{{ old('name_en') }}">
                                    <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                                </div>
                            </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="category"> الصوره  </label>
                                <input type="file"  class="form-control" name="photo"
                                 placeholder="{{ __('dashboard.main.photo') }}" autofocus >

                            </div>
                        </div>


                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="category"> أختر الالبوم </label>
                                        <select name="album_id" id="category" class="select2 form-control">
                                     <optgroup label="من فضلك أختر القسم ">
                                        @if($albums && $albums -> count() > 0)
                                            @foreach($albums as $album)
                                                <option  value="{{$album -> id }}">{{$album -> name_ar}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                </select>
                                @include('admin.includes.alerts.input-errors', ['input' => "category_id"])
                            </div>
                        </div>



                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                            </div>

                 </div>
              </div>
        </form>
    </div>
    </div>
@endsection
