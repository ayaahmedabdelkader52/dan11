@extends('admin.master')
@section('title', ' الصور')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0"> الصور</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">كل الصور </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <section id="data-list-view" class="data-list-view-header">
            <div class="action-btns d-none">
                <div class="btn-dropdown mr-1 mb-1">
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('dashboard.main.Actions') }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('admin.albumimages.create')}}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                            <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data list view Ends -->
            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table data-list-view">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('dashboard.main.name_in_ar') }}</th>
                        <th>{{ __('dashboard.main.name_in_en') }}</th>
                        <th>{{ __('dashboard.main.photo') }}</th>
                        <th>{{ __('dashboard.main.album_name') }}</th>
                        <th>{{ __('dashboard.main.Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($photos as $photo)

                        <tr id="photo-{{$photo -> id}}">
                            <td data-photo-id ="{{$photo->id}}"></td>
                            <td>{{$photo -> name_ar  }}</td>
                            <td>{{$photo -> name_en  }}</td>
                            <td>
                                <img style="width: 150px;" src="{{$photo -> photoPath  }}">
                            </td>
                            <td>{{$photo -> album -> name_ar  }}</td>
                            <td>

                                <a href="#">
                                 <span class="action-delete" data-photo="{{ $photo->id }}" title="حذف">
                                     <i class="feather icon-trash"></i>
                                 </span>
                                </a>

                                <a href="{{ route('admin.albumimages.edit', $photo) }}">
                                <span data-photo-id="{{$photo -> id}}" data-name_ar="{{ $photo->name_ar }}"
                                      data-name_en="{{ $photo->name_en }}" data-album_id="{{$photo -> album_id}}"
                                      title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                                </a>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
@section('scripts')

    <script>

        var photo = [];

        $('.action-delete').on('click', function () {

            var photo = $(this).data('photo'),

                url = '{{ route("admin.albumimages.destroy", ":id") }}',
                newUrl = url.replace(':id', photo);

            console.log(photo);
            console.log(newUrl);

            Swal.fire({
                title: 'هل تريد حذف الخدمه ؟',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم احذفه ',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: 'الغاء',
                buttonsStyling: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                     //   _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#photo-' + photo).remove();
                        }
                    });
                }
            })
        });
    </script>

    <script>
        $('.delete-all').click(function () {
            var selectedphotos = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedphotos.push( $(this).parent().data('photo-id') );
            });
            console.log(selectedphotos.length);
            if(selectedphotos.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاعلانات المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.albumimages.destroySelected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                ids: selectedphotos
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedphotos.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#photo-' + element).remove();
                                } );

                            }
                        });
                    }
                })

            }

        });
    </script>
@endsection
