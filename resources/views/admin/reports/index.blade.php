@extends('admin.master')
@section('title')
    التقارير
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">التقارير</h2>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item action-delete-selected" href="#"><i class="feather icon-trash"></i>حذف المحدد </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>ارقام التقارير</th>
                    <th>تاريخ التقرير</th>
                    <th>منشء التقرير</th>
                    <th>التقرير</th>
                    <th>حذف التقرير</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $report)
                    <tr id="report-{{$report->id}}">
                        <td data-report-id="{{ $report->id }}"></td>
                        <td> {{ $i++ }} </td>
                        <td> {{ $report->created_at }} </td>
                        <td> {{($report->user) ?  ($report->user->role ?$report->user->name:'' ) : '' }} </td>
                        <td>{{ $report->msg }}</td>

                        <td>
                            <span class="action-delete" data-report-id="{{ $report->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->

@endsection

@section('scripts')
<script>
    var selectedReports = [];



    // On Delete
    $('.action-delete').on('click', function () {
        var reportId = $(this).data('report-id'),
            url = '{{ route("admin.reports.destroy", ':id') }}',
            newUrl = url.replace(':id', reportId);

        console.log(reportId);
        console.log(newUrl);

        Swal.fire({
            title: 'هل تريد حذف التقربر',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger ml-1',
            cancelButtonText: '{{ __('dashboard.action.cancel') }}',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: newUrl,
                    method: 'DELETE',
                    // _token: "{{ csrf_token() }}",
                    success: function(response) {
                        Swal.fire({
                            position: 'top-start',
                            type: 'success',
                            title: '{{ __('dashboard.alerts.deleted')  }}',
                            showConfirmButton: false,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: false,
                        });
                        $('#report-' + reportId).remove();
                    }
                });
            }
        })
    });

    $('.action-delete-selected').click(function () {
        selectedReports = [];
        $('.dt-checkboxes:checked').each(function() {
            selectedReports.push( $(this).parent().data('report-id') );
            // console.log($(this).parent().data('user-id') )
        });
        console.log(selectedReports);
        // console.log(selectedUsers.length);
        if(selectedReports.length < 1)
            Swal.fire({
                type: 'error',
                title: '{{ __('dashboard.alerts.no_data_selected') }}',
                text: '{{ __('dashboard.alerts.select_data') }}',
                confirmButtonClass: 'btn btn-primary',
                buttonsStyling: false,
            })
        else {
            console.log(selectedReports)
            Swal.fire({
                title: '{{ __('dashboard.user.do_you_want_to_delete_selected_users') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('admin.reports.destroy_selected') }}",
                        method: 'POST',
                        // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        // _token: "{{ csrf_token() }}",
                        data: {
                            reports: selectedReports
                        },
                        success: function(response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })

                            // Remove the raw
                            selectedReports.forEach(report => {
                                $('#report-' + report).remove();
                            })
                        }
                    });
                }
            })
        }
    });
</script>
@stop
