@extends('admin.master')
@section('title','اضافه ملف صوتي')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">ملفات صوتيه</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.audio.index') }}">ملفات صوتيه</a></li>
                        <li class="breadcrumb-item active">اضافة ملف صوتي</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{ route('admin.audio.store') }}" method="POST" enctype="multipart/form-data" >
        @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="ar-name" class="form-control" name="name_ar"
                                           placeholder="{{ __('dashboard.main.name_in_ar') }}"  value="{{ old('name_ar') }}">
                                    <label for="ar-name">{{ __('dashboard.main.name_in_ar') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                                </div>
                            </div>

                        <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="en-name" class="form-control" name="name_en"
                                           placeholder="{{ __('dashboard.main.name_in_en') }}"  value="{{ old('name_en') }}">
                                    <label for="en-name">{{ __('dashboard.main.name_in_en') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                                </div>
                            </div>

                        <div class="col-md-6 col-12 ">
                            <div class="form-label-group form-group">
                                <input type="file" id="audio" class="form-control" name="audio" placeholder="{{ __('dashboard.subcategories.audio') }}">
                                <label for="email-id-column">{{ __('dashboard.main.audio') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "audio"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="projectinput2"> أختر الخدمه </label>
                                @if($products -> count() > 0 )
                                    @foreach($products as $product )
                                        <li class="d-flex justify-content-between align-items-center py-25">
                                                        <span class="vs-checkbox-con vs-checkbox-primary">
                                                            <input name="product_id[]" value="{{$product -> id}}" class="checkbox-input checkk"
                                                                   type="checkbox">
                                                            <span class="vs-checkbox">
                                                                <span class="vs-checkbox--check">
                                                                    <i class="vs-icon feather icon-check"></i>
                                                                </span>
                                                            </span>
                                                            <span class="">{{$product -> name_ar }}</span>
                                                        </span>
                                        </li>
                                    @endforeach
                                @endif
                                @include('admin.includes.alerts.input-errors', ['input' => "product_id"])
                            </div>
                        </div>


                        <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                            </div>
                    </div>
                                </div>
            </div>
        </form>
            </div>



@endsection
