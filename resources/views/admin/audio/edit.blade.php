@extends('admin.master')
@section('title','تعديل ملفات صوتية')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">ملفات صوتية</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.audio.index') }}">ملفات صوتية</a></li>
                            <li class="breadcrumb-item active">تعديل ملفات صوتية</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- START: Content-->
        <form class="form" action="{{ route('admin.audio.update' , $audio) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="name_ar"
                                       placeholder="{{ __('dashboard.main.name_in_ar') }}"  value="{{ $audio -> name_ar }}">
                                <label for="ar-name">{{ __('dashboard.main.name_in_ar') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="en-name" class="form-control" name="name_en"
                                       placeholder="{{ __('dashboard.main.name_in_en') }}"  value="{{ $audio -> name_en }}">
                                <label for="en-name">{{ __('dashboard.main.name_in_en') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="projectinput2"> أختر القسم الفرعي </label>
                                <select name="product_id" class="select2 form-control">
                                    <optgroup label="من فضلك أختر القسم الفرعي ">

                                            @foreach($audio -> products as $product)
                                                <option  value="{{$product -> id }}" @if($audio -> product_id == $product -> id)selected @endif>{{$product -> name_ar}}</option>
                                            @endforeach

                                    </optgroup>
                                </select>
                                @include('admin.includes.alerts.input-errors', ['input' => "subcategory_id"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12 ">
                            <div class="form-label-group form-group">
                               <input type="file" id="audio" class="form-control" name="audio" placeholder="{{ __('dashboard.subcategories.audio') }}">
                               <label for="email-id-column">{{ __('dashboard.main.audio') }}</label>

                        <div class="col-md-6 co-12">
                            <div class="form-label-group form-group">
                                <audio controls style="height:54px;">
                                    <source src="{{ asset($audio -> audioPath) }}">
                                </audio>
                            </div>
                        </div>
                                @include('admin.includes.alerts.input-errors', ['input' => "audio"])
                            </div>
                        </div>

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
        @endsection
