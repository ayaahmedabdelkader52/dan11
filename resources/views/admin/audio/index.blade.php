@extends('admin.master')
@section('title', 'الملفات الصوتيه')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الملفات الصوتيه</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">كل الملفات الصوتيه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <section id="data-list-view" class="data-list-view-header">
            <div class="action-btns d-none">
                <div class="btn-dropdown mr-1 mb-1">
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('dashboard.main.Actions') }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('admin.audio.create')}}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                            <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data list view Ends -->
            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table data-list-view">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('dashboard.main.name_in_ar') }}</th>
                        <th>{{ __('dashboard.main.name_in_en') }}</th>
                        <th>{{__('dashboard.main.audio')}}</th>
                        <th>{{ __('dashboard.main.Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($manyaudio as $audio)
                        <tr id="audio-{{$audio->id}}">
                            <td data-audio-id = '{{$audio -> id}}' ></td>
                            <td>{{$audio -> name_ar  }}</td>
                            <td>{{$audio -> name_en  }}</td>
                            <td>
                                <audio controls style="height:54px;">
                                    <source src="{{ asset($audio -> audioPath) }}">
                                </audio>
                            </td>

                            <td>

                                <a href="#">
                                 <span class="action-delete" data-report-id="{{ $audio->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>
                                </a>

                                <a href="{{ route('admin.audio.edit', $audio) }}">
                                <span data-audio-id="{{$audio -> id}}" data-name_ar="{{ $audio->name_ar }}" data-name_en="{{ $audio->name_en }}"
                                       title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>

        var audio = [];

        $('.action-delete').on('click', function () {

            var audio = $(this).data('report-id'),
                url = '{{ route("admin.audio.destroy", ":id") }}',
                newUrl = url.replace(':id', audio);

            console.log(audio);
            console.log(newUrl);

            Swal.fire({
                title: 'هل تريد حذف الملف الصوتي ؟',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم احذفه ',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: 'الغاء',
                buttonsStyling: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'GET',
                         {{--_token: "{{ csrf_token() }}",--}}
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#audio-' + audio).remove();
                        }
                    });
                }
            })
        });


        $('.delete-all').click(function () {

            var selectedaudio = [];

            $('.dt-checkboxes:checked').each(function() {
                selectedaudio.push( $(this).parent().data('audio-id') );
            });
             // console.log(selectedaudio.length);
            if(selectedaudio.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الملفات المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.audio.destroySelected') }}",
                            method: 'POST',
                            {{-- headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
                            {{--_token: "{{ csrf_token() }}",--}}
                            data: {
                                ids : selectedaudio
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedaudio.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#audio-' + element).remove();
                                });
                            }
                        });
                    }
                })

            }

        });
    </script>
@endsection
