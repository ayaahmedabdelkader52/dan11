@extends('admin.master')
@section('title','اضافه زفات')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0"> الزفات</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.products.index') }}"> الزفات </a></li>
                        <li class="breadcrumb-item active">اضافة زفات </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{route('admin.products.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="ar-name" class="form-control" name="name_ar"
                                           placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus value="{{ old('name_ar') }}">
                                    <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                                </div>
                            </div>


                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="en-name" class="form-control" name="name_en"
                                           placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus value="{{ old('name_en') }}">
                                    <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                                </div>
                            </div>


                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="ar-description">{{ __('dashboard.main.description_in_ar' ) }}</label>
                                    <input type="text" id="ar-description" class="form-control" name="description_ar"
                                           placeholder="{{ __('dashboard.main.description_in_ar' ) }}" autofocus value="{{ old('description_ar') }}">

                                    @include('admin.includes.alerts.input-errors', ['input' => "description_ar"])
                                </div>
                            </div>
                             <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="en-description">{{ __('dashboard.main.description_in_en' ) }}</label>
                                    <input type="text" id="en-description" class="form-control" name="description_en"
                                           placeholder="{{ __('dashboard.main.description_in_en' ) }}" autofocus value="{{ old('description_en') }}">

                                    @include('admin.includes.alerts.input-errors', ['input' => "description_en"])
                                </div>
                            </div>


                             <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="photo">{{ __('dashboard.main.photo' ) }}</label>
                                    <input type="file" id="photo" class="form-control" name="photo"
                                           placeholder="{{ __('dashboard.main.photo' ) }}" autofocus>

                                    @include('admin.includes.alerts.input-errors', ['input' => "photo"])
                                </div>
                            </div>


                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="price" class="form-control" name="price_before"
                                           placeholder="{{ __('dashboard.main.price') }}" autofocus >
                                    <label for="price">{{ __('dashboard.main.price') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "price"])
                                </div>
                            </div>


{{--                            <div class="col-md-6 col-12">--}}
{{--                                <div class="form-label-group form-group">--}}
{{--                                    <label for="category"> أختر القسم الرئيسي </label>--}}
{{--                                        <select name="category_id" id="category" class="select2 form-control">--}}
{{--                                     <optgroup label="من فضلك أختر القسم الرئيسي">--}}
{{--                                        @if($categories && $categories -> count() > 0)--}}
{{--                                            @foreach($categories as $category)--}}
{{--                                                <option  value="{{$category -> id }}">{{$category -> name_ar}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                    </optgroup>--}}
{{--                                </select>--}}
{{--                                @include('admin.includes.alerts.input-errors', ['input' => "category_id"])--}}
{{--                            </div>--}}
{{--                        </div>--}}

                             <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="subcategory"> أختر القسم الفرعي </label>
                                        <select name="subcategory_id" id="subcategory" class="select2 form-control">
                                     <optgroup label=" أختر القسم الفرعي ">
                                        @if($subcategories && $subcategories -> count() > 0)
                                            @foreach($subcategories as $subcategory)
                                                <option value="{{$subcategory -> id }}">{{$subcategory -> name_ar}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                </select>

                            </div>
                        </div>
                        <div class="col-12">
                            <li class="d-flex justify-content-between align-items-center py-25">
                                     <span class="vs-checkbox-con vs-checkbox-primary">
                                         <input name="offer" class="checkbox-input checkk" value="0" type="checkbox">
                                           <span class="vs-checkbox">
                                           <span class="vs-checkbox--check">
                                            <i class="vs-icon feather icon-check"></i>
                                           </span>
                                           </span>
                                        <span class=""> تخفيض </span>
                                     </span>
                            </li>
                        </div>
                        <div class="col-md-6 col-12 offers"  style="display: none">
                            <div class="form-group">
                                <input type="text" id="price_after" class="form-control" name="price_after"
                                       placeholder="{{ __('dashboard.main.price_after') }}" autofocus >
                                <label for="price_after">{{ __('dashboard.main.price_after') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "price_after"])
                            </div>
                        </div>


                        <div class="col-md-6 col-12 offers" style="display: none" >
                            <div class="form-group">
                                <input type="date" id="start" class="form-control" name="start"
                                       placeholder="{{ __('dashboard.main.start') }}" autofocus >
                                <label for="start"> بداية العرض </label>
                                @include('admin.includes.alerts.input-errors', ['input' => "start"])
                            </div>
                        </div>
                        <div class="col-md-6 col-12 offers" style="display: none" >
                            <div class="form-group">
                                <input type="date" id="end" class="form-control" name="end" autofocus >
                                <label for="end">نهاية العرض</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "end"])
                            </div>
                        </div>


                           <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                            </div>

                 </div>
              </div>
            </div>
        </form>
    </div>

@endsection
@section('scripts')
    <script>
        $('.checkk').on("change" , function (){
            this.value = this.checked ? 1 : 0;
            if($(this).is(':checked') ) {
                $('.offers').show();


            }else {
                $('.offers').hide();

            }
        })


    </script>
@endsection
