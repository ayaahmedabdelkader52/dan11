@extends('admin.master')
@section('title', 'الزفات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الزفات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.products.index') }}">الزفات</a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.products.show' , $product -> id ) }}">{{ $product->name }}</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="content-body">
            <!-- page users view start -->
            <section class="page-users-view">
                <div class="row">
                    <!-- account start -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">عرض تفاصيل الزفه </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                        <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                            <input type="text" id="ar-name" class="form-control" name="name_ar"
                                                   placeholder="{{ __('dashboard.main.name_in_ar') }}" disabled value="{{$product -> name_ar}}">
                                        </div>
                                        <div class="col-md-6 col-12">
                                        <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                            <input type="text" id="en-name" class="form-control" name="name_en"
                                                   placeholder="{{ __('dashboard.main.name_in_en') }}" disabled value="{{$product -> name_en}}">
                                        </div>
                                        <div class="col-md-6 col-12">
                                        <label for="ar-description">{{ __('dashboard.main.description_in_ar' ) }}</label>
                                        <input type="text" id="ar-description" class="form-control" name="description_ar"
                                               placeholder="{{ __('dashboard.main.description_in_ar' ) }}" disabled value="{{ $product -> description_ar }}">
                                        </div>
                                        <div class="col-md-6 col-12">
                                        <label for="en-description">{{ __('dashboard.main.description_in_en' ) }}</label>
                                        <input type="text" id="en-description" class="form-control" name="description_ar"
                                               placeholder="{{ __('dashboard.main.description_in_en' ) }}" disabled value="{{ $product -> description_en }}">
                                        </div>
                                        @if($product -> offer == 0)
                                        <div class="col-md-6 col-12">
                                        <label for="price">{{ __('dashboard.main.price') }}</label>
                                        <input type="text" id="price" class="form-control" name="price"
                                               placeholder="{{ __('dashboard.main.price') }}" disabled value="{{$product -> price_after }}" >
                                        </div>
                                        @endif
                                        @if($product -> offer == 1)
                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.price_before') }}</label>
                                                <input type="text" id="price" class="form-control" name="price_before"
                                                       placeholder="{{ __('dashboard.main.price_before') }}" disabled value="{{$product -> price_before }}" >
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.price_after') }}</label>
                                                <input type="text" id="price" class="form-control" name="price_after"
                                                       placeholder="{{ __('dashboard.main.price_after') }}" disabled value="{{$product -> price_after }}" >
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.start') }}</label>
                                                <input type="date" id="price" class="form-control" name="price"
                                                       placeholder="{{ __('dashboard.main.start') }}" disabled value="{{$product -> start }}" >
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.end') }}</label>
                                                <input type="date" id="price" class="form-control" name="price"
                                                       placeholder="{{ __('dashboard.main.end') }}" disabled value="{{$product -> end }}" >
                                            </div>
                                        @endif

                                        <div class="col-md-6 col-12">
                                        <label for="category_id">القسم الرئيسي </label>
                                        <input type="text" id="category_id" class="form-control" name="category_id"
                                               placeholder="القسم" disabled value="{{$product -> category -> name_ar }}" >
                                        </div>
                                    <div class="col-md-6 col-12">
                                        <label for="subcategory_id">القسم الفرعي </label>
                                        <input type="text" id="subcategory_id" class="form-control" name="subcategory_id"
                                               placeholder="القسم" disabled value="{{$product -> subcategory -> name_ar }}" >
                                    </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div>
                                                <img style="width: 50%" src="{{$product -> photoPath}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



    </div>
@endsection

