@extends('admin.master')
@section('title', 'الزفات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الزفات </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">كل الزفات </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <section id="data-list-view" class="data-list-view-header">
            <div class="action-btns d-none">
                <div class="btn-dropdown mr-1 mb-1">
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('dashboard.main.Actions') }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('admin.products.create')}}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                            <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data list view Ends -->
            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table data-list-view">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('dashboard.main.name_in_ar') }}</th>
                        <th>{{ __('dashboard.main.name_in_en') }}</th>
                        <th>{{ __('dashboard.main.price') }}</th>
                        <th>{{ __('dashboard.main.category') }}</th>
                        <th>{{ __('dashboard.main.subcategory') }}</th>
                        <th>{{ __('dashboard.main.Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)

                        <tr id="product-{{$product->id}}">

                            <td data-product-id ="{{$product->id}}"></td>
                            <td>{{$product -> name_ar  }}</td>
                            <td>{{$product -> name_en  }}</td>
                            <td>{{$product -> price_before  }}</td>
                            <td>{{$product -> category -> name_ar}}</td>
                            <td>{{$product -> subcategory -> name_ar  }}</td>

                            <td>

                                <a href="#">
                                 <span class="action-delete" data-product="{{ $product->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>
                                </a>
                                <a href="{{ route('admin.products.show', $product) }}">
                                <span title="{{ __('dashboard.action.show') }}">
                                    <i class="feather icon-eye"></i>
                                </span>
                                </a>
                                <a href="{{ route('admin.products.edit', $product) }}">
                                <span data-product-id="{{$product -> id}}" data-name_ar="{{ $product->name_ar }}" data-name_en="{{ $product->name_en }}"
                                     data-description_ar="{{ $product -> description_ar }}"
                                      data-description_en="{{ $product -> description_en }}"
                                      title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                                </a>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
@section('scripts')

    <script>

        var product = [];

        $('.action-delete').on('click', function () {

            var product = $(this).data('product'),

                url = '{{ route("admin.products.destroy", ":id") }}',
                newUrl = url.replace(':id',product);

            console.log(product);
            console.log(newUrl);

            Swal.fire({
                title: 'هل تريد حذف الخدمه ؟',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم احذفه ',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: 'الغاء',
                buttonsStyling: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'GET',
                           _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#product-' + product).remove();
                        }
                    });
                }
            })
        });
    </script>

    <script>
        $('.delete-all').click(function () {
            var selectedproducts = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedproducts.push( $(this).parent().data('product-id') );
            });
            // console.log(selectedproducts.length);
            if(selectedproducts.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاعلانات المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.products.destroySelected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                ids: selectedproducts
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedproducts.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#product-' + element).remove();
                                } );

                            }
                        });
                    }
                })

            }

        });
    </script>
@endsection
