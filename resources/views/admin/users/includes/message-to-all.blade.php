<div class="modal fade text-left" id="message-modal-to-all" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">{{ __('dashboard.user.send_message') }} </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="notification-to-all-message-fill" data-toggle="tab" href="#notification-message-to-all-message"
                       role="tab" aria-controls="notification-message-to-all-message" aria-selected="false">{{ __('dashboard.messages.notification_send') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="sms-to-all-message-fill" data-toggle="tab" href="#sms-message-to-all-message"
                       role="tab" aria-controls="sms-message-to-all-message" aria-selected="true">{{ __('dashboard.messages.sms_send') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="email-to-all-message-fill" data-toggle="tab" href="#email-message-to-all-message"
                       role="tab" aria-controls="email-message-to-all-message" aria-selected="false">{{ __('dashboard.messages.email_send') }}</a>
                </li>
            </ul>

            <div class="tab-content pt-1">
                <div class="tab-pane active" id="notification-message-to-all-message" role="tabpanel" aria-labelledby="notification-to-all-message-fill">
                    <form action="{{ route('admin.users.send_notification_to_all') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <fieldset class="form-group">
                                        <input name="title" class="form-control"
                                               placeholder="{{ __('dashboard.messages.notification_title') }}">
                                    </fieldset>

                                    <fieldset class="form-group">
                                                <textarea name="message" class="form-control"
                                                          rows="3" placeholder="{{ __('dashboard.messages.notification_content') }}"></textarea>
                                    </fieldset>

                                    @include('admin.includes.alerts.input-errors', ['input' => 'message'])
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" >{{ __('dashboard.action.send') }}</button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="sms-message-to-all-message" role="tabpanel" aria-labelledby="sms-to-all-message-fill">
                    <form action="{{ route('admin.users.send_sms_to_all') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <fieldset class="form-group">
                                            <textarea name="message" class="form-control"
                                                      rows="3" placeholder="{{ __('dashboard.messages.sms_content') }}"></textarea>
                                    </fieldset>

                                    @include('admin.includes.alerts.input-errors', ['input' => 'message'])
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">{{ __('dashboard.action.send') }}</button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="email-message-to-all-message" role="tabpanel" aria-labelledby="email-to-all-message-fill">
                    <form action="{{ route('admin.users.send_email_to_all') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <fieldset class="form-group">
                                        <textarea name="message" class="form-control"
                                                  rows="3" placeholder="{{ __('dashboard.messages.email_content') }}"></textarea>
                                    </fieldset>

                                    @include('admin.includes.alerts.input-errors', ['input' => 'message'])
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" >{{ __('dashboard.action.send') }}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
