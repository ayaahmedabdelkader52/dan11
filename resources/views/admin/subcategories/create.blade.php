@extends('admin.master')
@section('title','اضافه قسم فرعي')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">اقسام فرعيه</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.subcategories.index') }}">الاقسام الفرعيه</a></li>
                        <li class="breadcrumb-item active">اضافة قسم فرعي</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{route('admin.subcategories.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="ar-name" class="form-control" name="name_ar"
                                           placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus value="{{ old('name_ar') }}">
                                    <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                                </div>
                            </div>


                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="en-name" class="form-control" name="name_en"
                                           placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus value="{{ old('name_en') }}">
                                    <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                                </div>
                            </div>


                            <div class="col-md-6 col-12 ">
                                <div class="form-label-group form-group">
                                    <input type="file" id="image" class="form-control" name="photo"  placeholder="{{ __('dashboard.main.cat_photo') }}">
                                    <label for="image">{{ __('dashboard.main.photo') }}</label>

                                    @include('admin.includes.alerts.input-errors', ['input' => 'photo'])
                                </div>
                                </div>


                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="projectinput2"> أختر القسم </label>
                                        <select name="category_id" class="select2 form-control">
                                     <optgroup label=" أختر القسم ">
                                        @if($categories && $categories -> count() > 0)
                                            @foreach($categories as $category)
                                                <option name="category_id" value="{{$category -> id }}">{{$category -> name_ar}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                </select>
                                @include('admin.includes.alerts.input-errors', ['input' => "category_id"])
                            </div>
                        </div>







                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                            </div>
                    </div>
                 </div>
              </div>
        </form>
    </div>
    </div>
@endsection
