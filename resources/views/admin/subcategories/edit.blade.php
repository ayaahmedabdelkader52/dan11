@extends('admin.master')
@section('title','تعديل القسم الفرعي')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاقسام الفرعيه</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.subcategories.index') }}">الاقسام الفرعيه</a></li>
                            <li class="breadcrumb-item active">تعديل القسم الفرعي</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <form class="form" action="{{ route('admin.subcategories.update' , $subcategory) }}" method="POST" enctype="multipart/form-data" >
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="name_ar"
                                       placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus value="{{ $subcategory -> name_ar }}">
                                <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="en-name" class="form-control" name="name_en"
                                       placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus value="{{$subcategory -> name_en }}">
                                <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                            </div>
                        </div>



                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="file" id="image" class="form-control" name="photo" placeholder="{{ __('dashboard.main.photo') }}">
                                <label for="email-id-column">{{ __('dashboard.main.photo') }}</label>

                                <div class="form-label-group">
                                    <div class="multi-img-result">
                                        <div class="img-uploaded">
                                            <img src="{{ $subcategory -> photoPath }}" alt="">
                                        </div>
                                    </div>
                                </div>

                                @include('admin.includes.alerts.input-errors', ['input' => 'photo'])
                            </div>
                        </div>

                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="projectinput2"> أختر القسم </label>
                                    <select name="category_id" class="select2 form-control">
                                        <optgroup label="من فضلك أختر القسم ">
                                            @if($categories && $categories -> count() > 0)
                                                @foreach($categories as $category)
                                                    <option
                                                        value="{{$category -> id }}" @if($subcategory -> category_id == $category -> id)selected @endif >{{$category -> name_ar}}</option>
                                                @endforeach
                                            @endif
                                        </optgroup>
                                    </select>
                                </div>
                            </div>


                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.edit') }}</button>
                            </div>
                    </div>
                </div>
            </div>
        </form>
@endsection
