@extends('admin.master')

@section('title', 'الاشعارات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاشعارات</h2>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <section id="alert-colors">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            الاشعارات
                        </h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            @foreach($notifications as $notification)
                                <a href="{{ $notification->url }}">
                                    <div class="alert alert-primary" role="alert">
                                        <h4 class="alert-heading">{{ $notification->title }}</h4>
                                        <p class="mb-0">{{ $notification->content }}</p>
                                    </div>
                                </a>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
@endsection
