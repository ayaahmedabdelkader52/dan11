@extends('admin.master')

@section('title', 'اضافه اشعار')

@section('content')

    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاشعارات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الاشعارات</a>
                            </li>
                            <li class="breadcrumb-item active">اضافه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->
    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ __('dashboard.action.create') }}</h4>
                        </div>

                        <!-- You can all alert messages by removing the comment -->
                        {{--                        @include('admin.includes.alerts.all-errors')--}}
                        <div class="card-content">
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="notification-to-all-message-fill" data-toggle="tab"
                                           href="#notification-message-to-all-message"
                                           role="tab" aria-controls="notification-message-to-all-message"
                                           aria-selected="false">{{ __('dashboard.messages.notification_send') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="sms-to-all-message-fill" data-toggle="tab"
                                           href="#sms-message-to-all-message"
                                           role="tab" aria-controls="sms-message-to-all-message"
                                           aria-selected="true">{{ __('dashboard.messages.sms_send') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="email-to-all-message-fill" data-toggle="tab"
                                           href="#email-message-to-all-message"
                                           role="tab" aria-controls="email-message-to-all-message"
                                           aria-selected="false">{{ __('dashboard.messages.email_send') }}</a>
                                    </li>

                                </ul>

                                <div class="tab-content pt-1">
                                    <div class="tab-pane active" id="notification-message-to-all-message" role="tabpanel"
                                         aria-labelledby="notification-to-all-message-fill">
                                        <form action="{{ route('admin.notification.store_notification') }}"
                                              method="post">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <fieldset class="form-group">
                                                            <input name="title" class="form-control"
                                                                   placeholder="نص عنوان الرساله">
                                                        </fieldset>

                                                        @error('title')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>

                                                    <div class="col-12">
                                                        <fieldset class="form-group">
                                                          <textarea name="message" class="form-control"
                                                                    rows="3"
                                                                    placeholder="نص محتوي الرساله"></textarea>
                                                        </fieldset>
                                                        @error('message')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>


                                                    <div class="col-12">
                                                        <div class="media">
                                                            <a href="javascript: void(0);">
                                                                <img id="output-notification"
                                                                     src="{{ asset('assets/uploads/socials/default.jpg')}}"
                                                                     class="rounded mr-75" alt="profile image"
                                                                     height="64" width="64">
                                                            </a>
                                                            <div class="media-body mt-75">
                                                                <div
                                                                    class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                                                                    <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer"
                                                                        for="notification"> اضافه صوره
                                                                        للاشعار(اختياري) </label>
                                                                    <input type="file" id="notification" name="image"
                                                                           hidden
                                                                           accept="image"
                                                                           class="input-img" onchange="loadFile(event)">
                                                                    @error('image')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-label-group form-group">
                                                            <select class="form-control" name="user_type">
                                                                <option value="admins">الاداره</option>
                                                                <option value="all_users">جميع العملاء</option>
                                                                <option value="active_users" >العملاء النشطين</option>
                                                                <option value="inactive_users" >العملاء غير النشطين</option>
                                                                <option value="block_users" >العملاء المحظورين</option>
                                                            </select>
                                                            <label for="email-id-column">{{ __('dashboard.user.status') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit"
                                                        class="btn btn-primary">{{ __('dashboard.action.send') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="sms-message-to-all-message" role="tabpanel"
                                         aria-labelledby="sms-to-all-message-fill">
                                        <form action="{{ route('admin.notification.store_sms') }}" method="post">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <fieldset class="form-group">
                                                          <textarea name="message" class="form-control"
                                                                    rows="3"
                                                                    placeholder="نص محتوي الرساله"></textarea>
                                                        </fieldset>
                                                        @error('message')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-label-group form-group">
                                                            <select class="form-control" name="user_type">
                                                                <option value="admins">الاداره</option>
                                                                <option value="all_users">جميع العملاء</option>
                                                                <option value="active_users" >العملاء النشطين</option>
                                                                <option value="inactive_users" >العملاء غير النشطين</option>
                                                                <option value="block_users" >العملاء المحظورين</option>
                                                            </select>
                                                            <label for="email-id-column">{{ __('dashboard.user.status') }}</label>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit"
                                                        class="btn btn-primary">{{ __('dashboard.action.send') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="email-message-to-all-message" role="tabpanel"
                                         aria-labelledby="email-to-all-message-fill">
                                        <form action="{{ route('admin.notification.store_email') }}" method="post">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <fieldset class="form-group">
                                                          <textarea name="message" class="form-control"
                                                                    rows="3"
                                                                    placeholder="نص محتوي الرساله"></textarea>
                                                        </fieldset>
                                                        @error('message')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-label-group form-group">
                                                            <select class="form-control" name="user_type">
                                                                <option value="admins">الاداره</option>
                                                                <option value="all_users">جميع العملاء</option>
                                                                <option value="active_users" >العملاء النشطين</option>
                                                                <option value="inactive_users" >العملاء غير النشطين</option>
                                                                <option value="block_users" >العملاء المحظورين</option>
                                                            </select>
                                                            <label for="email-id-column">{{ __('dashboard.user.status') }}</label>


                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit"
                                                        class="btn btn-primary">{{ __('dashboard.action.send') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@endsection
@section('scripts')
    <script>

     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    var loadFile = function(event) {

        var outputId = event.path[0].id;
        outputId = 'output-' + outputId ;

        // console.log(outputId);
        // console.log(event.path[0].id);

        var outputs = document.getElementById(outputId);
        outputs.src = URL.createObjectURL(event.target.files[0]);
        outputs.onload = function() {
          URL.revokeObjectURL(outputs.src) // free memory
        }
      };



    </script>

@stop
