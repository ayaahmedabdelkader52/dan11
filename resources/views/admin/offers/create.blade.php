@extends('admin.master')
@section('title','اضافه تخفيض ')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">التخفيضات</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.offers.index') }}">التخفيضات</a></li>
                        <li class="breadcrumb-item active">اضافة تخفيض</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{ route('admin.offers.store') }}" method="POST" enctype="multipart/form-data" >
        @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">


                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="subcategory"> أختر القسم  </label>
                                <select name="category_id" id="category" class="select2 form-control">
                                    <optgroup label=" أختر القسم  ">
                                        @if($categories -> count() > 0)
                                            @foreach($categories as $category)
                                                <option value="{{$category -> id }}">{{$category -> name_ar}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="value" class="form-control" name="value"
                                           placeholder="{{ __('dashboard.main.value') }}"  value="{{ old('value') }}">
                                    <label for="value">{{ __('dashboard.main.value') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "value"])
                                </div>
                            </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="type"> أختر نوع التخفيض  </label>
                                <select name="type" id="type" class="select2 form-control">
                                    <optgroup label=" أختر نوع العرض ">
                                                <option value="percentage"> %العرض </option>
                                                <option value="monetary"> نقدي </option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="start">{{ __('dashboard.main.start' ) }}</label>
                                <input type="date" id="start" class="form-control" name="start"
                                       placeholder="{{ __('dashboard.main.start' ) }}" autofocus value="{{ old('start') }}">
                                @include('admin.includes.alerts.input-errors', ['input' => "start"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="start">{{ __('dashboard.main.end' ) }}</label>
                                <input type="date" id="end" class="form-control" name="end"
                                       placeholder="{{ __('dashboard.main.end' ) }}" autofocus value="{{ old('end') }}">
                                @include('admin.includes.alerts.input-errors', ['input' => "end"])
                            </div>
                        </div>


                        <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                            </div>
                    </div>
                                </div>
            </div>
        </form>
            </div>



@endsection
