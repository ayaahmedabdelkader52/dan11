@extends('admin.master')
@section('title', 'التخفيضات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">

        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">التخفيضات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">كل التخفيضات</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <section id="data-list-view" class="data-list-view-header">

            <div class="action-btns d-none">
                <div class="btn-dropdown mr-1 mb-1">
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('dashboard.main.Actions') }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('admin.offers.create')}}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                            <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Data list view Ends -->


            <!-- DataTable starts -->
            <div class="table-responsive">

                <table class="table data-list-view">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('dashboard.main.category') }}</th>
                        <th>{{ __('dashboard.main.value') }}</th>

                        <th>{{__('dashboard.main.start')}}</th>
                        <th>{{ __('dashboard.main.end') }}</th>
                        <th>{{ __('dashboard.main.Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($offers as $offer)
                        <tr id="offer-{{$offer->id}}">
                            <td data-offer-id = '{{$offer -> id}}' ></td>
                            <td> {{$offer -> category -> name_ar}} </td>
                            <td>@if($offer->type == 'percentage') {{$offer->value}}% @else {{$offer -> value}} @endif</td>
                            <td>{{$offer -> start }}</td>
                            <td>{{$offer -> end  }}</td>


                            <td>

                                <a href="#">
                                 <span class="action-delete" data-report-id="{{ $offer->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>
                                </a>

                                <a href="{{ route('admin.offers.edit', $offer) }}">
                                <span data-offer-id="{{$offer -> id}}"
                                      data-name_ar="{{ $offer->name_ar }}" data-name_en="{{ $offer->name_en }}"
                                       title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>

        var offer = [];

        $('.action-delete').on('click', function () {

            var offer = $(this).data('report-id'),
                url = '{{ route("admin.offers.destroy", ":id") }}',
                newUrl = url.replace(':id', offer);

            console.log( offer);
            console.log( newUrl);

            Swal.fire({
                title: 'هل تريد حذف القسم ؟',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم احذفه ',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: 'الغاء',
                buttonsStyling: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'GET',
                         {{--_token: "{{ csrf_token() }}",--}}
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#offer-' + offer).remove();
                        }
                    });
                }
            })
        });



        $('.delete-all').click(function () {

            var offers = [];

            $('.dt-checkboxes:checked').each(function() {
                offers.push( $(this).parent().data('offer-id') );
            });
             // console.log(offers.length);
            if(offers.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاقسام المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.offers.destroy_selected') }}",
                            method: 'POST',
                            {{-- headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
                            {{--_token: "{{ csrf_token() }}",--}}
                            data: {
                                ids : offers
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                offers.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#offer-' + element).remove();
                                });
                            }
                        });
                    }
                })

            }

        });
    </script>
@endsection
