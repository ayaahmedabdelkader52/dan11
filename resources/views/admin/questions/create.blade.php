@extends('admin.master')
@section('title','اضافه اسئله ')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">الاسئله </h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.questions.index') }}">الاسئله</a></li>
                        <li class="breadcrumb-item active">اضافة اسئله </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{ route('admin.questions.store') }}" method="POST" enctype="multipart/form-data" >
        @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="question_ar" class="form-control" name="question_ar"
                                           placeholder="{{ __('dashboard.main.question_ar') }}"  value="{{ old('question_ar') }}">
                                    <label for="question_ar">{{ __('dashboard.main.question_ar') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                                </div>
                            </div>

                        <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="question_en" class="form-control" name="question_en"
                                           placeholder="{{ __('dashboard.main.question_en') }}"  value="{{ old('question_en') }}">
                                    <label for="question_en">{{ __('dashboard.main.question_en') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "question_en"])
                                </div>
                            </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="ar-description">{{ __('dashboard.main.answer_ar' ) }}</label>
                                <input type="text" id="ar-description" class="form-control" name="answer_ar"
                                       placeholder="{{ __('dashboard.main.answer_ar' ) }}" autofocus value="{{ old('answer_ar') }}">

                                @include('admin.includes.alerts.input-errors', ['input' => "answer_ar"])
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="en-description">{{ __('dashboard.main.answer_en' ) }}</label>
                                <input type="text" id="en-description" class="form-control" name="answer_en"
                                       placeholder="{{ __('dashboard.main.answer_en' ) }}" autofocus value="{{ old('answer_en') }}">

                                @include('admin.includes.alerts.input-errors', ['input' => "answer_en"])
                            </div>
                        </div>



                        <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                            </div>
                    </div>
                                </div>
            </div>
        </form>
            </div>



@endsection
