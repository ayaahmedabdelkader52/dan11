<div class="tab-pane fade" id="sms" role="tabpanel"
     aria-labelledby="account-pill-connections" aria-expanded="false">
    <form action="{{route('admin.settings.update_sms')}}" method="post">
        @csrf
        @method('PUT')

        @include('admin.includes.alerts.input-errors', ['input' => "type"])
        <div class="row">


            @foreach($smsPackages as $package)
                <div class="col-md-6">
                    <h3 class="panel-title">{{ ucfirst($package->type) }}</h3>
                    <div class="row">

                        <div class="col-12">
                            <div class="vs-radio-con vs-radio-primary">
                                <input id="sms-{{ $package->type }}" type="radio" value="{{ $package->type }}" name="type"
                                    {{ ($package && $package->active == 'true') ? 'checked' : '' }} />
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <label for="sms-{{ $package->type }}" class="font-medium-2" >تنشيط</label>
                            </div>
                            <br>
                        </div>

                        <div class="col-12">
                            <div class="form-label-group form-group">
                                    <input type="text" class="form-control" name="username[{{ $package->type }}]" placeholder="اسم المستخدم"
                                           value="{{ ($package) ? $package->username : ''}}">

                                    <label for="account-name">اسم المستخدم </label>

                                @include('admin.includes.alerts.input-errors', ['input' => "username.$package->type"])
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-label-group form-group">
                                <input value="{{ ($package) ? $package->password : '' }}"
                                       placeholder="الرقم السري" type="password" class="form-control" name="password[{{ $package->type }}]">
                                <label for="account-e-mail">الرقم السري</label>


                                @include('admin.includes.alerts.input-errors', ['input' => "password.$package->type"])
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-label-group form-group">
                                <input type="text" class="form-control" name="sender_name[{{ $package->type }}]" placeholder="اسم الراسل"
                                       value="{{ ($package) ? $package->sender_name : '' }}">

                                <label for="account-name">اسم الراسل </label>

                                @include('admin.includes.alerts.input-errors', ['input' => "sender_name.$package->type"])
                            </div>
                        </div>

                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                                حفظ التعديلات
                            </button>
                        </div>

                        <br>

                    </div>
                </div>
            @endforeach

            @foreach($otherPackages as $key => $package)
                    <div class="col-md-6">
                        <h5 class="panel-title">{{ ucfirst($package) }}</h5>
                        <div class="row">

                            <div class="col-12">
                                <div class="vs-radio-con vs-radio-primary">
                                    <input id="sms-{{ $package }}" type="radio" value="{{ $package }}" name="type"/>
                                    <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                    <label for="sms-{{ $package }}" class="font-medium-2" >تنشيط</label>
                                </div>
                                <br>
                            </div>

                            <div class="col-12">
                                <div class="form-label-group form-group">

                                    <input type="text" class="form-control" name="username[{{ $package }}]" placeholder="اسم المستخدم">
                                    <label for="account-name">اسم المستخدم </label>

                                    @include('admin.includes.alerts.input-errors', ['input' => "username.$package"])
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-label-group form-group">
                                    <input placeholder="الرقم السري" type="password" class="form-control" name="password[{{ $package }}]">
                                    <label for="account-e-mail">الرقم السري</label>


                                    @include('admin.includes.alerts.input-errors', ['input' => "password.$package"])
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" class="form-control" name="sender_name[{{ $package }}]" placeholder="اسم الراسل"
                                           value="">

                                    <label for="account-name">اسم الراسل </label>

                                    @include('admin.includes.alerts.input-errors', ['input' => "sender_name.$package"])
                                </div>
                            </div>

                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                                    حفظ التعديلات
                                </button>
                            </div>

                            <br>

                        </div>
                    </div>
            @endforeach
        </div>
    </form>

</div>
