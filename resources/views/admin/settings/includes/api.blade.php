<div class="tab-pane fade" id="api" role="tabpanel" aria-labelledby="terms-pill-Api" aria-expanded="true">
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.settings.update')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="settings[google_places_key]"
                                   placeholder="Google Places Key" value="{{ ($setting['google_places_key']) ?? '' }}">
                            <label for="account-name">Google Places Key</label>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="settings[wasl_api_key]"
                                   placeholder="Wasl API Key" value="{{ ($setting['wasl_api_key']) ?? '' }}">
                            <label for="account-name">Wasl API Key</label>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="settings[currencyconverterapi]"
                                   placeholder="Currency Converter key"
                                   value="{{ ($setting['currencyconverterapi']) ?? '' }}">
                            <label for="account-name">Currency Converter key</label>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="settings[google_analytics]"
                                   placeholder="Google Analytics"
                                   value="{{ ($setting['google_analytics']) ?? '' }}">
                            <label for="account-name">Google Analytics</label>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="settings[live_chat]"
                                   placeholder="Live Chat" value="{{ ($setting['live_chat']) ?? '' }}">
                            <label for="account-name">Live Chat</label>

                        </div>
                    </div>

                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                            حفظ التغييرات
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
