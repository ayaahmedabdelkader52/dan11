<div class="tab-pane fade " id="site-settings" role="tabpanel"
     aria-labelledby="site-pill-settings" aria-expanded="false">
    <form  method="post" action="{{ route('admin.settings.update') }}" enctype="multipart/form-data">
        @csrf
        <div class="media">
            <a href="javascript: void(0);">
                <img id="output-logo"  src="{{ isset($setting['logo']) ? asset('assets/uploads/settings/' . $setting['logo'] ) : asset('assets/uploads/settings/logo.png') }}"
                      class="rounded mr-75" alt="site logo" height="64" width="64">
            </a>
            <div class="media-body mt-75">
                <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                    <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer" for="logo">تحديث لوجو الموقع</label>
                    <input type="file" id="logo" name="settings[logo]" hidden  accept="image/*" class="input-img" onchange="loadFile(event)">
                </div>
            </div>

            @include('admin.includes.alerts.input-errors', ['input' => "settings.logo"])
        </div>
        <hr>
        <div class="row">
            <div class="col-12">

                <div class="form-group">
                    <div class="controls">
                        <label for="account-e-mail">البريد الالكتروني</label>
                        <input  name="settings[site_email]" class="form-control" placeholder="البريد الالكتروني للتطبيق"
                                value="{{$setting['site_email']??''}}">
                    </div>
                    @include('admin.includes.alerts.input-errors', ['input' => "settings.site_email"])
                </div>

                <div class="form-group">
                    <div class="controls">
                        <label for="account-name">رقم الهاتف</label>
                        <input type="text"  name="settings[site_phone]" class="form-control" placeholder="رقم الهاتف للتطبيق"
                        value=" {{ $setting['site_phone'] ??  '' }}">
                    </div>
                    @include('admin.includes.alerts.input-errors', ['input' => "settings.site_email"])
                </div>

                <div class="form-group">
                    <div class="controls">
                        <label for="account-name">اسم الموقع</label>
                        <input type="text" name="settings[site_name]" class="form-control" placeholder="اسم الموقع"
                        value="{{ $setting['site_name'] ??  '' }}">
                    </div>
                    @include('admin.includes.alerts.input-errors', ['input' => "settings.site_name"])
                </div>

            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">الكلمات الدلالية</label>
                    <textarea class="form-control" name="settings[site_tagged]" rows="3"
                              placeholder="الكلمات الدلالية">{{$setting['site_tagged'] ?? ''}} </textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.site_tagged"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">حقوق التطبيق بالعربي</label>
                    <textarea class="form-control"  name="settings[copyright_ar]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{{$setting['copyright_ar'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.copyright_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">حقوق التطبيق بالانجليزي</label>
                    <textarea class="form-control"  name="settings[copyright_en]"  rows="3"
                              placeholder="حقوق التطبيق بالانجليزي">{{$setting['copyright_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.copyright_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">سياسات الموقع بالعربي</label>
                    <textarea class="form-control"  name="settings[policies_ar]"  rows="3"
                              placeholder="سياسات الموقع بالعربي">{{$setting['policies_ar'] ?? ''}} </textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.policies_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">سياسات الموقع بالانجليزي</label>
                    <textarea class="form-control"  name="settings[policies_en]"  rows="3"
                              placeholder="سياسات الموقع بالانجليزي">{{ $setting['policies_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.policies_en"])
            </div>

            <!-- contact us -->

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">عنوان العنوان بالعربي </label>
                    <textarea class="form-control" id="contact_us_ar" name="settings[contact_us_name_ar]"  rows="3"
                              placeholder="سياسات الموقع بالانجليزي">{{ $setting['contact_us_name_ar'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.contact_us_name_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">عنوان تواصل معنا بالانجليزي</label>
                    <textarea class="form-control" id="contact_us_en" name="settings[contact_us_name_en]"  rows="3"
                              placeholder="سياسات الموقع بالانجليزي">{{ $setting['contact_us_name_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.contact_us_name_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف تواصل معنا بالعربي</label>
                    <textarea class="form-control"  name="settings[contact_us_description_ar]"  rows="3"
                              placeholder="سياسات الموقع بالانجليزي">{{ $setting['contact_us_description_ar'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.contact_us_description_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف تواصل معنا بالانجليزي</label>
                    <textarea class="form-control"  name="settings[contact_us_description_en]"  rows="3"
                              placeholder="سياسات الموقع بالانجليزي">{{ $setting['contact_us_description_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.contact_us_description_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره اتصل بنا</label>
                    <input class="form-control"  name="settings[about_us_photo1]"  rows="3"
                           placeholder="اضف صورة اتصل بنا" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['about_us_photo1']) }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره  موقعنا</label>
                    <input class="form-control"  name="settings[about_us_photo2]"  rows="3"
                           placeholder="اضف صورة اتصل بنا" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['contact_us_photo2']) }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره الايميل </label>
                    <input class="form-control"  name="settings[contact_us_photo3]"  rows="3"
                           placeholder="اضف صورة الايميل " type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['contact_us_photo3']) }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره العنوان </label>
                    <input class="form-control"  name="settings[contact_us_photo4]"  rows="3"
                           placeholder="اضف صورة الايميل " type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['contact_us_photo4']) }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <!-- End contact us -->


            <!-- Faqs -->

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> الاسئله الشائعه بالعربي</label>
                    <textarea class="form-control" id="faqs_name_ar" name="settings[faqs_name_ar]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{!! $setting['faqs_name_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.faqs_name_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> الاسئله الشائعه بالانجليزي</label>
                    <textarea class="form-control" id="faqs_name_en" name="settings[faqs_name_en]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{!!$setting['faqs_name_en'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.faqs_name_ar"])
            </div>


            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف الاسئله الشائعه بالعربي</label>
                    <textarea class="form-control" id="faqs_description_ar" name="settings[faqs_description_ar]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{{$setting['faqs_description_ar'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.faqs_description_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف الاسئله الشائعه بالانجليزي</label>
                    <textarea class="form-control" id="faqs_description_en" name="settings[faqs_description_en]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{{$setting['faqs_description_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.faqs_description_en"])
            </div>


            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">يمكنك البحث  في الاسئله بالعربي</label>
                    <textarea class="form-control" id="view_faqs_ar" name="settings[view_questions_ar]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{!! $setting['view_questions_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.view_questions_ar"])
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">يمكنك البحث  في الاسئله بالانجليزي</label>
                    <textarea class="form-control" id="view_faqs_en" name="settings[view_questions_ar]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{!! $setting['view_questions_en'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.view_questions_en"])
            </div>



            <!-- End Faqs -->

            <!-- about us -->


            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> من نحن بالعربي</label>
                    <textarea class="form-control" id="about_us_name_ar" name="settings[about_us_name_ar]"  rows="3"
                              placeholder="عنوان صفحه من نحن بالانجليزي">{!!  $setting['about_us_name_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.about_us_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  من نحن بالانجليزي</label>
                    <textarea class="form-control" id="about_us_name_en" name="settings[about_us_name_en]"  rows="3"
                              placeholder="">{!! $setting['about_us_name_en'] ?? '' !!} </textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.about_us_en"])

            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف من نحن  بالعربي</label>
                    <textarea class="form-control" id="about_us_ar" name="settings[about_us_description1_ar]"  rows="3"
                              placeholder="">{!! $setting['about_us_description_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_description_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف من نحن  بالانجليزي</label>
                    <textarea class="form-control" id="about_us_en" name="settings[about_us_description1_en]"  rows="3"
                              placeholder="">{!!  $setting['about_us_description_en'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_description_en"])
            </div>



            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره من نحن 1</label>
                    <input class="form-control"  name="settings[about_us_photo1]"  rows="3"
                              placeholder="اضف صوره 1" type="file" >
                    <img id="output-account-upload"
                         src="{{ isset($setting['about_us_photo1']) ? asset('assets/uploads/settings/' . $setting['about_us_photo1'] ) : asset('assets/uploads/settings/default.png') }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">

                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره من نحن 2</label>
                    <input class="form-control"  name="settings[about_us_photo2]"  rows="3"
                           placeholder="اضف صوره2" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['about_us_photo2'] )}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره من نحن 3</label>
                    <input class="form-control"  name="settings[about_us_photo3]"  rows="3"
                           placeholder="اضف صوره3" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['about_us_photo3']) }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره من نحن 4</label>
                    <input class="form-control"  name="settings[about_us_photo4]"  rows="3"
                           placeholder="اضف صوره4" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['about_us_photo4'])}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>


            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> الرؤية بالعربي</label>
                    <textarea class="form-control" id="card_ar" name="settings[about_us_card_ar]"  rows="3"
                              placeholder="">{!! $setting['about_us_card_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_card_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> الرؤية بالانجليزي</label>
                    <textarea class="form-control" id="card_en"  name="settings[about_us_card_en]"  rows="3"
                              placeholder="">{!!  $setting['about_us_card_en'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_card_en"])
            </div>


            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره رؤيتنا </label>
                    <input class="form-control"  name="settings[about_us_card_photo]"  rows="3"
                           placeholder="صورة رؤيتنا" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['about_us_card_photo'] )}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> رسالتنا بالعربي</label>
                    <textarea class="form-control" id="messege_ar"  name="settings[about_us_message_ar]"  rows="3"
                              placeholder="">{!!  $setting['about_us_message_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_message_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> رسالتنا بالانجليزي</label>
                    <textarea class="form-control" id="messege_en" name="settings[about_us_messege_en]"  rows="3"
                              placeholder="">{!! $setting['about_us_messege_en'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_messege_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره رسالتنا </label>
                    <input class="form-control"  name="settings[about_us_messege_photo]"  rows="3"
                           placeholder="صورة رسالتنا" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['about_us_messege_photo'] )}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> قيمنا بالعربي</label>
                    <textarea class="form-control" id="rate_us_ar" name="settings[about_us_rate_ar]"  rows="3"
                              placeholder="">{!!$setting['about_us_rate_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_rate_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea"> قيمنا بالانجليزي</label>
                    <textarea class="form-control" id="rate_us_en" name="settings[about_us_rate_en]"  rows="3"
                              placeholder="">{!!  $setting['about_us_rate_en'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "about_us_rate_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره قيمنا </label>
                    <input class="form-control"  name="settings[about_us_rate_photo]"  rows="3"
                           placeholder="صورة قيمنا" type="file" >
                    <img id="output-account-upload"
                         src=""
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>


         <!-- End About Us  -->

         <!--  Join Us -->

            <div class=" col-12">
                <div class="form-label-group form-group">
                    <textarea class="form-control" name="settings[join_us_name_ar]"  id="editor_joinus_name_ar">{{ $setting['join_us_name_ar'] ?? ''}}</textarea>
                    <label for="editor_joinus_ar">عنوان انضم الينا بالعربي</label>
                    @include('admin.includes.alerts.input-errors', ['input' => "join_us_name_ar"])
                </div>
            </div>

            <div class=" col-12">
                <div class="form-label-group form-group">
                    <textarea class="form-control" name="settings[join_us_name_en]"  id="editor_joinus_name_ar">{{ $setting['join_us_name_en'] ?? ''}}</textarea>
                    <label for="editor_joinus_ar">عنوان انضم الينا بالانجليزي</label>
                    @include('admin.includes.alerts.input-errors', ['input' => "join_us_name_en"])
                </div>
            </div>

            <div class=" col-12">
                <div class="form-label-group form-group">
                    <textarea class="form-control" name="settings[join_us_description_ar]"  id="editor_joinus_ar">{{ $setting['join_us_description_ar'] ?? ''}}</textarea>
                    <label for="editor_joinus_ar"> وصف انضم الينا بالعربي</label>
                    @include('admin.includes.alerts.input-errors', ['input' => "join_us_description_ar"])
                </div>
            </div>

            <div class=" col-12">
                <div class="form-label-group form-group">
                    <textarea class="form-control" name="settings[join_us_description_en]"  id="editor_joinus_en">{{ $setting['join_us_description_en'] ?? ''}}</textarea>
                    <label for="editor_joinus_en">وصف انضم الينا بالانجليزي</label>
                    @include('admin.includes.alerts.input-errors', ['input' => "join_us_description_en"])
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره انضم الينا الاولى </label>
                    <input class="form-control"  name="settings[join_us_photo1]"  rows="3"
                           placeholder="صورة  انضم الينا 1" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['join_us_photo1']) }}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره انضم الينا الثانيه </label>
                    <input class="form-control"  name="settings[join_us_photo2]"  rows="3"
                           placeholder=" صورة  انضم الينا 2" type="file" >
                    <img id="output-account-upload"
                         src="{{ asset('assets/uploads/settings/' . $setting['join_us_photo2'])}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره انضم الينا الثالثه </label>
                    <input class="form-control"  name="settings[join_us_photo3]"  rows="3"
                           placeholder="صور انضم الينا3" type="file" >
                    <img id="output-account-upload"
                         src="{{asset('assets/uploads/settings/' . $setting['join_us_photo3'])}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  صوره انضم الينا الرابعه </label>
                    <input class="form-control"  name="settings[join_us_photo4]"  rows="3"
                           placeholder="صور انضم الينا4 " type="file" >
                    <img id="output-account-upload"
                         src="{{ asset('assets/uploads/settings/' . $setting['join_us_photo4'])}}"
                         class="rounded mr-75" alt="profile image" height="64" width="64">
                </div>
            </div>

            <!-- End Join Us -->

            <!-- Start Privacy -->
            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  عنوان صفحة سياسيه الخصوصيه </label>
                    <textarea class="form-control"  name="settings[privacy_title]" id="privacy_title"  rows="3"
                           placeholder="عنوان صفحة سياسيه الخصوصيه " type="file" > {!! $setting['privacy_title'] !!} </textarea>
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">  موضوع صفحة سياسيه الخصوصيه </label>
                    <textarea class="form-control"  name="settings[privacy_content]" id="privacy_content"  rows="3"
                           placeholder="عنوان صفحة سياسيه الخصوصيه " type="file" >{!! $setting['privacy_content'] !!}</textarea>
                </div>
            </div>

            <!-- End Privacy -->
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                    حفظ التغييرات
                </button>
            </div>
        </div>
    </form>
</div>
