<div class="tab-pane fade" id="smtp-package" role="tabpanel"
     aria-labelledby="account-pill-connections" aria-expanded="false">
    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('admin.settings.update_smtp')}}" method="post">
                @csrf

                <div class="form-group">
                    <h5 class="panel-title">SMTP</h5>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input placeholder="اسم المستخدم" type="text" class="form-control" name="username"
                                   value="{{ ($smtp) ? $smtp->username : ''}}">
                            <label for="account-name">اسم المستخدم </label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'username'])
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input placeholder="الرقم السري" name="password" type="password" class="form-control"
                                   value="{{ ($smtp) ? $smtp->password : ''}}">
                            <label for="account-e-mail">الرقم السري</label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'password'])
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="email" class="form-control" name="sender_email"
                                   placeholder="الايميل المرسل" value="{{ ($smtp)?$smtp->sender_email:''}}">
                            <label for="account-name">الايميل المرسل </label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'sender_email'])
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="sender_name"
                                   value="{{($smtp)?$smtp->sender_name:''}}" placeholder="الاسم المرسل">
                            <label for="account-name">الاسم المرسل</label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'sender_name'])
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="integer" class="form-control" name="port"
                                   value="{{ ($smtp)?$smtp->port:''}}" placeholder="البورت">
                            <label for="account-name">البورت</label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'port'])
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="host"
                                   value="{{($smtp)?$smtp->host:''}}" placeholder="الهوست">
                            <label for="account-name">الهوست</label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'host'])
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="text" class="form-control" name="encryption"
                                   value="{{($smtp)?$smtp->encryption:''}}" placeholder="التشفير">
                            <label for="account-name">التشفير</label>

                        </div>

                        @include('admin.includes.alerts.input-errors', ['input' => 'encryption'])
                    </div>

                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                            حفظ التعديلات
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
