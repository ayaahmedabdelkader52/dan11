<div class="tab-pane fade " id="change-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">

    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.settings.update_password')}}" method="post">
                @csrf
                <div class="row">

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="password" class="form-control" name="old_password" placeholder="كلمة سر القديمة" >
                            <label for="account-old-password">كلمة سر القديمة</label>

                            @include('admin.includes.alerts.input-errors', ['input' => "old_password"])
                        </div>
                    </div>



                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="password" name="password" class="form-control"  placeholder="كلمة السر الجديدة" >
                            <label for="account-new-password">كلمة السر الجديدة</label>

                            @include('admin.includes.alerts.input-errors', ['input' => "password"])
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-label-group form-group">
                            <input type="password" name="password_confirmation"  class="form-control"  placeholder="أعد كتابة كلمة السر الجديدة" >
                            <label for="account-retype-new-password">أعد كتابة كلمة السر الجديدة</label>

                            @include('admin.includes.alerts.input-errors', ['input' => "password_confirmation"])
                        </div>
                    </div>

                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                            حفظ التغييرات
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
