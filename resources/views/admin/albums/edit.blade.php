@extends('admin.master')
@section('title','تعديل البوم الصور')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">البوم الصور</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.albums.index') }}">البوم الصور</a></li>
                            <li class="breadcrumb-item active">تعديل البوم الصور</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <form class="form" action="{{ route('admin.albums.update' , $album) }}" method="POST" >
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="name_ar"
                                       placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus value="{{ $album -> name_ar }}">
                                <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="en-name" class="form-control" name="name_en"
                                       placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus value="{{$album -> name_en }}">
                                <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                            </div>
                        </div>


                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <input type="text" id="price" class="form-control" name="price"
                                           placeholder="{{ __('dashboard.main.price') }}" autofocus value="{{$album -> price}}" >
                                    <label for="price">{{ __('dashboard.main.price') }}</label>
                                </div>
                            </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="photo">{{ __('dashboard.main.photo' ) }}</label>
                                <input type="file" id="photo" class="form-control" name="photo"
                                       placeholder="{{ __('dashboard.main.photo' ) }}" autofocus>

                                @include('admin.includes.alerts.input-errors', ['input' => "photo"])
                                <div class="form-label-group">
                                    <div class="multi-img-result">
                                        <div class="img-uploaded">
                                            <img src="{{ $album ->photoPath }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="projectinput2"> أختر القسم </label>
                                    <select name="category_id" class="select2 form-control">
                                        <optgroup label="من فضلك أختر القسم ">
                                            @if($categories && $categories -> count() > 0)
                                                @foreach($categories as $category)
                                                    <option
                                                        value="{{$category -> id }}" @if($album -> category_id == $category -> id)selected @endif >{{$category -> name_ar}}</option>
                                                @endforeach
                                            @endif
                                        </optgroup>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6 col-12">
                                <div class="form-label-group form-group">
                                    <label for="projectinput2"> أختر القسم الفرعي </label>
                                    <select name="category_id" class="select2 form-control">
                                        <optgroup label=" أختر القسم الفرعي">
                                            @if($subcategories && $subcategories -> count() > 0)
                                                @foreach($subcategories as $subcategory)
                                                    <option
                                                        value="{{$subcategory -> id }}" @if($album -> subcategory_id == $subcategory -> id)selected @endif >{{$subcategory -> name_ar}}</option>
                                                @endforeach
                                            @endif
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                        @if($album -> offer == 1)

                            <div class="col-12">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                     <span class="vs-checkbox-con vs-checkbox-primary">
                                         <input name="offer" class="checkbox-input checkk" value="{{$album -> offer}}" type="checkbox">
                                           <span class="vs-checkbox">
                                           <span class="vs-checkbox--check">
                                            <i class="vs-icon feather icon-check"></i>
                                           </span>
                                           </span>
                                        <span class=""> تخفيض </span>
                                     </span>
                                </li>
                            </div>
                            <div class="col-md-6 col-12 offer"  >
                                <div class="form-group">
                                    <input type="text" id="price_after" class="form-control" name="price_after"
                                           placeholder="{{ __('dashboard.main.price_after') }}" autofocus value="{{$album -> price_after}}">
                                    <label for="price_after">{{ __('dashboard.main.price_after') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "price_after"])
                                </div>
                            </div>
                            <div class="col-md-6 col-12 offer"  >
                                <div class="form-group">
                                    <input type="text" id="price_before" class="form-control" name="price_before"
                                           placeholder="{{ __('dashboard.main.price_before') }}" autofocus value="{{$album -> price_before}}" >
                                    <label for="price_before">{{ __('dashboard.main.price_before') }}</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "price_before"])
                                </div>
                            </div>

                            <div class="col-md-6 col-12 offer" >
                                <div class="form-group">
                                    <input type="date" id="start" class="form-control" name="start"
                                           placeholder="{{ __('dashboard.main.start') }}" autofocus value="{{$album -> start}}">
                                    <label for="start"> بداية العرض </label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "start"])
                                </div>
                            </div>
                            <div class="col-md-6 col-12 offer"  >
                                <div class="form-group">
                                    <input type="date" id="end" class="form-control" name="end" autofocus value="{{$album -> end}}">
                                    <label for="end">نهاية العرض</label>
                                    @include('admin.includes.alerts.input-errors', ['input' => "end"])
                                </div>
                            </div>


                        @endif


                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.edit') }}</button>
                            </div>
                    </div>
                </div>
            </div>
        </form>
@endsection
 @section('scripts')
            <script>

                $('.checkk').on("change" , function (){
                    this.value = this.checked ? 1 : 0;
                    if($(this).is(':checked') )
                        $('.offer').show()
                    else
                        $('.offer').hide()
                })


            </script>
@endsection
