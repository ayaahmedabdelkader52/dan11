@extends('admin.master')
@section('title', ' البوم الصور')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">البوم الصور</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.albums.index') }}">البوم الصور </a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.albums.show' , $album -> id ) }}">{{ $album->name_ar }}</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="content-body">
            <!-- page users view start -->
            <section class="page-users-view">
                <div class="row">
                    <!-- account start -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">عرض الالبوم </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                        <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                            <input type="text" id="ar-name" class="form-control" name="name_ar"
                                                   placeholder="{{ __('dashboard.main.name_in_ar') }}" disabled value="{{$album -> name_ar}}">
                                        </div>
                                        <div class="col-md-6 col-12">
                                        <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                            <input type="text" id="en-name" class="form-control" name="name_en"
                                                   placeholder="{{ __('dashboard.main.name_in_en') }}" disabled value="{{$album -> name_en}}">
                                        </div>

                                        <div class="col-md-6 col-12">
                                        <label for="price">{{ __('dashboard.main.price') }}</label>
                                        <input type="text" id="price" class="form-control" name="price"
                                               placeholder="{{ __('dashboard.main.price') }}" disabled value="{{$album -> price }}" >
                                        </div>

                                        @if($album -> offer == 1)
                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.price_before') }}</label>
                                                <input type="text" id="price" class="form-control" name="price_before"
                                                       placeholder="{{ __('dashboard.main.price_before') }}" disabled value="{{$album -> price_before }}" >
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.price_after') }}</label>
                                                <input type="text" id="price" class="form-control" name="price_after"
                                                       placeholder="{{ __('dashboard.main.price_after') }}" disabled value="{{$album -> price_after }}" >
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.start') }}</label>
                                                <input type="date" id="price" class="form-control" name="price"
                                                       placeholder="{{ __('dashboard.main.start') }}" disabled value="{{$album -> start }}" >
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label for="price">{{ __('dashboard.main.end') }}</label>
                                                <input type="date" id="price" class="form-control" name="price"
                                                       placeholder="{{ __('dashboard.main.end') }}" disabled value="{{$album -> end }}" >
                                            </div>
                                        @endif

                                        <div class="col-md-6 col-12">
                                        <label for="category_id">القسم الرئيسي</label>
                                        <input type="text" id="category_id" class="form-control" name="category_id"
                                               placeholder="القسم" disabled value="{{$album -> category -> name_ar }}" >
                                        </div>

                                        <div class="col-md-6 col-12">
                                        <label for="category_id">القسم الفرعي </label>
                                        <input type="text" id="subcategory_id" class="form-control" name="category_id"
                                               placeholder="القسم" disabled value="{{$album -> subcategory -> name_ar }}" >
                                        </div>

                                    </div>

                                    <div class="row">
                                        @foreach($photos as $photo)
                                            <div class="card col-md-6 col-12 " style="width: 18rem;">
                                                <img src="{{asset($photo -> photoPath )}}" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                    <p class="card-text">{{$photo -> name_ar}}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>



    </div>
@endsection

