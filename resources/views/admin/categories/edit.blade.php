@extends('admin.master')
@section('title','تعديل قسم')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاقسام</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.categories.index') }}">الاقسام</a></li>
                            <li class="breadcrumb-item active">تعديل قسم</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- START: Content-->
        <form class="form" action="{{ route('admin.categories.update' , $category) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="name_ar"
                                       placeholder="{{ __('dashboard.main.name_in_ar') }}"  value="{{ $category -> name_ar }}">
                                <label for="ar-name">{{ __('dashboard.main.name_in_ar') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="en-name" class="form-control" name="name_en"
                                       placeholder="{{ __('dashboard.main.name_in_en') }}"  value="{{ $category -> name_en }}">
                                <label for="en-name">{{ __('dashboard.main.name_in_en') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                            </div>
                        </div>


                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="ar-description">{{ __('dashboard.main.description_in_ar' ) }}</label>
                                <input type="text" id="ar-description" class="form-control" name="description_ar"
                                       placeholder="{{ __('dashboard.main.description_in_ar' ) }}" autofocus value="{{$product -> description_ar  }}">

                                @include('admin.includes.alerts.input-errors', ['input' => "description_ar"])
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="en-description">{{ __('dashboard.main.description_in_en' ) }}</label>
                                <input type="text" id="en-description" class="form-control" name="description_en"
                                       placeholder="{{ __('dashboard.main.description_in_en' ) }}" autofocus value="{{  $product -> description_en }}">

                                @include('admin.includes.alerts.input-errors', ['input' => "description_en"])
                            </div>
                        </div>

                              <!-- photo -->
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="file" id="image" class="form-control" name="photo" placeholder="{{ __('dashboard.main.cat_photo') }}">
                                <label for="email-id-column">{{ __('dashboard.main.cat_photo') }}</label>

                                <div class="form-label-group">
                                    <div class="multi-img-result">
                                        <div class="img-uploaded">
                                            <img src="{{ $category->photoPath }}" alt="">
                                        </div>
                                    </div>
                                </div>

                                @include('admin.includes.alerts.input-errors', ['input' => 'photo'])
                            </div>
                        </div>


                        <div class="col-12">
                            <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.edit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
        @endsection
