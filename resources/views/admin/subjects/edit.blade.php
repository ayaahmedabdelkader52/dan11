@extends('admin.master')
@section('title','تعديل المناسبات ')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">المناسبات </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.subjects.index') }}">المناسبات</a></li>
                            <li class="breadcrumb-item active">تعديل المناسبات </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <form class="form" action="{{ route('admin.subjects.update' , $subject) }}" method="POST" >
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="name_ar"
                                       placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus value="{{ $subject -> name_ar }}">
                                <label for="ar-name">{{ __('dashboard.main.name_in_ar' ) }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_ar"])
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="en-name" class="form-control" name="name_en"
                                       placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus value="{{$subject -> name_en }}">
                                <label for="en-name">{{ __('dashboard.main.name_in_en' ) }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "name_en"])
                            </div>
                        </div>


                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="projectinput2"> أختر hالزفه </label>
                                <select name="product_id" class="select2 form-control">
                                    <optgroup label="أختر الزفة ">
                                        @if($products -> count() > 0)
                                            @foreach($products as $product)
                                                <option
                                                    value="{{$product -> id }}" @if($subject -> product_id == $product -> id)selected @endif >{{$product -> name_ar}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.edit') }}</button>
                            </div>
                    </div>
                </div>
            </div>
        </form>
@endsection
