@extends('admin.master')

@section('title', 'الصلاحيات')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الصلاحيات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">عرض  صلاحية</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->

    <!-- page permissions view start -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="sidebar-detached sidebar-left">
                    <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                        <table>
                            <tr>
                                <td class="font-weight-bold"> <h3>الاسم : </h3></td>
                                <td><h3>{{ $role->name }}</h3></td>
                            </tr>
                        </table>
                    </div>
                    <div class="row">
                        <!-- Permissions -->
                        @foreach($superPermissions as $key => $permission)
                            <div class="col col-md-4 col-sm-6 md-3 roll-checkk">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="brands">
                                            <a class="filter-title mb-0 select-all-permissions">{{ $permission['title'] }}</a>
                                            <div class="brand-list" id="brands">
                                                <ul class="list-unstyled">
                                                    @foreach($permission['childrens'] as $child)
                                                        <li class="d-flex justify-content-between align-items-center py-25">
                                        <span class="vs-checkbox-con vs-checkbox-primary">
                                            <input name="permissions[]" value="{{ $child }}" disabled class="checkbox-input"
                                                   @if(in_array($child, $roleUserPermissions)) checked @endif
                                                   type="checkbox">
                                            <span class="vs-checkbox">
                                                <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                </span>
                                            </span>
                                            <span class="">{{ __('routes.' . $child ) }}</span>
                                        </span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                    <!-- /Permissions -->
                    </div>
                </div>

                <div class="card-footer">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.edit') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page permissions view end -->

@endsection
