@extends('admin.master')

@section('title', 'إضافة صلاحية')

@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الصلاحيات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.permissions.index') }}">الصلاحيات</a></li>
                            <li class="breadcrumb-item active">اضافة صلاحية</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>

    <!-- START: Content-->
    <form class="form" action="{{ route('admin.permissions.store') }}" method="POST" >
    @csrf
    <!-- You can all alert messages by removing the comment -->
        {{--            @include('admin.includes.alerts.all-errors')--}}

        <div class="card">
            <div class="card-body">
                <div class="row">
                    @foreach(config('app.languages') as $key => $language)
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="{{$key}}-name" class="form-control" name="name[{{$key}}]"
                                       placeholder="{{ __('dashboard.main.name_in_' . $key) }}" autofocus value="{{ old('name.'. $key) }}">
                                <label for="{{$key}}-name">{{ __('dashboard.main.name_in_' . $key) }}</label>

                                @include('admin.includes.alerts.input-errors', ['input' => "name.$key"])
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="sidebar-detached sidebar-left">
                <div class="row">
                    <!-- Permissions -->
                    @foreach($superPermissions as $key => $permission)
                        <div class="col col-md-4 col-sm-6 md-3 roll-checkk">
                            <div class="card">
                                <div class="card-body">
                                    <div class="brands">
                                        <a class="filter-title mb-0 select-all-permissions">{{ $permission['title'] }}</a>
                                        <div class="brand-list" id="brands">
                                            <ul class="list-unstyled">
                                                <li class="d-flex justify-content-between align-items-center py-25">
                                                    <span style="border-bottom: 1px solid #ddd;padding-bottom: 10px;" class="vs-checkbox-con vs-checkbox-primary">
                                                        <input class="checkbox-input check-all" type="checkbox">
                                                        <span class="vs-checkbox">
                                                            <span class="vs-checkbox--check">
                                                                <i class="vs-icon feather icon-check"></i>
                                                            </span>
                                                        </span>
                                                        <span class="">تحديد الكل</span>
                                                    </span>
                                                </li>
                                                @foreach($permission['childrens'] as $child)

                                                    <li class="d-flex justify-content-between align-items-center py-25">
                                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                                        <input name="permissions[]" value="{{ $child }}" class="checkbox-input checkk" type="checkbox">
                                                        <span class="vs-checkbox">
                                                            <span class="vs-checkbox--check">
                                                                <i class="vs-icon feather icon-check"></i>
                                                            </span>
                                                        </span>
                                                        <span class="">{{ __('routes.' . $child ) }}</span>
                                                    </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                @endforeach
                <!-- /Permissions -->
                </div>
            </div>

            <div class="card-footer">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                </div>
            </div>
        </div>

    </form>
    <!-- END: Content-->
@endsection
@section('scripts')
    <script>
        $(".check-all").on("change" , function(){
            if($(this).is(':checked') )
                $(this).parents(".roll-checkk").find(".checkk").attr("checked" , true);
            else
                $(this).parents(".roll-checkk").find(".checkk").attr("checked" , false);
        })
    </script>
@endsection
