@extends('admin.master')
@section('title','تعديل كود خصم ')
@section('content')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">اكواد خصم </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.coupons.index') }}"> اكواد الخصم </a></li>
                            <li class="breadcrumb-item active">تعديل كود خصم </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Content-->
        <form class="form" action="{{ route('admin.coupons.update' , $coupon) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="code"
                                       placeholder="{{ __('dashboard.main.coupon') }}"  value="{{ $coupon -> code }}">
                                <label for="ar-name">{{ __('dashboard.main.coupon') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "coupon"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <label for="type"> أختر نوع الخصم  </label>
                                <select name="type" id="type" class="select2 form-control">
                                    <optgroup label=" أختر نوع الخصم  ">

                                        <option value="percent" @if($coupon -> type == 'percent')selected @endif> %العرض </option>
                                        <option value="fixed" @if($coupon -> type == 'fixed')selected @endif> نقدي </option>

                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="value"
                                       placeholder="{{ __('dashboard.main.value') }}"  value="{{ $coupon->value }}">
                                <label for="ar-name">{{ __('dashboard.main.value') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "value"])
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-label-group form-group">
                                <input type="text" id="ar-name" class="form-control" name="cart_value"
                                       placeholder="{{ __('dashboard.main.cart') }}"  value="{{ $coupon -> cart_value }}">
                                <label for="ar-name">{{ __('dashboard.main.cart') }}</label>
                                @include('admin.includes.alerts.input-errors', ['input' => "cart_value"])
                            </div>
                        </div>



                        <div class="col-12">
                            <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
        @endsection
