@extends('admin.master')
@section('title', 'عرض شكوي')

@section('content')

    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الشكاوي</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.complains.index') }}">الشكاوي</a></li>
                            <li class="breadcrumb-item active">عرض شكوي</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <div class="app-content-overlay"></div>
        <div class="email-app-area">
            <!-- Detailed Email View -->
            <div class="">
                <div class="row">
                    <div class="col-12">
                        <div class="card px-1">
                            <div class="card-header email-detail-head ml-75">
                                <div class="user-details d-flex justify-content-between align-items-center flex-wrap">
                                    <div class="avatar mr-75">
                                        <img src="{{ ($message->user ) ? asset('assets/uploads/users/' . $message->user->avatar) : asset('assets/uploads/users/default.png') }}" alt="avtar" width="61" height="61">
                                    </div>
                                    <div class="mail-items">
                                        <h4 class="list-group-item-heading mb-0">{{ $message->subject }}</h4>

                                        <span class="font-small-3">{{ ($message->user) ? $message->user->email : '' }}</span>
                                    </div>
                                </div>
                                <div class="mail-meta-item">
                                    <div class="mail-time mb-1">{{ $message->created_at->format('H:i') }}</div>
                                    <div class="mail-date">{{ $message->created_at->toDateString() }}</div>
                                </div>
                            </div>
                            <div class="card-body mail-message-wrapper pt-2 mb-0">
                                <div class="mail-message">
                                    <p>{{ $message->text }}</p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="" data-toggle="modal" data-target="#messageModal" class="send-message btn btn-primary"
                                   data-user-phone="{{ $message->phone }}" data-user-id="{{ $message->user_id }}"
                                   title="{{ __('dashboard.action.reply_message') }}" >رد</a>

                                <select class="btn btn-success complain-status" name="complain_status">
                                    <option value="open" @if($message->status == 'open') selected @endif >{{ __('dashboard.complaints.open') }}</option>
                                    <option value="in_progress" @if($message->status == 'in_progress') selected @endif>{{ __('dashboard.complaints.in_progress') }}</option>
                                    <option value="finished" @if($message->status == 'finished') selected @endif>{{ __('dashboard.complaints.finished') }}</option>
                                    <option value="closed" @if($message->status == 'closed') selected @endif>{{ __('dashboard.complaints.closed') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Detailed Email View -->
        </div>

    </div>
    @include('admin.contact.includes.message-to-single-user')

@endsection
@section('scripts')
    <script>
        $('.send-message').on('click', function() {
            var userEmail = $(this).data('user-email'),
                userId = $(this).data('user-id'),
                userPhone = $(this).data('user-phone');

            // console.log('hello');
            console.log(userEmail);
            console.log(userPhone);

            $('.user-phone').val(userPhone);
            $('.user-email').val(userEmail);
            $('.user-id').val(userId);
        });

        // Change Complain Status
        $('.complain-status').change(function() {
            var $option = $(this).find('option:selected'),
                value = $option.val(),
                text = $option.text();

            Swal.fire({
                title:  'هل تود تغير حالة الشكوي الي  "' + text + '"',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('admin.complains.change_status', $message) }}",
                        method: 'POST',
                        data: {
                            status: value,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function(response) {
                            fireSuccess(response.msg)
                        }
                    });
                }
            })
        });

    </script>
@stop
