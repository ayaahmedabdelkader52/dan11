@extends('admin.master')
@section('title')
    الشكاوي والمقترحات
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الشكاوي والمقترحات</h2>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>

                    <div class="dropdown-menu">
                        <a class="dropdown-item action-delete-selected" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الموضوع</th>
                    <th>الرساله</th>
{{--                    <th>الطلب</th>--}}
                    <th>التاريخ</th>
                    <th>التحكم</th>
                </tr>
                </thead>
                <tbody>
                @foreach($complains as $complain )
                    <tr id="complain-{{$complain->id}}">
                        <td data-complain-id="{{ $complain->id }}"></td>
                        <td>{{$complain->user ? $complain->user->name : ''}}</td>
                        <td> {{$complain->phone}} </td>
                        <td> {{$complain->subject}} </td>
                        <td> {{ Str::limit($complain->text, 20) }} </td>
                        <td> {{ $complain->created_at }} </td>
                        <td>
                            <span class="action-delete" data-complain-id="{{ $complain->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>

                            <span data-toggle="modal" data-target="#messageModal" class="send-message" data-user-phone="{{ $complain->phone }}"
                                  data-user-email="{{ $complain->email }}" data-user-id="{{ $complain->user_id }}"
                                  title="{{ __('dashboard.action.reply_message') }}">
                                <i class="feather icon-send"></i>
                            </span>

                            <a href="{{ route('admin.complains.show', $complain) }}">
                                <span title="عرض">
                                    <i class="feather icon-eye"></i>
                                </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->

    <!-- Message starts -->
    @include('admin.contact.includes.message-to-single-user')
    <!-- Message ends -->

@stop
@section('scripts')
    <script>

        $('.action-delete').on('click', function () {
            var complainId = $(this).data('complain-id'),
                url = '{{ route("admin.complains.destroy", ":id") }}',
                newUrl = url.replace(':id', complainId);

                console.log(complainId);


            Swal.fire({
                title: 'هل تريد حذف الشكوي',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            $('#complain-' + complainId).remove();
                        }
                    });
                }
            })
        });

        $('.action-delete-selected').on('click', function () {
            var selectedMessages = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedMessages.push( $(this).parent().data('complain-id') );
                // console.log($(this).parent().data('user-id') )
            });
            console.log(selectedMessages);
            // console.log(selectedMessages.length);
            if(selectedMessages.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.alerts.do_you_want_to_delete_selected_data') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.complains.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                messages: selectedMessages
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedMessages.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#complain-' + element).remove();
                                } );

                                // console.log(selectedMessages)
                                // console.log(response.data.userId)
                            }
                        });
                    }
                })

            }

        });

        $('.send-message').on('click', function() {
            var userEmail = $(this).data('user-email'),
                userId = $(this).data('user-id'),
                userPhone = $(this).data('user-phone');

            // console.log('hello');
            console.log(userEmail);
            console.log(userPhone);
            console.log(userId);

            $('.user-phone').val(userPhone);
            $('.user-email').val(userEmail);
            $('.user-id').val(userId);
        });
    </script>
@stop
