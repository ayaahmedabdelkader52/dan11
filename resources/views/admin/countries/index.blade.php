@extends('admin.master')

@section('title', 'قائمة الدول')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الدول</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الدول</a></li>
                            <li class="breadcrumb-item active">كل الدول</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('admin.countries.create') }}"><i class="feather icon-archive"></i>{{ __('dashboard.action.add') }}</a>
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        <a class="dropdown-item action-download" href="{{ route('admin.countries.downloadCountries') }}" ><i class="feather icon-file"></i>{{ __('dashboard.action.download_excel') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>الصورة</th>
                    <th>{{ __('dashboard.main.name_in_ar') }}</th>
                    <th>{{ __('dashboard.main.name_in_en') }}</th>
                    <th>{{ __('dashboard.main.status') }}</th>
                    <th>{{ __('dashboard.main.Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($countries as $country)
                    <tr id="country-{{$country->id}}">
                        <td data-country-id="{{ $country->id }}"></td>
                        <td>
                            <img width="50" src="{{ $country->flagPath }}" alt="">
                        </td>
                        <td>{{ $country->getTranslation('name', 'ar') }}</td>
                        <td>{{ $country->getTranslation('name', 'en') }}</td>

                        <td>
                            <div class="chip @if($country->active == 1) chip-success @else chip-primary @endif">
                                <div class="chip-body">
                                    <div class="chip-text">{{ __('dashboard.main.' . $country->active) }}</div>
                                </div>
                            </div>
                        </td>

                        <td class="product-action">
                            {{--                        <a href="#" class="action-edit">--}}
                            <a href="{{ route('admin.countries.edit', $country) }}">
                                <span data-country-id="{{ $country->id }}" data-name="{{ $country->name }}"
                                      title="{{ __('dashboard.action.edit') }}" >
                                    <i class="feather icon-edit"></i>
                                </span>
                            </a>
                            <span class="action-delete" data-country-id="{{ $country->id }}" title="{{ __('dashboard.action.delete') }}">
                                <i class="feather icon-trash"></i>
                            </span>

                            <a href="{{ route('admin.countries.show', $country) }}">
                                <span title="{{ __('dashboard.action.show') }}">
                                    <i class="feather icon-eye"></i>
                                </span>
                            </a>

                            <a href="{{ route('admin.countries.regions', $country) }}">
                                <span title="المناطق">
                                    <i class="fa fa-map-signs"></i>
                                </span>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->

    </section>
    <!-- Data list view end -->

@endsection

@section('scripts')
    <script>
        var selectedCountries = [];

        // On Edit
        $('.action-edit').on("click",function(e){
            e.stopPropagation();
            var name = $(this).data('name'),
                countryId = $(this).data('country-id'),
                url = '{{ route("admin.countries.update", ":id") }}',
                newUrl = url.replace(':id', countryId);


            $('#edit-user-form').attr('action', newUrl);

            $('#data-name').val(name);
            // $('#data-price').val(price);
            $(".add-new-data").addClass("show");
            $(".overlay-bg").addClass("show");
        });

        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {
            var countryId = $(this).data('country-id'),
                url = '{{ route("admin.countries.destroy", ":id") }}',
                newUrl = url.replace(':id', countryId);

            console.log( countryId);
            console.log( newUrl);

            Swal.fire({
                title: 'هل تود حذف هذه الدولة',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })

                            $('#country-' + countryId).remove();
                        }
                    });
                }
            })
        });

        $('.delete-all').click(function () {
            selectedCountries = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedCountries.push( $(this).parent().data('country-id') );
                // console.log($(this).parent().data('country-id') )
            });
            console.log(selectedCountries);
            // console.log(selectedCountries.length);
            if(selectedCountries.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.alerts.do_you_want_to_delete_selected_data') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.countries.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                countries: selectedCountries
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedCountries.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#country-' + element).remove();
                                } );
                            }
                        });
                    }
                })

            }

        });

    </script>
@endsection
