<div>
    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="{{ route('admin.admin') }}">
                        <img class="brand-logo" alt="Logo" src="{{ (isset($setting['logo'])) ? asset('assets/uploads/settings/' . $setting['logo']) : asset('Admin/app-assets/images/ico/logo.svg') }}">
                        <h2 class="brand-text">{{ (isset($setting['site_name'])) ? $setting['site_name'] : 'اوامر' }}</h2>
                    </a>
                </li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                <!-- start home page -->
                @hasPermission('admin.admin')
                <li class=" nav-item  @if(\Request::route()->getName() == 'admin.admin') active @endif">
                    <a href="{{route('admin.admin')}}"><i class="feather icon-home"></i>
                        <span class="menu-title" data-i18n="Dashboard"> الصفحه الرئيسيه</span>
                        <span class="badge badge badge-warning badge-pill float-right mr-2"></span>
                    </a>
                </li>
                @endhasPermission
                <!-- end home page -->

                @hasPermission(['admin.users.index', 'admin.users.blocked', 'admin.users.active', 'admin.users.inactive', 'admin.users.male', 'admin.users.female' ])
                    <li class=" nav-item">
                        <a href="#">
                            <i class="feather icon-user"></i>
                            <span class="menu-title" data-i18n="User">المستخدمين</span>
                        </a>
                        <ul class="menu-content">

                            @hasPermission('admin.users.index')
                            <li class="@if(\Request::is('dashboard/users*')&&
                                            !\Request::is('dashboard/users/blocked*') &&
                                            !\Request::is('dashboard/users/active*') &&
                                            !\Request::is('dashboard/users/inactive*') &&
                                            !\Request::is('dashboard/users/male*') &&
                                            !\Request::is('dashboard/users/female*')
                                            ) active @endif">
                                <a href="{{ route('admin.users.index') }}">
                                    <i class="feather icon-list"></i>
                                    <span class="menu-item" data-i18n="List">الكل </span>
                                </a>
                            </li>
                            @endhasPermission

                            @hasPermission('admin.users.blocked')
                            <li class="@if(\Request::is('dashboard/users/blocked*') ) active @endif">
                                <a href="{{ route('admin.users.blocked') }}">
                                    <i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="View">المحظورين</span>
                                </a>
                            </li>
                            @endhasPermission

                            @hasPermission('admin.users.active')
                            <li class="@if(\Request::route()->getName() == 'admin.users.active') active @endif">
                                <a href="{{ route('admin.users.active') }}">
                                    <i class="feather icon-user-check"></i>
                                    <span class="menu-item" data-i18n="View">النشطين</span>
                                </a>
                            </li>
                            @endhasPermission

                            @hasPermission('admin.users.inactive')
                            <li class="@if(\Request::route()->getName() == 'admin.users.inactive') active @endif">
                                <a href="{{ route('admin.users.inactive') }}">
                                    <i class="feather icon-user-minus"></i>
                                    <span class="menu-item" data-i18n="View">الغير نشطين</span>
                                </a>
                            </li>
                            @endhasPermission

                            @hasPermission('admin.users.male')
                            <li class="@if(\Request::route()->getName() == 'admin.users.male') active @endif">
                                <a href="{{ route('admin.users.male') }}">
                                    <i class="feather icon-mic"></i>
                                    <span class="menu-item" data-i18n="View">الذكور</span>
                                </a>
                            </li>
                            @endhasPermission

                            @hasPermission('admin.users.female')
                            <li class="@if(\Request::route()->getName() == 'admin.users.female') active @endif">
                                <a href="{{ route('admin.users.female') }}">
                                    <i class="feather icon-mic-off"></i>
                                    <span class="menu-item" data-i18n="View">الاناث</span>
                                </a>
                            </li>
                            @endhasPermission
                        </ul>
                    </li>

                @endhasPermission
                <!-- start Users -->

                <!-- End Users -->


                <!-- start Permissions -->
                @hasPermission('admin.permissions.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.permissions.index') active @endif">
                    <a href="{{route('admin.permissions.index')}}">
                        <i class="feather icon-user-check"></i>
                        <span class="menu-title" data-i18n="Account Settings">الصلاحيات</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Permissions -->

                <!-- start Rule -->
                @hasPermission('admin.rules.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.rules.index') active @endif ">
                    <a href="{{route('admin.rules.index')}}">
                        <i class="feather icon-box"></i>
                        <span class="menu-title" data-i18n="Email"> الاحكام والشروط </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Rule -->

                <!-- start Question -->
                @hasPermission('admin.questions.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.questions.index') active @endif ">
                    <a href="{{route('admin.questions.index')}}">
                        <i class="feather icon-question-mark "></i>
                        <span class="menu-title" data-i18n="Email"> الاسئله </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Question -->

                <!-- start Coupons -->
                @hasPermission('admin.coupons.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.coupons.index') active @endif ">
                    <a href="{{route('admin.coupons.index')}}">
                        <i class="feather icon-dollar-sign"></i>
                        <span class="menu-title" data-i18n="Email">اكواد الخصم </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Coupons -->

                <!-- start Category -->
                @hasPermission('admin.orders.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.orders.index') active @endif ">
                    <a href="{{route('admin.orders.index')}}">
                        <i class="feather icon-shopping-bag"></i>
                        <span class="menu-title" data-i18n="Email">الطلبات </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Category -->

                <!-- start Category -->
                @hasPermission('admin.categories.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.categories.index') active @endif ">
                    <a href="{{route('admin.categories.index')}}">
                        <i class="feather icon-box"></i>
                        <span class="menu-title" data-i18n="Email">الاقسام الرئيسيه </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Category -->

                <!-- start Category -->
                @hasPermission('admin.offers.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.offers.index') active @endif ">
                    <a href="{{route('admin.offers.index')}}">
                        <i class="feather icon-dollar-sign"></i>
                        <span class="menu-title" data-i18n="Email">التخفيضات </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Category -->

                <!-- start  subcategories -->
                @hasPermission('admin.subcategories.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.subcategories.index') active @endif ">
                    <a href="{{route('admin.subcategories.index')}}">
                        <i class="feather icon-package"></i>
                        <span class="menu-title" data-i18n="Email">الاقسام الفرعيه </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  subcategories -- >

                  <!- start  Products -->
                @hasPermission('admin.products.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.products.index') active @endif ">
                    <a href="{{route('admin.products.index')}}">
                        <i class="feather icon-server"></i>
                        <span class="menu-title" data-i18n="Email">الزفات</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  Products -- >


                <!- start  Album -->
                @hasPermission('admin.albums.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.albums.index') active @endif ">
                    <a href="{{route('admin.albums.index')}}">
                        <i class="feather icon-image"></i>
                        <span class="menu-title" data-i18n="Email">  البوم الصور </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  Album -- >

                 <!- start  Album Images -->
                @hasPermission('admin.albumimages.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.albumimages.index') active @endif ">
                    <a href="{{route('admin.albumimages.index')}}">
                        <i class="feather icon-image"></i>
                        <span class="menu-title" data-i18n="Email"> الصور  </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  Album Images -- >

                  <!- start  Characters -->
                @hasPermission('admin.characters.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.characters.index') active @endif ">
                    <a href="{{route('admin.characters.index')}}">
                        <i class="feather icon-italic"></i>
                        <span class="menu-title" data-i18n="Email"> الاسماء في الزفات  </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  Characters -- >

                 <!- start  Singers -->
                @hasPermission('admin.singers.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.singers.index') active @endif ">
                    <a href="{{route('admin.singers.index')}}">
                        <i class="feather icon-mic"></i>
                        <span class="menu-title" data-i18n="Email"> المغنيين </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  Singers -- >

                <!- start  Subjects -->
                @hasPermission('admin.subjects.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.subjects.index') active @endif ">
                    <a href="{{route('admin.subjects.index')}}">
                        <i class="feather icon-activity"></i>
                        <span class="menu-title" data-i18n="Email"> المناسبات  </span>
                    </a>
                </li>
                @endhasPermission
                <!-- end  Subjects -- >

               <!- start Audio -->
                @hasPermission('admin.audio.index')
                <li class="nav-item @if(\Request::route()->getName() == 'admin.audio.index') active @endif ">
                    <a href="{{route('admin.audio.index')}}">
                        <i class="feather icon-radio"></i>
                        <span class="menu-title" data-i18n="Email">الملفات الصوتيه</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Audio -- >

                <!- start reports -->
                @hasPermission('admin.reports.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.reports.index') active @endif">
                    <a href="{{route('admin.reports.index')}}">
                        <i class="feather icon-file-text"></i>
                        <span class="menu-title" data-i18n="Account Settings">التقارير</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end reports -->

                <!-- start email -->
                @hasPermission('admin.inbox.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.inbox.index') active @endif">
                    <a href="{{route('admin.inbox.index')}}">
                        <i class="feather icon-mail"></i>
                        <span class="menu-title" data-i18n="Email">البريد الوارد</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end email -->

                <!-- start complains -->
                @hasPermission('admin.complains.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.complains.index') active @endif">
                    <a href="{{route('admin.complains.index')}}">
                        <i class="feather icon-help-circle"></i>
                        <span class="menu-title" data-i18n="Email">الشكاوي والمقترحات</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end complains -->

                <!-- start Country -->
                @hasPermission('admin.countries.index')
                <li class=" nav-item @if(\Request::is('dashboard/countries*') || \Request::is('dashboard/regions*') || \Request::is('dashboard/cities*') || \Request::is('dashboard/districts*')) active @endif">
                    <a href="{{route('admin.countries.index')}}">
                        <i class="feather icon-map"></i>
                        <span class="menu-title" data-i18n="Email">الدول</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Country -->


                <!-- start Notification -->
                @hasPermission('admin.notifications.create')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.notification.create') active @endif">
                    <a href="{{route('admin.notifications.create')}}">
                        <i class="ficon feather icon-bell"></i>
                        <span class="menu-title" data-i18n="Email">الاشعارات</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Notification -->

                <!-- start Slider -->
                @hasPermission('admin.slids.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.slids.index') active @endif">
                    <a href="{{route('admin.slids.index')}}">
                        <i class="feather icon-layers"></i>
                        <span class="menu-title" data-i18n="Email">خلفية الموقع</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Slider -->




                <!-- start Faqs -->
                @hasPermission('admin.faqs.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.faqs.index') active @endif">
                    <a href="{{route('admin.faqs.index')}}">
                        <i class="feather icon-alert-circle"></i>
                        <span class="menu-title" data-i18n="Email">الاسئله الشائعه</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end Faqs -->

                <!-- start settings -->
                @hasPermission('admin.settings.index')
                <li class=" nav-item @if(\Request::route()->getName() == 'admin.settings.index') active @endif">
                    <a href="{{route('admin.settings.index')}}">
                        <i class="feather icon-settings"></i>
                        <span class="menu-title" data-i18n="Account Settings">الاعدادات</span>
                    </a>
                </li>
                @endhasPermission
                <!-- end settings -->
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->
</div>
