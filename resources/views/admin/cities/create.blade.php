@extends('admin.master')

@section('title', 'اضافة مدينة')

@section('content')

    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الدول</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الدول</a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.countries.show', $region?->country) }}">{{ $region?->country?->name }}</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.regions.index', $region?->country) }}">المناطق</a></li>
                            <li class="breadcrumb-item active">اضافة مدينة </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->
    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">اضافة مدينة</h4>
                        </div>

                        <!-- You can all alert messages by removing the comment -->
{{--                        @include('admin.includes.alerts.all-errors')--}}
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.cities.store') }}" method="POST">
                                @csrf
                                    <input type="hidden" name="region_id" value="{{ $region->id }}">

                                    <div class="form-body">
                                        <div class="row">
                                            @foreach(config('app.languages') as $key => $language)
                                                <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" id="{{$key}}-name" class="form-control" name="name[{{$key}}]"
                                                               placeholder="{{ __('dashboard.main.name_in_' . $key) }}" autofocus
                                                               value="{{ old("name.$key") }}">

                                                        <label for="{{$key}}-name">{{ __('dashboard.main.name_in_' . $key) }}</label>

                                                        @include('admin.includes.alerts.input-errors', ['input' => "name.$key"])
                                                    </div>
                                                </div>
                                            @endforeach

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@endsection
