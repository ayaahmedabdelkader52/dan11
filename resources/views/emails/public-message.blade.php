<html>
<head>
    <title></title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;700&family=Poppins:wght@700&display=swap"
          rel="stylesheet">
    <meta name="viewport" content="width=device-width"/>
    <style>
        @media screen and (max-width: 600px) {
            .whiteboard {
                width: 84% !important;
            }

            .title {
                font-size: 18px !important;
                line-height: 24px !important;
                margin-left: 4% !important;
            }

            .paragraph {
                width: 92% !important;
                font-size: 14px !important;
                margin: 22px auto !important;
            }

            .button {
                margin: 36px auto !important;
                font-size: 16px !important;
                padding-top: 18px !important;
                width: 240px !important;

            }

            .line {
                margin-top: 4px !important;
                width: 98% !important;
                margin: 0 auto !important;
            }

            .email {
                font-size: 14px !important;
            }

            .img {
                width: 18px !important;
            }

            .icon {
                width: 18px !important;
            }

            .facebook {
                display: none !important;
            }

            .title-text {
                margin: 0 6%;
                text-align: left;
                font-size: 28px;
                font-family: 'Poppins', -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
                Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
                font-weight: 700;
                margin-top: 20px;
                line-height: 38px
            }
        }
    </style>
</head>


<body>
<div data-role="module-unsubscribe" class="module" role="module" data-type="unsubscribe"
     style="position: relative;  color:#444444; font-size:12px; line-height:20px; padding:16px 16px 16px 16px; text-align:Center; background-color: white;"
     data-muid="4e838cf3-9892-4a6d-94d6-170e474d21e5">
    <!--Container Principal-->
    <div class="whiteboard" id="whiteboard"
         style="width: 536px; border-radius: 8px;  background-color: white; border: 1px solid #E5E5E5; margin: 20px auto;">

        <img id="logo" class="logo"
             src="{{ (isset($setting['logo'])) ? asset('assets/uploads/settings/' . $setting['logo']) : asset('Admin/app-assets/images/ico/logo.svg') }}"
             style="width: 50%; position: relative; margin: 15px;"/>

        <div style="padding: 0px 30px 24px 30px;">
            <h1 class="title title-text" style="margin: 0 6%; text-align: left; font-size: 28px; font-family: 'Poppins', -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
                Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif; font-weight: 700; margin-top: 20px; line-height: 38px">
                {!! $data['header'] ?? '' !!}
            </h1>

            <p class="paragraph" style="text-align: left; font-size:16px; font-family: 'Mulish',-apple    -system, BlinkMacSystemFont, Segoe UI, Roboto,
                 Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif; font-weight: 400; color: #767F89; line-height: 26px; width: 84%; margin:6%;">
                {!! $data['content'] ?? '' !!}
                <br><br>

            </p>
            @if(isset($data['url']))
                <a href="{{ $data['url'] ?? '#' }}" class="button"
                   style="font-size: 18px; text-decoration: none; color: white; background-color: #009028; padding: 18px; width: 300px; border: none; border-radius: 31px; margin: 50px auto; font-family: 'Poppins',-apple    -system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif; display: block; text-align: center; font-weight: 700;">
                    {{ $data['urlText'] ?? '' }}
                </a>
            @endif

        </div>

    </div>
    <a class="Unsubscribe--unsubscribeLink" style="font-family:sans-serif;text-decoration:none; color: #313E4C;">
        {!! $setting['copyright_ar'] ?? '' !!}
    </a><br>
</div>
</body>
</html>
