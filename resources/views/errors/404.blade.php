<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="@if(isset($setting['description'])){{ $setting['description'] }} @endif">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="@if(isset($setting['author'])){{ $setting['author'] }} @endif">
    <title>صفحه غير متوفره</title>

    <link rel="shortcut icon" type="image/x-icon" href="@if(isset($setting['favicon'])){{asset('assets/uploads/settings/' . $setting['favicon'] )}} @else {{asset('Admin/app-assets/images/ico/favicon.ico')}} @endif">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/vendors'.$dir.'.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/app-assets/css'.$dir.'/colors.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/app-assets/css'.$dir.'/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/themes/semi-dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/assets/css/style.css')}}">
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- error 404 -->
            <section class="row flexbox-container">
                <div class="col-xl-7 col-md-8 col-12 d-flex justify-content-center">
                    <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
                        <div class="card-content">
                            <div class="card-body text-center">
                                <img src="{{ asset('Admin/app-assets/images/pages/404.png') }}" class="img-fluid align-self-center" alt="branding logo">
                                <h1 class="font-large-2 my-1">404 </h1>
                                <p class="p-2">صفحه غير متوفره</p>
                                <a class="btn btn-primary btn-lg mt-2" onclick="window.history.back();">عوده</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- error 404 end -->

        </div>
    </div>
</div>
<!-- END: Content-->

</body>
<!-- END: Body-->

</html>
