<?php

return [
    'notifications' => 'الاشعارات',
    'note added' => 'تم اضافة ملاحظات علي المرحلة',
    'installment created' => 'تم اضافة دفعة مالية لمشروعك',
    'options created' => 'تم انشاء اختيارات من فضلك اختر احد هذه الاختيارات',
    'project created' => 'تم انشاء مشروع لك',
    'project updated' => 'تم التعديل المشروع الخاص بك',
    'link approved' => 'تم اعتماد اللينك بنجاح',
    'option approved' => 'تم اعتماد الاختيار بنجاح',
    'step in work' => 'جاري العمل علي المرحلة',
    'step is done' => 'انتهت المرحلة بنجاح',
    'document uploaded' => 'قام برفع ملفات',
    'all notifications' => 'كل الاشعارات',

    'user_sent_message' => 'قام :name بارسال رسالة جديدة',
    'user_sent_complain' => 'قام :name بارسال شكوي جديدة',
    'new_notifications_count' => 'يوجد :count اشعارات جديدة',
    'no_new_notifications' => 'لا يوجد اشعارات جديدة',
    'new_messages_count' => 'يوجد :count رسائل جديدة',
    'no_new_messages' => 'لا يوجد رسائل جديدة',
    'from_admin' => 'لديك اشعار من الادارة',
    'new_order' => 'لديك طلب جديد',
];
