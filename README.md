<<<<<<< HEAD
<p align="center">
<img src="https://managementsystem.4hoste.com/assets/web/images/logo.svg" width="400">
</p>

# SETUP

### 1. Clone The repo to your locale
- **using ssh key**
```
git clone git@gitlab.com:moazmarouf444/dashboard_awamer_2021.git
```

- **using https**
```
git clone https://gitlab.com/moazmarouf444/dashboard_awamer_2021.git
```
### 2. [Optional] Rename the repo

```
mv Dashboard_Awamer_2021 your_name
```

### 2. cd into

```
cd your_name_or_Dashboard_Awamer_2021
```

### 4. Install Composer Dependencies

Whenever you clone a new Laravel project you must now install all the project dependencies. This is what actually installs Laravel itself, among other necessary packages to get started.

```
composer install
```

### 5. Create a copy of your .env file
`.env` files are not generally committed to source control for security reasons. But there is a `.env.example` which is a template of the `.env` file that the project expects us to have.

```
cp .env.example .env
```

### 6. Generate an app encryption key
Laravel requires you to have an app encryption key which is generally randomly generated and stored in your `.env` file. The app will use this encryption key to encode various elements of your application from cookies to password hashes and more.

```
php artisan key:generate
```

### 7. Create an empty database for our application
Create an empty database for your project

### 8. In the .env file, add database information to allow Laravel to connect to the database
We will want to allow Laravel to connect to the database that you just created in the previous step. To do this, we must add the connection credentials in the .env file and Laravel will handle the connection from there.

### 9. Migrate the database

```
php artisan migrate
```

#### 10. Seed the database

```
php artisan db:seed
```

### 11. server your project
Start up a local development server with `php artisan serve` And, visit the URL http://localhost:8000/dashboard in your browser.

If you installed with our data, an admin has been created for you with the following login credentials:

```
Email: aait@info.com
Password: 123456789
```

# Custom Notifications


We are providing custom notification that has the ability to send messages to different channels such as Database, Fcm, Email, and Sms

We are replaced laravel notifications with this custom notification

First replace `Illuminate\Notifications\Notifiable` with `App\Traits\Notifications\Notifiable` in `User` model
```php
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
}
```

## Quick Example

`php artisan make:notification OrderAcceptedNotification`

Class `App\Notifications\AcceptBookingNotification` will create that contains a `via` method that accept `database`, `fcm`, `sms`, `email`

> ⚠️ This is not laravel notification it was customized by using [Stub Customization](https://laravel.com/docs/7.x/artisan#stub-customization)

The notify method that is provided by this trait expects to receive a notification instance:

```php
use App\Models\User;
use App\Notifications\AcceptBookingNotification;

$user = User::first();

$user->notify(new AcceptBookingNotification());
```

if you want to notify a user by database notification, Run the following command to make our notification:

# Make Modules Command

Make modules [command](https://laravel.com/docs/8.x/artisan) is artisan command That generate all admin Modules include `model`, `controller`, `Migration`, `Requests`, and crud `blades`.

### Argument

### Argument
This command take three argument `name` [required] , `arSingleName` [optional], and `arPluralName` [optional]

1. `name` The name is the name of model that you want to create like `Product`
2. `arSingleName` The arabic Single name that shown to users like `منتج`
3. `arPluralName` The arabic Plural name that shown to users like `منتجات`

### Options

1. `--ob` to Create observer
2. `--seed` to Create seeder
3. `--resource` to Create resource

## Example

```
 php artisan make:modeles Product منتج منتجات
```
> ⚠️ The second argument is `منتج`  not `منتجات` As shown in the argument section

This is the info message that shown after create the model
```
Model Created        -> C:\laragon\www\laravel-advanced\app\Models\Products.php
Controller Created   -> C:\laragon\www\laravel-advanced\app\Http\Controllers\Dashboard\ProductsController.php
Migration Created    -> C:\laragon\www\laravel-advanced\database\migrations\2021_09_08_120245_create_productses_table.php
Index View Created   -> C:\laragon\www\laravel-advanced\resources\views\admin\productses\index.blade.php
Create View Created  -> C:\laragon\www\laravel-advanced\resources\views\admin\productses\create.blade.php
Edit View Created    -> C:\laragon\www\laravel-advanced\resources\views\admin\productses\edit.blade.php
Show View Created    -> C:\laragon\www\laravel-advanced\resources\views\admin\productses\show.blade.php
Store Request        -> C:\laragon\www\laravel-advanced\app\Http\Requests\Products\StoreRequest.php
Update Request       -> C:\laragon\www\laravel-advanced\app\Http\Requests\Products\UpdateRequest.php
Export Created       -> C:\laragon\www\laravel-advanced\app\Exports\productsesExport.php
Admin Routes updates -> C:\laragon\www\laravel-advanced\routes/admin.php
Ar Routes Updated    -> C:\laragon\www\laravel-advanced\resources/lang/ar/routes.php
En Routes Updated    -> C:\laragon\www\laravel-advanced\resources/lang/en/routes.php
Ar dashboard Updated -> C:\laragon\www\laravel-advanced\resources/lang/ar/dashboard.php
En dashboard Updated -> C:\laragon\www\laravel-advanced\resources/lang/en/dashboard.php
```

#### ⚠️if The modules exists it will show [asking-for-confirmation](https://laravel.com/docs/8.x/artisan#asking-for-confirmation) to confirm if you want to Overwrite it for example if the model is exists it will show this confirm.
`Model Already exists, Do you want to Overwrite  (yes/no) [yes]:`

# Roles & Permissions

we are customizing roles and permission by getting the permissions and put the permissions to any `route` in its `child`, and `title` and we save the permission that in child in the database related with role then check if the auth role by `role_id`.
the  `App\Http\Middleware\CheckRole` middleware check if the authenticated use role include the current route name.

### For example

We will put `admin.users.index`, and `admin.users.create`.
```php
Route::get('/', [
    'uses' => 'App\Http\Controllers\Dashboard\UserController@index',
    'as' => 'users.index',
    'title' => __('dashboard.user.users'),
    'child' => [
        'admin.users.index',
        'admin.users.create',
    ]
]);
```

In this code we put two permissions then you can add it in the database as it defined in the `Database\Seeders\RoleSeeder`

### hasPermission directive
In the `App\Providers\ladeServiceProvider` we define [custom if directive](https://laravel.com/docs/8.x/blade#custom-if-statements) that check if the passed route in the authenticated permissions

#### Usage

```php
@hasPermission('admin.users.index')
<!-- auth user has this permission -->
@endhasPermission
```

* Or

```php
@hasPermission(['admin.users.index', 'admin.users.create'])
<!-- auth user has one of this permissions -->
@endhasPermission
```

> ⚠ `hasPermission directive` accept a permission name as a `string` as you can see or multi as `array` permissions that check if one of them in the  authenticated permissions


# Settings

In setting, you can update a lot of setting such as `update profile`, `change password`, `site setting`, and sms, email, notification `packages`
after you update packages you will not need to edit your `.env` file or `config`.
In `App\Providers\PackagesServiceProvider` provider it fetches the active package data from database then set this data in `config`.

## Packages

As we mentioned before you will not need to edit your `config` files all of these settings are user-friendly, and you can update it from your Admin setting.

### SMS Package

We are providing a lot of sms packages you can see it `App\Models\MessagePackage::SMS_PACKAGES` const.
Each Package has service in the `App\Services\Messaging` directory using it to send the message.
if you want to add package:-
1. Adding the package name in the `App\Models\MessagePackage::SMS_PACKAGES` array, it will be shown to you in Admin Setting
2. Create service for this package in the `App\Services\Messaging` namespace.
> be carefully that the name of this service must be the name in the array concatenating with Service.

For example:-
I wanna to add `tillio` package.
1. Add `twillio` to the `App\Models\MessagePackage::SMS_PACKAGES` array.
2. Create `App\Services\Messaging\TwillioService` that implement `App\Contracts\MessagingService\MessagingService` interface.
=======
# DAN



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ayaahmedabdelkader52/DAN.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:376464fcaa78fed9044da90efde6b9bc?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

>>>>>>> origin/main
