<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('country_key')->nullable();
            $table->string('phone')->nullable();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('avatar')->default('default.png');
            $table->foreignId('role_id')->default(2)->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->enum('status', ["block", "pending", "active"])->default('pending');
            $table->enum('gender', ['male', 'female']);
            $table->enum('type', ['admin', 'user'])->default('user');
            $table->string('code')->nullable();
            $table->string('lat')->nullable();
            $table->string('lang')->default('ar');
            $table->text('address')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
