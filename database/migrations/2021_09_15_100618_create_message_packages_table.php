<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_packages', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('sender_email')->nullable();
            $table->string('sender_name')->nullable();
            $table->integer('port')->nullable();
            $table->string('host')->nullable();
            $table->string('encryption')->nullable();
            $table->string('number')->nullable();
            $table->string('application_id')->nullable();
            $table->string('authorization')->nullable();
            $table->string('server_key')->nullable();
            $table->string('sender_id')->nullable();
            $table->enum('active', ['true', 'false'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_packages');
    }
}
