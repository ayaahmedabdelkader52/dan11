<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->text('cart')->nullable();
            $table->text('address')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->date('order_date')->nullable();
            $table->enum('type' , ['album' , 'product'])->nullable();
            $table->enum('status' , ['paid' , 'wait' , 'expired'])->default('wait');
            $table->enum('type_payment' , ['cash' , 'online'])->nullable();
            $table->string('extra_phone')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
