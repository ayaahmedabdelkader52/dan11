<?php

namespace Database\Seeders;

use App\Models\MessagePackage;
use Illuminate\Database\Seeder;

class PackagesSeeder extends Seeder
{
    /**
     * Uses because if you refresh your database.
     */
    private array $packages = [
        [
            'type' => 'smtp',
            'username' => 'TEST_USERNAME',
            'password' => 'TEST_PASSWORD',
            'sender_email' => 'TEST_EMAIL',
            'sender_name' => 'TEST_NAME',
            'port' => 123,
            'host' => 'TEST_HOST',
            'encryption' => 'TEST_ENCRYPTION',
            'active' => 'true',
        ], [
            'type' => 'forjawally',
            'username' => 'TEST_USERNAME',
            'password' => 'TEST_PASSWORD',
            'sender_name' => 'TEST_NAME',
            'active' => 'true',
        ], [
            'type' => 'fcm',
            'server_key' => 'TEST_KEY',
            'sender_id' => 'TEST_ID',
            'active' => 'true',
        ],
    ];

    public function run()
    {
        foreach ($this->packages as $package) {
            MessagePackage::create($package);
        }
    }
}
