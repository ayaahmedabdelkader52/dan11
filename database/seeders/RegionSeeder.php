<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionSeeder extends Seeder {
  public function run() {
    $egypt        = Country::where('iso2', 'EG')->first();
    $sar          = Country::where('iso2', 'SA')->first();
    $egyptRegions = json_decode(file_get_contents(__DIR__ . '/data/egypt/governorates.json'), true);
    $sarRegions   = json_decode(file_get_contents(__DIR__ . '/data/Saudi-Arabia v2/regions.json'), true);

    if ($sar) {
      foreach ($sarRegions as $region) {
        $data[] = [
          'country_id' => $sar->id,
          'name'       => \json_encode(['ar' => ($region['governorate_name_ar']) ?? '', 'en' => ($region['governorate_name_en']) ?? '']),
          'population' => ($region['population']) ?? 0,
          'center'     => isset($region['center']) ? json_encode($region['center']) : null,
          'code'       => ($region['code']) ?? '',
          // 'boundaries' => ($region['boundaries']) ?? '',
        ];
      }
    }

    if ($egypt) {
      foreach ($egyptRegions as $region) {
        $data[] = [
          'country_id' => $egypt->id,
          'name'       => \json_encode(['ar' => ($region['governorate_name_ar']) ?? '', 'en' => ($region['governorate_name_en']) ?? '']),
          'population' => ($region['population']) ?? 0,
          'center'     => isset($region['center']) ? json_encode($region['center']) : null,
          'code'       => ($region['code']) ?? '',
          // 'boundaries' => ($region['boundaries']) ?? '',
        ];
      }

    }

    foreach (array_chunk($data, 1000) as $t) {DB::table('regions')->insert($t);}

  }
}
