<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictSeeder extends Seeder {
  public function run() {
    $sarDistricts = json_decode(file_get_contents(__DIR__ . '/data/Saudi-Arabia v2/districts.json'), true);

    foreach ($sarDistricts as $district) {
      $data[] = [
        'name'       => \json_encode(['ar' => ($district['name_ar']) ?? '', 'en' => ($district['name_en']) ?? '']),
//        'city_id'    => $district['city_id'],
//        'region_id'  => $district['region_id'],
        // 'boundaries' => $district['boundaries'],
      ];
    }

    foreach (array_chunk($data, 1000) as $t) {DB::table('districts')->insert($t);}

  }
}
