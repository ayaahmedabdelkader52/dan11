<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
  public function run() {
    $user = [
      'name'        => 'AAIT',
      'phone'       => '1551244085',
      'country_key' => '002',
      'email'       => 'aait@info.com',
      'password'    => 123456789,
      'status'      => 'pending',
      'address'     => 'Mansoura',
      'role_id'     => 1,
      'type'        => 'admin',
      'gender'      => 'male',
    ];
    User::create($user);
  }
}
