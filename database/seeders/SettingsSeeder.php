<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $settings = [
        [
            'key' => 'logo',
            'value' => 'default.png',
        ],
        [
            'key' => 'site_email',
            'value' => 'aait@info.com',
        ],
        [
            'key' => 'site_phone',
            'value' => '012227965236'
        ],
        [
            'key' => 'site_name',
            'value' => 'أوامر',
        ],
        [
            'key' => 'site_address',
            'value'=>'الدقهليه , المنصوره , طلخا',
        ],

        [
            'key' => 'site_keywords',
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.',
        ],
        [
            'key' => 'copyright_ar',
            'value' => 'جميع الحقوق محفوظة لشركة أوامر الشبكة سياسات 2021',
        ],
        [
            'key' => 'copyright_en',
            'value' => 'All rights reserved to a Awamer Elsabaka Policies 2021',
        ],
        [
            'key' => 'policies_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص ',
        ],
        [
            'key' => 'policies_en',
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.',
        ],

        [
            'key' => 'google_places_key',
            'value' => '',
        ],
        [
            'key' => 'wasl_api_key',
            'value' => '',
        ],
        [
            'key' => 'currencyconverterapi',
            'value' => '',
        ],
        [
            'key' => 'google_analytics',
            'value' => '',
        ],
        [
            'key' => 'live_chat',
            'value' => '',
        ],
        [
            'key' => 'login_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص',
        ],
        [
            'key' => 'login_description_en',
            'value' => '',
        ],
        [
            'key' => 'register_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص',
        ],
        [
            'key' => 'register_description_en',
            'value' => '',
        ],
        [
            'key' => 'active_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص',
        ],
        [
            'key' => 'active_description_en',
            'value' => '',
        ],
        [
            'key' => 'forget_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص',
        ],
        [
            'key' => 'forget_description_en',
            'value' => '',
        ],
        [
            'key' => 'sendcode_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص',
        ],
        [
            'key' => 'sendcode_description_en',
            'value' => '',
        ],
        [
            'key' => 'contact_us_minname_ar',
            'value' => ' تواصل معانا ',
        ],
        [
            'key' => 'contact_us_miname_en',
            'value' => '',
        ],
        [
            'key' => 'contact_us_name_ar',
            'value' => 'الوصول إلينا في اي وقت',
        ],
        [
            'key' => 'contact_us_name_en',
            'value' => '',
        ],
        [
            'key' => 'contact_us_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا',
        ],
        [
            'key' => 'contact_us_description_en',
            'value' => '',
        ],
        [
            'key' => 'contact_us_photo1',
            'value' => 'contact1.png',
        ],
        [
            'key' => 'contact_us_photo2',
            'value' => 'contact2.png',
        ],
        [
            'key' => 'contact_us_photo3',
            'value' => 'contact3.png',
        ],
        [
            'key' => 'contact_us_photo4',
            'value' => 'contact4.png',
        ],

        [
            'key' => 'view_questions_ar',
            'value' => 'يمكنك البحث الأن في الاسئله',
        ],
        [
            'key' => 'view_questions_en',
            'value' => '',
        ],

        [
            'key' => 'faqs_name_ar',
            'value' => 'الأسئله الشائعه',
        ],
        [
            'key' => 'faqs_name_en',
            'value' => '',
        ],


        [
            'key' => 'faqs_description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى',
        ],
        [
            'key' => 'faqs_description_en',
            'value' => '',
        ],

                /// About Us
        [
            'key' => 'about_us_name_ar',
            'value'=>'أشهر مواقع الزفاف في العالم العربي'
        ],
        [
            'key' => 'about_us_name_en',
            'value' => '',
        ],

        [
            'key' => 'about_us_description_ar',
            'value' => 'إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.',
        ],
        [
            'key' => 'about_us_description_en',
            'value' => '',
        ],


        [
            'key' => 'about_us_photo1',
            'value' => 'about1.png',
        ],
        [
            'key' => 'about_us_photo2',
            'value' => 'about2.png',
        ],
        [
            'key' => 'about_us_photo3',
            'value' => 'about3.png',
        ],
        [
            'key' => 'about_us_photo4',
            'value' => 'about4.png',
        ],
        [
            'key' => 'about_us_card_ar',
            'value' => 'اﻟﺮؤﻳﺔ',
        ],
        [
            'key' => 'about_us_card_en',
            'value' => '',
        ],

        [
            'key' => 'about_us_card_photo',
            'value' => 'a1.png',
        ],
        [
            'key' => 'about_us_message_ar',
            'value' => 'رسالتنا',
        ],
        [
            'key' => 'about_us_message_en',
            'value' => '',
        ],

        [
            'key' => 'about_us_messege_photo',
            'value' => 'a2.png',
        ],
        [
            'key' => 'about_us_rate_ar',
            'value' => 'ﻗﻴﻤﻨﺎ',
        ],
        [
            'key' => 'about_us_rate_en',
            'value' => '',
        ],

        [
            'key' => 'about_us_rate_photo',
            'value' => 'a3.png',
        ],


            //// Join Us

        [
            'key' => 'join_us_name_ar',
            'value' => 'اكتر من مئه فريق حول العالم',
        ],
        [
            'key' => 'join_us_name_en',
            'value' => '',
        ],

        [
            'key' => 'join_us_description_ar',
            'value' => 'إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.',
        ],
        [
            'key' => 'join_us_description_en',
            'value' => '',
        ],

        [
            'key' => 'join_us_photo1',
            'value' => 'join1.png',
        ],
        [
            'key' => 'join_us_photo2',
            'value' => 'join2.png',
        ],
        [
            'key' => 'join_us_photo3',
            'value' => 'join3.png',
        ],
        [
            'key' => 'join_us_photo4',
            'value' => 'join4.png',
        ],
         /// End Join Us

        [
            'key' => 'title_rules_ar',
            'value' => 'الشروط والأحكام',
        ],
        [
            'key' => 'title_rules_en',
            'value' => '',
        ],
        [
            'key'=>'privacy_title_ar',
            'value'=>'',
        ],
        [
            'key'=>'privacy_title_en',
            'value'=>'',
        ],
        [
            'key'=>'privacy_content_ar',
            'value'=>'',
        ],
        [
            'key'=>'privacy_content_en',
            'value'=>'',
        ],
    ];

    public function run()
    {
        Setting::insert($this->settings);
    }
}
