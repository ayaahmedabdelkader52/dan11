<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private  $langs =
        [
            [
                'name_ar' => 'العربية',
                'name_en' => 'ar' ,
            ],
            [
                'name_ar' => 'الانجليزية',
                'name_en' => 'en' ,
            ],
            [
                'name_ar' => 'الفرنسية',
                'name_en' => 'fr' ,
            ],
            [
                'name_ar' => 'الايطالية',
                'name_en' => 'ita' ,
            ],
        ];
    public function run()
    {
        foreach ($this->langs as $lang) {
            Language::create($lang);
        }
    }
}
