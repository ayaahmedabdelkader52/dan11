<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder {
  public function run() {
    $egyptCities = json_decode(file_get_contents(__DIR__ . '/data/egypt/cities.json'), true);
    $sarCities   = json_decode(file_get_contents(__DIR__ . '/data/Saudi-Arabia v2/cities.json'), true);

    foreach ($sarCities as $city) {
      $data[] = [
        'name'      => \json_encode(['ar' => ($city['name_ar']) ?? '', 'en' => ($city['name_en']) ?? '']),
        'region_id' => $city['region_id'],
        'center'    => isset($city['center']) ? json_encode($city['center']) : null,
      ];
    }

    foreach ($egyptCities as $city) {
      $data[] = [
        'name'      => \json_encode(['ar' => ($city['city_name_ar']) ?? '', 'en' => ($city['city_name_en']) ?? '']),
        'region_id' => $city['governorate_id'],
        'center'    => isset($city['center']) ? json_encode($city['center']) : null,
      ];
    }

    foreach (array_chunk($data, 1000) as $t) {DB::table('cities')->insert($t);}

  }
}
