$(window).on('load', function() {

    $('.loader').fadeOut(1000);

    new WOW().init();

    $('body').addClass('o-auto');

})

$(document).ready(function() {
    $('.default').on('click', function(e) {
        e.preventDefault();
    });

    // Start Navbar 
    $('.select-2').select2();
    $('.select2-container--default').addClass("form-control");

    $('.overlay').fadeOut();
    $(".top-cover").parents("body").find("header").addClass("topheader");
    $(".departments .div1 , .departments .div4 , .departments .div5 , .departments .div8 ,  .departments .div9  ").addClass("col-md-4");
    $(".departments .div2 , .departments .div3 , .departments .div6 , .departments .div7 ,  .departments .div10 ").addClass("col-md-8");

    $(".open-details").on('click', function() {
        $(this).parents(".orders").slideUp(500);
        $(".show-order-details").slideDown(500);

    });
    $(".back-to-orders").on('click', function() {
        $(this).parents(".show-order-details").slideUp(500);
        $(".orders").slideDown(500);

    });
    $(".btn-search").on('click', function() {
        $(this).parents(".menu").find(".header-search").addClass("open-div");
        $(this).parents(".menu").find(".header-search").removeClass("close-div");
    });
    $(".close-search").on('click', function() {
        $(this).parents(".header-search").addClass("close-div");
    });
    $(".open-share").on('click', function() {
        $(".share-div").fadeToggle();
    });

    $(".mob-collaps").on('click', function() {
        $(this).parents(".menu-content").find(".nav-links > ul").toggleClass('nav-open');

        $('.overlay').fadeToggle();

        $(this).find("span:first-child").toggleClass('rotate');
        $(this).find("span:nth-child(2)").toggleClass('none');
        $(this).find("span:nth-child(3)").toggleClass('rotate2');
    });

    $(".overlay").on('click', function() {
        $(".nav-links ul").removeClass('nav-open');
        $(this).fadeOut();

        $(".mob-collaps span:first-child").removeClass('rotate');
        $(".mob-collaps span:nth-child(2)").removeClass('none');
        $(".mob-collaps span:nth-child(3)").removeClass('rotate2');
    });



    $(".nav-links a").each(
        function() {
            if (window.location.href.includes($(this).attr('href'))) {
                $(this).parents('li').addClass("active");
            }
        }
    );

    $(".top-link ").each(
        function() {
            if (window.location.href.includes($(this).attr('href'))) {
                $(this).addClass("active");
            }
        }
    );



    $('a.page-scroll').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 40
                }, 900);
                return false;
            }
        }
    });
    $('.top').owlCarousel({

        margin: 0,
        rtl: true,

        loop: true,
        nav: true,
        dots: true,
        navText: ["<span></span>", "<span></span>"],

        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.image-uploader').change(function(event) {
        for (var one = 0; one < event.target.files.length; one++) {
            $(this).parents('.images-upload-block').find('.upload-area').append('<div class="uploaded-block" data-count-order="' + one + '"><img src="' + URL.createObjectURL(event.target.files[one]) + '"><button class="close">&times;</button></div>');
        }
    });

    $('.images-upload-block').on('click', '.close', function() {
        $(this).parents('.uploaded-block').remove();
    });



});
var $star_rating = $('.star-rating .fa');

var SetRatingStar = function() {
    return $star_rating.each(function() {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};

$star_rating.on('click', function() {
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});

SetRatingStar();