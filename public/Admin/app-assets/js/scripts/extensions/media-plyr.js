/*=========================================================================================
    File Name: media-plyr.js
    Description: Media Plyr Extenstion
    --------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function () {
  // video player  define
  var player = new Plyr(".video-player");
  // categories player define
  var player1 = new Plyr(".categories-player");

});
