<?php

namespace App\Http\Controllers;

use App;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Traits\HyperPay;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    use HyperPay, App\Traits\UploadTrait;
    public function index()
    {
        $products = Subcategory::all();
        Cache ::put('key', 'value' , 15);

        return view('subcategories.index', compact('products'));
    }

    public function create()
    {
        return view('subcategories.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['image'] = $this->uploadOne($request->image, 'subcategories');
        dd($data);
        Subcategory::create($request->all());

        return redirect()->route('subcategories.index')->withSuccess('Added successfully');
    }

    public function show(Subcategory $product)
    {
        if (\request()->has('resourcePath')) {
            dd($this->getStatus(\request()->input('id'), \request()->input('resourcePath')));
        }
        return view('subcategories.show', compact('product'));
    }

    public function edit(Subcategory $product)
    {
        //
    }

    public function update(Request $request, Subcategory $product)
    {
        //
    }

    public function destroy(Subcategory $product)
    {
        //
    }

    public function generateBarcode(Subcategory $product)
    {

        return view('subcategories.barcode', compact('product'));
    }

    public function showDescription(Subcategory $product)
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($product->description);
        return $pdf->stream();
    }

    public function buy(Subcategory $product)
    {
        $response = $this->checkout($product);

        $code = $response['result']['code'];
        $checkoutId = $response['id'];
        $view = view('subcategories.payment-form')->with(['response' => $response, 'id' => $product->id])
            ->renderSections();

        return response()->json([
            'status' => true,
            'form' => $view['form'],
            'data' => $response
        ]);
    }


}
